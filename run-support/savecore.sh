#!/bin/sh
# Save core files from lyskomd for future debugging.
# Copyright (C) 1995, 2003  Lysator Academic Computer Association.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# This script can be modified to save core files.  It is run
# by updateLysKOM before it starts a new lyskomd.  This script
# does nothing by default, since most sites probably don't
# want to save the core files anyhow.
