/*
 * Copyright (C) 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <assert.h>
#include <sys/time.h>
#include <stdlib.h>

#include "oop.h"

#include "misc-types.h"
#include "timeval-util.h"

Bool
timeval_nonzero(struct timeval t)
{
    return t.tv_sec != 0 || t.tv_usec != 0;
}

Bool
timeval_zero(struct timeval t)
{
    return t.tv_sec == 0 && t.tv_usec == 0;
}
    
struct timeval
timeval_ctor(time_t sec, int usec)
{
    struct timeval res;

    res.tv_sec = sec;
    res.tv_usec = usec;
    return res;
}

/* This function is taken from the GNU libc manual, and modified
   so that it doesn't alter the x and y arguments. */
static int
timeval_subtract(struct timeval *result,
		 struct timeval x,
		 struct timeval y)
{
    /* Perform the carry for the later subtraction by updating Y. */
    if (x.tv_usec < y.tv_usec)
    {
	int nsec = (y.tv_usec - x.tv_usec) / 1000000 + 1;
	y.tv_usec -= 1000000 * nsec;
	y.tv_sec += nsec;
    }
    if (x.tv_usec - y.tv_usec > 1000000)
    {
	int nsec = (x.tv_usec - y.tv_usec) / 1000000;
	y.tv_usec += 1000000 * nsec;
	y.tv_sec -= nsec;
    }
     
    /* Compute the time remaining to wait.
       `tv_usec' is certainly positive. */
    result->tv_sec = x.tv_sec - y.tv_sec;
    result->tv_usec = x.tv_usec - y.tv_usec;
     
    /* Return 1 if result is negative. */
    return x.tv_sec < y.tv_sec;
}

Bool
timeval_remaining(struct timeval *remaining,
		  struct timeval wanted_interval,
		  struct timeval start,
		  struct timeval now)
{
    if (timeval_subtract(remaining, now, start))
	return FALSE;

    if (timeval_subtract(remaining, wanted_interval, *remaining))
	return FALSE;

    return TRUE;
}

Bool
timeval_greater(struct timeval a,
		struct timeval b)
{
    if (a.tv_sec > b.tv_sec)
	return TRUE;

    if (a.tv_sec < b.tv_sec)
	return FALSE;

    if (a.tv_usec > b.tv_usec)
	return TRUE;

    return FALSE;
}
    
Bool
timeval_less(struct timeval a,
	     struct timeval b)
{
    if (a.tv_sec < b.tv_sec)
	return TRUE;

    if (a.tv_sec > b.tv_sec)
	return FALSE;

    if (a.tv_usec < b.tv_usec)
	return TRUE;

    return FALSE;
}

long
timeval_diff_sec(struct timeval a, struct timeval b)
{
    struct timeval res;

    timeval_subtract(&res, a, b);

    return res.tv_sec + (res.tv_usec >= 500000);
}

double
timeval_diff_d(struct timeval a, struct timeval b)
{
    return (a.tv_sec - b.tv_sec) + 1e-6 * (a.tv_usec - b.tv_usec);
}

int
setup_timer(struct timeval *tv,
	    struct timeval interval)
{
    if (gettimeofday(tv, NULL) < 0)
    {
	*tv = OOP_TIME_NOW;
	return -1;
    }

    tv->tv_sec += interval.tv_sec;
    tv->tv_usec += interval.tv_usec;
    if (tv->tv_usec >= 1000000)
    {
	tv->tv_usec -= 1000000;
	tv->tv_sec++;
    }
    assert(tv->tv_usec >= 0);
    assert(tv->tv_usec < 1000000);
    return 0;
}
