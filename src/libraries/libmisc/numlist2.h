/*
 * $Id: numlist2.h,v 0.11 2003/08/23 16:38:19 ceder Exp $
 * Copyright (C) 1991, 1993-1995, 1999, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: numlist2.h,v 0.11 2003/08/23 16:38:19 ceder Exp $
 *
 * File: numlist2.h
 * 
 * This file is supposed to be a reference while testing numlist.
 *
 * Author: Inge Wallin
 */


#ifndef NUMLIST2__H
#define NUMLIST2__H

#define NUMLIST_ERR      0xffffffff

#define MAX_NUMS   131072
#define NUM_CELLS  (MAX_NUMS / 32)

typedef struct {
    u_long   cells[NUM_CELLS];
} Numlist;


Numlist *numlist_create(void);
Bool     numlist_isempty(Numlist *);
Bool     numlist_member(Numlist *, u_long);
u_long   numlist_first(Numlist *);
u_long   numlist_next(Numlist *, u_long);
void     numlist_insert(Numlist *, u_long);
void     numlist_delete(Numlist *, u_long);


#endif   /* NUMLIST2__H */
