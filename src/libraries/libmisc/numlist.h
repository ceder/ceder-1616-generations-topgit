/*
 * $Id: numlist.h,v 0.11 2003/08/23 16:38:19 ceder Exp $
 * Copyright (C) 1991, 1993-1995, 1999, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: numlist.h,v 0.11 2003/08/23 16:38:19 ceder Exp $
 *
 * File: numlist.h
 * 
 * A numlist is an ordered list of numbers. One of the big features 
 * of this implementation is intervals.
 *
 * Author: Inge Wallin
 */


#ifndef NUMLIST__H
#define NUMLIST__H

#define INTERVAL_MASK    0xc0000000
#define NUMBER_MASK      0x3fffffff
#define INTERVAL_START   0x80000000
#define INTERVAL_END     0x40000000


/* This constant is returned whenever there are no more fitting values */
/* for a call, e.g. when numlist_first() is called with an empty Numlist. */

#define NUMLIST_ERR      0xffffffff


/* This constant is stored in the field last_num when no previous */
/* number is stored. */

#define NUMLIST_NO_NUMBER   0xffffffff


/*
 * One int + 13 longs + one pointer + the length info that 
 * malloc adds makes 64 bytes. This should make allocation fairly
 * efficient.
 */

/* #define NUM_LONGS_IN_NUMLIST  13 */
#define NUM_LONGS_IN_NUMLIST  5


typedef struct numlist_node   Numlist_node;
typedef struct numlist        Numlist;

struct numlist_node {
    int             num_numbers;
    u_long          numbers[NUM_LONGS_IN_NUMLIST];
    Numlist_node  * next;
};


struct numlist {
    Numlist_node  * first;
    Numlist_node  * last;
    
    /* The following info is used only for speeding up numlist_next(). */
    u_long          last_num;	/* The last number returned. */
    Numlist_node  * last_node;	/* The node containing the last number... */
    int             last_index;	/* ...and the index to it. */
};


/* ================================================================ */
/*    Prototypes for all global functions in the numlist package.   */


Numlist *numlist_create(void);
Bool     numlist_isempty(Numlist *);
Bool     numlist_member(Numlist *, u_long);
u_long   numlist_first(Numlist *);
u_long   numlist_last(Numlist *);
u_long   numlist_next(Numlist *, u_long);
void     numlist_insert(Numlist *, u_long);
void     numlist_delete(Numlist *, u_long);

/*
 * Useful (?) but not yet implemented:
 *
 * void     numlist_destroy(Numlist *);
 * void     numlist_insert_interval(Numlist *, u_long, u_long);
 * void     numlist_delete_interval(Numlist *, u_long, u_long);
 * Numlist *numlist_union(Numlist *, Numlist *);
 * Numlist *numlist_intersection(Numlist *, Numlist *);
 * Numlist *numlist_difference(Numlist *, Numlist *);
 * void     numlist_print(FILE *, Numlist *);
 * void     numlist_read(FILE *, Numlist *);
 */

#endif   /* NUMLIST__H */
