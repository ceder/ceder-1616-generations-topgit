/*
** isc_queue.c                           Handle queues for the ISC subsystem
**
** Copyright (C) 1991, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      moved to separate file
** (See ChangeLog for recent history)
*/

#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#ifndef NULL
#  include <stdio.h>
#endif
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <assert.h>

#include "oop.h"
#include "adns.h"
#include "oop-adns.h"

#include "s-string.h"

#include "isc.h"
#include "intern.h"


struct isc_msgqueue *
isc_newqueue(void)
{
    struct isc_msgqueue  * msg_q;

    
    msg_q = (struct isc_msgqueue *) isc_malloc(sizeof(struct isc_msgqueue));
    msg_q->head = NULL;
    msg_q->tail = NULL;
    msg_q->entries = 0;
    msg_q->bytes = 0;
    
    return msg_q;
}

int
isc_killqueue(struct isc_msgqueue  * queue)
{
    struct isc_msg_q_entry  * mqe;
    struct isc_msg_q_entry  * prev;
    int size = 0;


    if (queue == NULL)
      return 0;
    
    if ( queue->head != NULL )
    {
	mqe = queue->head;
	while ( mqe != NULL )
	{
	    prev = mqe;
	    mqe = mqe->next;
	    size += prev->msg->length;
	    isc_freemsg(prev->msg);
	    isc_free(prev);
	}
    }

    assert(size == queue->bytes);
    isc_free(queue);
    return size;
}


/*
** Push a message onto a queue. It is legal to push a message onto the
** the NULL queue and will be equal to freeing that message
*/
void
isc_pushqueue(struct isc_msgqueue  * queue,
	      struct isc_msg   * msg)
{
  struct isc_msg_q_entry  * mqe;


  if (queue == NULL)
  {
    isc_freemsg(msg);
    return;
  }
  
  mqe = (struct isc_msg_q_entry *) isc_malloc(sizeof(struct isc_msg_q_entry));
  
  mqe->msg  = msg;
  mqe->prev = queue->tail;
  mqe->next = NULL;

  if (queue->head == NULL)
    queue->head = mqe;
  
  if (queue->tail)
    queue->tail->next = mqe;

  queue->tail = mqe;
  queue->entries++;
  queue->bytes += msg->length;
}


struct isc_msg *
isc_topqueue(struct isc_msgqueue  * queue)
{
    if (queue != NULL && queue->head != NULL)
        return queue->head->msg;
    else
        return NULL;
}


int
isc_sizequeue(struct isc_msgqueue  * queue)
{
  if (queue)
    return queue->entries;
  else
    return -1;
}
	      

struct isc_msg *
isc_popqueue(struct isc_msgqueue  * queue)
{
    struct isc_msg_q_entry    * mqe;
    struct isc_msg  * msg;

    
    if (queue != NULL && queue->head != NULL)
    {
	mqe = queue->head;
	msg = mqe->msg;
	queue->head = mqe->next;

	if (queue->head)
	    queue->head->prev = NULL;
	else
	    queue->tail = NULL;

	queue->entries--;
	isc_free(mqe);
	return msg;
    }
    else
	return NULL;
}
