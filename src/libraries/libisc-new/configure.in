dnl ISC - networking library
dnl Copyright (C) 1998-1999, 2001 by Peter Eriksson and Per Cederqvist of the
dnl                         Lysator Academic Computer Association.
dnl
dnl
dnl This library is free software; you can redistribute it and/or
dnl modify it under the terms of the GNU Library General Public
dnl License as published by the Free Software Foundation; either
dnl version 2 of the License, or (at your option) any later version.
dnl
dnl This library is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl Library General Public License for more details.
dnl
dnl You should have received a copy of the GNU Library General Public
dnl License along with this library; if not, write to the Free
dnl Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

dnl Process this file with autoconf to produce a configure script.

AC_REVISION($Revision: 1.24 $)dnl
AC_PREREQ(2.56)
AC_INIT([isc],[1.01])
AC_CONFIG_SRCDIR([src/isc_master.c])
AM_INIT_AUTOMAKE(isc, 1.01)

AC_ARG_WITH([checker],
        AC_HELP_STRING([--with-checker], [compile with Gnu Checker]),
        [use_checker=$withval],
        [use_checker=no])

AC_ARG_WITH([gcov],
        AC_HELP_STRING([--with-gcov], [instrument for gcov (requires gcc)]),
        [use_gcov=$withval],
        [use_gcov=no])

AC_ARG_WITH([optimization],
        AC_HELP_STRING([--without-optimization], 
		       [turn off optimization (default on)])
AC_HELP_STRING([--with-optimization@<:@=N@:>@],[specify level of optimization]),
        [opt_level=$withval],
        [opt_level=""])

AC_ARG_ENABLE([ipv6],
        AC_HELP_STRING([--enable-ipv6], [enable IPv6 support]),
        [use_inet6=yes],
        [use_inet6=no])

AC_ARG_ENABLE([malloc-guards],
	AC_HELP_STRING([--disable-malloc-guards], 
	               [disable defensive guard areas (may crash lyskomd)]),
	[use_malloc_guards=$enableval],
	[use_malloc_guards=yes])

[if test "$use_malloc_guards" = "yes"
then]
	AC_DEFINE([USE_MALLOC_GUARDS], 1, 
		  [Insert magic number before and after malloced areas.
		   This can help detect buffer overruns.])
[fi]

dnl Checks for programs.

AC_PATH_PROG([AR], [ar], [notfound], [$PATH$PATH_SEPARATOR/usr/ccs/bin])
AC_ARG_VAR([AR], [ar program to use])
[if test "$AR" = "notfound"; then]
    AC_MSG_ERROR([cannot find ``ar''])
[fi]
AC_PROG_CC
AC_AIX
AC_ISC_POSIX
AC_MINIX
AC_PROG_RANLIB

dnl Checks for libraries.

dnl Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS(stdlib.h stdarg.h stddef.h string.h sys/select.h unistd.h)
AC_CHECK_HEADERS(sys/time.h stdint.h strings.h)

dnl Checks for typedefs, structures, and compiler characteristics.
AM_PROG_CC_STDC
AC_HEADER_TIME
AC_C_CONST
AC_C_INLINE
AC_TYPE_SIZE_T
AC_CHECK_TYPE([socklen_t],AC_DEFINE([HAVE_SOCKLEN_T]),,[
#include <sys/types.h>
#include <sys/socket.h>
])

dnl Low-level networking code on Solaris 2.
AC_CHECK_FUNC(gethostbyname)
[if test $ac_cv_func_gethostbyname = no
then]
      AC_CHECK_LIB(nsl, gethostbyname)
[fi]
dnl socket() et c on Solaris 2.
AC_CHECK_FUNC(socket)
[if test $ac_cv_func_socket = no
then]
  AC_CHECK_LIB(socket, socket)
[fi]
AC_REPLACE_FUNCS(inet_aton)
[if test $ac_cv_func_inet_aton = no
then]
    AC_CHECK_DECL([INADDR_NONE],,
	AC_DEFINE([INADDR_NONE],[((uint32_t)-1)], [Error indicator for inet_addr]),
	[#include <netinet/in.h>])
[fi]
dnl
AC_CHECK_TYPE([uint32_t],AC_DEFINE([HAVE_UINT32_T]),,[
#include <limits.h>
#ifdef HAVE_STDINT_H
#  include <stdint.h>
#endif
#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif
])
[if test $ac_cv_type_uint32_t = no
then]
    AC_CHECK_SIZEOF(int)
    AC_CHECK_SIZEOF(long)
[fi]

AC_CHECK_TYPE(intptr_t)
AC_CHECK_TYPE(intmax_t)
AC_CHECK_TYPE(size_t)

CMOD_C_WORKING_ATTRIBUTE_UNUSED

[if test -n "$GCC"; then]
    dnl "-Wtraditional" isn't really useful: we don't support
    dnl pre-c89-compilers.
    [CFLAGS="$CFLAGS -Wall -W -Wshadow"]
    CMOD_CHECK_CC_OPT([-Wbad-function-cast], [bad_function_cast])
    CMOD_CHECK_CC_OPT([-Wcast-qual], [cast_qual])
    CMOD_CHECK_CC_OPT([-Wcast-align], [cast_align])
    CMOD_CHECK_CC_OPT([-Wwrite-strings], [write_strings])
    CMOD_CHECK_CC_OPT([-Wstrict-prototypes], [strict_prototypes])
    CMOD_CHECK_CC_OPT([-Wmissing-prototypes], [missing_prototypes])
    CMOD_CHECK_CC_OPT([-Wmissing-declarations], [missing_declarations])
    CMOD_CHECK_CC_OPT([-Wnested-externs], [nested_externs])
    CMOD_CHECK_CC_OPT([-pipe], [pipe])
[fi]

dnl This test is complex.  We want to find a type that can be used
dnl as socklen_t and that gives no compiler warnings.  There is no
dnl builtin support in autoconf for this.  So we peek inside the
dnl config.log file...
dnl FIXME (bug 765): This computation should be cached.

[if test $ac_cv_type_socklen_t = no
then]
    AC_MSG_CHECKING([for socklen_t replacement type])

    [socklen_found=
    socklen_fallback=

    for socklen_type in "unsigned int" "size_t" "unsigned long"
    do]
        echo lyskomd-socklen-test-$socklen_type >&5
	AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
#include <sys/types.h>
#include <sys/socket.h>]],
[[  struct sockaddr addr;  
  $socklen_type len;
  getsockname(0, &addr, &len);]])],
[	    if sed "1,/lyskomd-socklen-test-$socklen_type/d" config.log | grep -i 'warning.*getsockname' >/dev/null
	    then
		echo warnings found, not using $socklen_type >&5
		if test -z "$socklen_fallback"
		then
		    echo retaining $socklen_type as fallback >&5
		    socklen_fallback=$socklen_type
		fi
	    else
		socklen_found=$socklen_type
		break
	    fi],
	    [echo compilation failed, not using $socklen_type >&5])
    [done
    if test -z "$socklen_found"
    then
	if test -z "$socklen_fallback"
	then	
	    socklen_fallback="unsigned int"
	fi]
	socklen_found=$socklen_fallback
	AC_MSG_RESULT([unknown (using $socklen_found)])
	AC_MSG_WARN([the choosen socklen_t may be wrong])
    [else]
	AC_MSG_RESULT([$socklen_found])
    [fi]
    AC_DEFINE_UNQUOTED([SOCKLEN_TYPE], $socklen_found, [socklen_t-compatible type])
[fi]


dnl Checks for IPv6 support

AC_CHECK_FUNCS(inet_ntop,
		,
		[if test "$use_inet6" = 'yes'; then]
		    AC_MSG_WARN(Proper decoding of IPv6	addresses requires inet_ntop)
		[fi]
		)
AC_CHECK_FUNCS(inet_pton,
		,
		[if test "$use_inet6" = 'yes'; then]
		    AC_MSG_WARN(Proper encoding of IPv6	addresses requires inet_pton)
		[fi]
		)
AC_CHECK_FUNCS(getipnodebyname gethostbyname2 freehostent)
AC_CHECK_TYPES(struct sockaddr_storage,
		      [if test "$use_inet6" = 'yes'; then]
			  AC_DEFINE(USE_INET6)
			  AC_MSG_NOTICE([IPv6 support is enabled])
			  [else]
			  AC_MSG_NOTICE([IPv6 support is disabled])
			  [fi],
		      [if test "$use_inet6" = 'yes'; then]
		          AC_MSG_ERROR([IPv6 support requires struct sockaddr_storage])
			  [fi],
		      [#include <sys/socket.h>])


[if test "$use_gcov" = "yes" -a -n "$GCC"; then]
        CMOD_CHECK_CC_OPT([-ftest-coverage], [test_coverage])
        CMOD_CHECK_CC_OPT([-fprofile-arcs], [profile_arcs])
[fi]


[if test -n "$opt_level" -a "$opt_level" != "yes" ; then
    CFLAGS=`echo "$CFLAGS" | sed "s/-O[0-9]*//"`
    if test "$opt_level" != "no" ; then
        CFLAGS="$CFLAGS -O$opt_level"
    fi
fi]

# Check for checker

[if test "$use_checker" = "yes"
then]
        AC_CHECK_PROGS(CHECKER, checker)
        [ CC="checker $CC"
          LIBS="-lchkr_m $LIBS" ]
[fi]

AC_CONFIG_FILES([
Makefile
src/Makefile
man/Makefile
doc/Makefile
demo/Makefile
])

AC_OUTPUT
