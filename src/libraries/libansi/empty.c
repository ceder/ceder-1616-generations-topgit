/*
 * $Id: empty.c,v 1.13 2003/08/23 16:38:21 ceder Exp $
 * Copyright (C) 1993, 1998-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "linkansi.h"

/* Some linkers warn if a library isn't used to resolve any symbol, so
   we include this useless function in libansi.a and use it from
   main() of all programs that may need to link with libansi.a.

   A historic note: Some systems complain if a library is empty.  This
   file was initially created as an empty C file, just so that we
   always had an object file to include in libansi.a.  But then, some
   system complained when a file contained nothing, so we added an
   external variable.  Then we found the warnings from the linker, and
   added linkansi(). */

void
link_ansi(void)
{
}
