/*
 * $Id: memcmp.c,v 1.8 2003/08/23 16:38:20 ceder Exp $
 * Copyright (C) 1996, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#include <sys/types.h>

int
memcmp (const void * s1, const void * s2, size_t n)
{
    unsigned char *uc1 = s1;
    unsigned char *uc2 = s2;
    int diff;
    for (; n > 0; n--, uc1++, uc2++)
    {
	diff = *uc1 - *uc2;
	if (diff != 0)
	    return diff;
    }

    return 0;
}
