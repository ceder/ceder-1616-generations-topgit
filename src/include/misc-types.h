/*
 * $Id: misc-types.h,v 0.13 2003/08/23 16:38:21 ceder Exp $
 * Copyright (C) 1990-1991, 1994-1996, 1999, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 *  misc-types.h  --  Miscellaneous types and constants that are useful
 *		      in many places in LysKOM, but are not very LysKOM-
 *		      specific.
 *
 *
 *  Copyright (C) 1990-1991, 1994-1996, 1999, 2003	Lysator Computer Club,
 *			Linkoping University,  Sweden
 *
 *  Everyone is granted permission to copy, modify and redistribute
 *  this code, provided the people they give it to can.
 *
 *
 *  Author:	Thomas Bellman
 *		Lysator Computer Club
 *		Linkoping University
 *		Sweden
 *
 *  email:	Bellman@Lysator.LiU.SE
 *
 */


#ifndef  MISC_TYPES_ALREADY_INCLUDED
#define  MISC_TYPES_ALREADY_INCLUDED


/* AIX 3.2 defines FALSE in <sys/types.h>. */
#ifdef FALSE
#  if FALSE != 0
#    error FALSE was defined to a non-zero value
#  endif
#  undef FALSE
#endif

#ifdef TRUE
#  if TRUE != 1
#    error TRUE was defined to a non-one value
#  endif
#  undef TRUE
#endif

/* Define TYPE_CHECK_COMPILATION to get warnings from the compiler if
   there is a mixup between Success and Bool.  This doesn't produce
   working code.  In fact, it doesn't even compile.  You will get
   several errors, since a boolean expression cannot be assigned to a
   pointer.  You have to manually check that all reported errors are
   due to the implementation below, and not because of a mixup. */
#undef TYPE_CHECK_COMPILATION

/* Define SUCCESS_AS_PTR if you want the Success type to be a pointer
   rather than an enum.  This should be a little less efficient, but
   might get more errors from the compiler.  Implied by
   TYPE_CHECK_COMPILATION. */
#define SUCCESS_AS_PTR

#if defined(TYPE_CHECK_COMPILATION)

struct kom_bool {
    short unused;
};

typedef const struct kom_bool *Bool;

extern const struct kom_bool *const TRUE;
extern const struct kom_bool *const FALSE;

#else

typedef	enum { FALSE = 0, TRUE = 1 }	Bool;

#endif




#if defined(SUCCESS_AS_PTR) || defined(TYPE_CHECK_COMPILATION)

struct success {
    int unused[1];
};

typedef const struct success *Success;

extern const struct success *const OK;
extern const struct success *const FAILURE;

#else

typedef enum { OK = 017, FAILURE = 17 }    Success;

#endif

#define NO_TIME ((time_t) 0)

#endif  /* _MISC_TYPES_ALREADY_INCLUDED__ */
