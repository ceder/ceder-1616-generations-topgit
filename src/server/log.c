/*
 * $Id: log.c,v 0.29 2003/08/23 16:38:16 ceder Exp $
 * Copyright (C) 1991-1995, 1998-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * log.c
 *
 * File created by ceder 1990-05-25.
 */



#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif
#include <stdio.h>
#include <sys/types.h>
#include "timewrap.h"
#include <unistd.h>
#include <errno.h>

#include "log.h"
#include "lyskomd.h"

/*
 * Add a string to the log file.
 */

#if defined(HAVE_VFPRINTF) && defined(HAVE_STDARG_H)

static void kom_logv(const char * format, va_list AP);

extern void
kom_log (const char * format, ...)
{
    va_list AP;

    va_start(AP, format);
    kom_logv(format, AP);
    va_end(AP);
}


static void
kom_logv (const char *format, va_list AP)
{
    time_t clk;
    struct tm *t;

    time(&clk);
    t = localtime(&clk);

    fprintf(stderr, "%02d%02d%02d %02d:%02d:%02d %ld ",
	    t->tm_year % 100, t->tm_mon + 1, t->tm_mday,
	    t->tm_hour, t->tm_min, t->tm_sec, (long)getpid());
    vfprintf(stderr, format, AP);

    fflush(stderr);
}
#else  /* !HAVE_VFPRINTF */
extern void
kom_log (const char * format, 
     int a, int b, int c, int d, int e, int f, int g)
{
    time_t clk;
    struct tm *t;

    time(&clk);
    t = localtime(&clk);

    fprintf(stderr, "%02d%02d%02d %02d:%02d:%02d %ld ",
	    t->tm_year % 100, t->tm_mon + 1, t->tm_mday,
	    t->tm_hour, t->tm_min, t->tm_sec, (long)getpid());
    fprintf(stderr, format, a, b, c, d, e, f, g);

    fflush(stderr);
    
}
#endif /* !HAVE_VFPRINTF */


void
#if defined(HAVE_VFPRINTF) && defined(HAVE_STDARG_H)
restart_kom(const char * format, ...)
#else
restart_kom(format,
	    a, b, c, d, e, f, g)
     const char *format;
     int a, b, c, d, e, f, g;
#endif
{
#if defined(HAVE_VFPRINTF) && defined(HAVE_STDARG_H)
    va_list AP;
#endif
#if defined(HAVE_GETCWD)
    char pathname[1026];
#else /* !defined(HAVE_GETCWD) */
    /* getwd should be in sys/param.h, but if we are on such an old
       system that it doesn't have getcwd, we can probably not count
       on that.  */
    char *getwd(char *);
# if defined(MAXPATHLEN)
    char pathname[MAXPATHLEN];
# else
    char pathname[1024];
# endif /* !defined(MAXPATHLEN) */
#endif /* !defined(HAVE_GETCWD) */

#if defined(HAVE_VFPRINTF) && defined(HAVE_STDARG_H)
    va_start(AP, format);
    kom_logv(format, AP);
    va_end(AP);
#else
    kom_log(format, a, b, c, d, e, f, g);
#endif

    kom_log("Previous message is fatal. Will dump core now.\n");

#ifdef HAVE_GETCWD
    /* getcwd is POSIX, so try that first. */
    if (getcwd(pathname, 1026) == NULL)
	kom_log("getcwd failed: errno %d\n", errno);
    else
	kom_log("Search for the core in %s\n", pathname);
#else
    if ( getwd(pathname) == NULL )
	kom_log("getwd failed: %s\n", pathname);
    else
	kom_log("Search for the core in %s\n", pathname);
#endif
    
#ifdef AVOID_ABORTS
    exit(255);
#else
    abort();
#endif
}
