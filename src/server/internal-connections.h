/*
 * $Id: internal-connections.h,v 0.11 2003/08/23 16:38:16 ceder Exp $
 * Copyright (C) 1991, 2002-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: internal-connections.h,v 0.11 2003/08/23 16:38:16 ceder Exp $
 *
 * internal-connections.c
 *
 * Abstract routines on the data type Connection.
 */

extern Connection *
new_client(void);

extern void
kill_client(Connection *cp);


/* Return a Connection, or NULL if the specified session doesn't exist. */
extern Connection *
get_conn_by_number (Session_no session_no);


extern Session_no
traverse_connections (Session_no session_no);

extern void
dump_allocated_connections(FILE *fp);

enum ignored_conditions
{
    ignore_dns = 1,
    ignore_ident = 2,
};

extern Bool
handshake_ok(struct connection *cptr,
	     enum ignored_conditions ignored);
