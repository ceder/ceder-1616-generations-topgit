/*
 * $Id: conf-file.h,v 1.12 2003/08/23 16:38:18 ceder Exp $
 * Copyright (C) 1994, 1998-1999, 2002-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/* 
 * Read configuration files.
 */

#ifndef CONF_FILE_INCLUDED
#define CONF_FILE_INCLUDED

struct parameter;

struct datatype {
    /* Assign a value to the parameter pointed to by par.  The value
       comes from the val. */
    Success (*assigner)(const char *val, const struct parameter *par);

    /* Constructor: initialize the parameter pointed to by par.  May
       allocate memory.  However, the memory pointed to by par must
       already be allocated.   NULL if no construction is needed. */
    void    (*ctor)(const struct parameter *par);

    /* Destructor: undo whatever the constructor did.  NULL if no
       destruction is needed. */
    void    (*dtor)(const struct parameter *par);
};

struct parameter {
    const char *name;
    const struct datatype *tp;
    int   min_assignments;
    int   max_assignments;	/* -1 == eternity */
    const char *default_val;	/* String, as read from the config file. */
    void *value;
    const char *default_suffix;	/* Default suffix for assigners that
				   cares about such things. */
};

Success read_config(const char *config_file, const struct parameter *par);
void free_config(const struct parameter *par);

extern const struct datatype cf_text_no;
extern const struct datatype cf_conf_no;
extern const struct datatype cf_int;
extern const struct datatype cf_ulong;
extern const struct datatype cf_uint;
extern const struct datatype cf_string;
extern const struct datatype cf_bool;
extern const struct datatype cf_double;
extern const struct datatype cf_timeval;

struct ipport_entry
{
    char *ipaddr;
    char *port;
};

struct ipport_list
{
    int size;
    struct ipport_entry *entries;
};

extern const struct datatype cf_ipport_list;

#endif
