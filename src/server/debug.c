/*
 * $Id: debug.c,v 1.12 2003/08/25 17:13:27 ceder Exp $
 * Copyright (C) 1991, 1993-1996, 1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * debug.c
 *
 * Testing and debugging calls.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifdef DEBUG_CALLS
#  ifdef HAVE_MALLINFO
#    include <malloc.h>
#  endif
#endif
#include <setjmp.h>
#include <unistd.h>
#include "timewrap.h"

#include "misc-types.h"
#include "kom-types.h"
#include "kom-errno.h"
#include "cache.h"
#include "async.h"
#include "com.h"
#include "connections.h"
#include "services.h"
#include "manipulate.h"
#include "log.h"
#include "lyskomd.h"


#ifdef DEBUG_CALLS

extern Success
get_memory_info (Memory_info *result)
{
    struct mallinfo info;

    CHK_CONNECTION(FAILURE);

#ifdef HAVE_MALLINFO
    info = mallinfo();
    result->arena = info.arena;
    result->ordblks = info.ordblks;
    result->smblks = info.smblks;
    result->hblks = info.hblks;
    result->hblkhd = info.hblkhd;
    result->usmblks = info.usmblks;
    result->fsmblks = info.fsmblks;
    result->uordblks = info.uordblks;
    result->fordblks = info.fordblks;
    result->keepcost = info.keepcost;
#else
    result->arena = 0;
    result->ordblks = 0;
    result->smblks = 0;
    result->hblks = 0;
    result->hblkhd = 0;
    result->usmblks = 0;
    result->fsmblks = 0;
    result->uordblks = 0;
    result->fordblks = 0;
    result->keepcost = 0;
#endif    

    return OK;
}


extern Success
set_marks (Text_no text_no,
           unsigned long no_of_marks)
{
    Text_stat *text_s;

    CHK_LOGIN(FAILURE);
    GET_T_STAT(text_s, text_no, FAILURE);

    text_s->no_of_marks = no_of_marks;
    mark_text_as_changed(text_no);
    return OK;
}

extern Success
backdate_text(Text_no text_no,
              unsigned long seconds)
{
    Text_stat *text_s;

    CHK_LOGIN(FAILURE);
    GET_T_STAT(text_s, text_no, FAILURE);

    text_s->creation_time -= seconds;
    mark_text_as_changed(text_no);

    return OK;
}

extern Success
backdate_comment_link(Text_no parent,
		      Text_no child,
		      unsigned long seconds)
{
    Text_stat *child_s;
    unsigned short nmisc;
    Misc_info 	  *misc;
    Bool           in_comm_group = FALSE;
    Bool           link_found = FALSE;

    CHK_LOGIN(FAILURE);
    GET_T_STAT(child_s, child, FAILURE);

    for (nmisc = child_s->no_of_misc, misc = child_s->misc_items;
	 nmisc > 0;
	 --nmisc, ++misc)
    {
	switch (misc->type)
	{
	case recpt:
	case cc_recpt:
	case bcc_recpt:
	case comm_in:
	case footn_in:
	    in_comm_group = FALSE;
	    break;

	case comm_to:
	case footn_to:
	    in_comm_group = (misc->datum.text_link == parent);
	    if (in_comm_group)
		link_found = TRUE;
	    break;

	case loc_no:
	case rec_time:
	case sent_by:
	    break;
	    
	case sent_at:
	    if (in_comm_group)
	    {
		misc->datum.sent_at -= seconds;
		mark_text_as_changed(child);

		return OK;
	    }
	    break;

#ifndef COMPILE_CHECKS
	default:
#endif
	case unknown_info:
	    restart_kom("backdate_comment_link(): Illegal misc-item.\n");
	}
    }

    if (link_found)
    {
	kom_errno = KOM_INDEX_OUT_OF_RANGE;
	kom_log("backdate_comment_link(): Test suite error:"
		" no sent_at found for parent=%ld, child=%ld\n",
		(unsigned long)parent, 
		(unsigned long)child);
    }
    else
    {
	kom_errno = KOM_NOT_COMMENT;
	kom_log("backdate_comment_link(): Test suite error:"
		" %ld is neither comment nor footnote to %ld.\n",
		(unsigned long)child, 
		(unsigned long)parent);
    }
    return FAILURE;
}

extern Success
server_sleep(int seconds)
{
    CHK_CONNECTION(FAILURE);
    sleep(seconds);
    return OK;
}

/* start_garb is in text-garb.c since it needs access to static variables */

#endif
