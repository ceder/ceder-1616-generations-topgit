/*
 * $Id: stderrlog.c,v 1.1 2003/10/03 07:34:05 ceder Exp $
 * Copyright (C) 1991-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif
#include <stdio.h>
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif

#include "lyskomd.h"
#include "log.h"


#if defined(HAVE_VFPRINTF) && defined(HAVE_STDARG_H)
extern void
kom_log (const char * format, ...)
{
    va_list AP;

    va_start(AP, format);

    vfprintf(stdout, format, AP);

    va_end(AP);
}
#else
extern void
kom_log (format, a, b, c, d, e, f, g)
     const char * format;
     int a, b, c, d, e, f, g;
{
    fprintf(stdout, format, a, b, c, d, e, f, g);
}
#endif

#if defined(HAVE_VFPRINTF) && defined(HAVE_STDARG_H)
extern void
restart_kom (const char * format, ...)
{
    va_list AP;

    va_start(AP, format);

    vfprintf(stdout, format, AP);

    va_end(AP);
    exit(1);
}
#else
extern void
restart_kom (format, a, b, c, d, e, f, g)
     const char * format;
     int a, b, c, d, e, f, g;
{
    fprintf(stdout, format, a, b, c, d, e, f, g);
    exit(1);
}
#endif
