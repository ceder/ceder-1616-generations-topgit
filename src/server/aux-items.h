/*
 * $Id: aux-items.h,v 1.26 2003/08/23 16:38:18 ceder Exp $
 * Copyright (C) 1994-2001, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/* Requires kom-types.h */


#define AUX_PREDEFINED_MIN      1
#define AUX_PREDEFINED_MAX      9999
#define AUX_CLIENT_SPECIFIC_MIN 10000
#define AUX_CLIENT_SPECIFIC_MAX 19999
#define AUX_EXPERIMENTAL_MIN    20000
#define AUX_EXPERIMENTAL_MAX    29999

#define AUX_IS_ILLEGAL(x) ((x) < 1)
#define AUX_IS_EXPERIMENTAL(x) ((x) >= AUX_EXPERIMENTAL_MIN && (x) <= AUX_EXPERIMENTAL_MAX)
#define AUX_IS_CLIENT_SPECIFIC(x) ((x) >= AUX_CLIENT_SPECIFIC_MIN && (x) <= AUX_CLIENT_SPECIFIC_MAX)
#define AUX_IS_PREDEFINED(x) (((x) >= AUX_PREDEFINED_MIN && (x) <= AUX_PREDEFINED_MAX) || (x) > AUX_EXPERIMENTAL_MAX)



/* Valid values for text_a etc. */

#define AUX_ITEM_ADD_ON_CREATE 1
#define AUX_ITEM_ADD_ON_MODIFY 2

/* Valid values for action trigger data field */

#define AUX_ITEM_ADD_ACTION    1
#define AUX_ITEM_DELETE_ACTION 2
#define AUX_ITEM_UNDELETE_ACTION 3

typedef struct Aux_item_definition_s Aux_item_definition;

typedef struct Aux_item_trigger_data_s
{
    short           action;
    enum object_type object_type;
    unsigned long   item_index;
    unsigned long   object_no;
    void          * object;
    Aux_item      * item;
} Aux_item_trigger_data;

typedef void (*Aux_item_trigger)(Aux_item_trigger_data *);

enum aux_item_validation_type {
    AUX_VALIDATE_FUNCTION,
    AUX_VALIDATE_REGEXP,
    AUX_VALIDATE_REJECT,
};

typedef struct
{
    const Aux_item                *item;
    const Aux_item_definition     *def;
    Connection              *creating_conn;
    Bool                     creating;
    Conf_no                  subordinate;

#if 0
    /* No validator currently use these fields, so they are
       temporarily removed.  See aux_item_add_perm() if you need to
       re-enable any of them.  */
    Aux_item_list           *add_to_list;
    unsigned long            start_looking_at;
    enum object_type         object_type;
#endif
} Aux_item_validation_data;

typedef Success (*Aux_item_validation_function)(Aux_item_validation_data *);

typedef struct
{
    enum aux_item_validation_type type;
    union
    {
        struct
        {
            char                        *regexp;
            struct re_pattern_buffer    *cached_re_buf;
        } re;
        struct
        {
            Aux_item_validation_function function;
        } fn;
    } v;
} Aux_item_validator;



typedef struct
{
    const char *name;
    Aux_item_validation_function function;
} Aux_item_validator_mapping;

typedef struct
{
    const char *name;
    Aux_item_trigger function;
} Aux_item_trigger_mapping;

struct Aux_item_definition_s
{
    char                    * name;
    unsigned long             tag;
    Aux_item_flags            clear_flags;
    Aux_item_flags            set_flags;
    Bool                      disabled;
    Bool                      author_only;
    Bool                      supervisor_only;
    Bool                      system_only;
    Bool                      one_per_person;
    Bool                      unique_data;
    Bool                      may_not_delete;
    Bool                      owner_delete;
    unsigned long             inherit_limit;
    Bool                      texts;
    short                     text_a; /* When can we add items of this type */
    Bool                      confs;
    short                     conf_a; /* When can we add items of this type */
    Bool                      letterboxes;
    Bool                      system;
    unsigned long             num_validators;
    Aux_item_validator      * validators;
    unsigned long             num_delete_triggers;
    Aux_item_trigger        * delete_triggers;
    unsigned long             num_add_triggers;
    Aux_item_trigger        * add_triggers;
    unsigned long             num_undelete_triggers;
    Aux_item_trigger        * undelete_triggers;
    struct Aux_item_definition_s *next;
};


extern Aux_item_definition empty_aux_item_definition;

/* Inerit items from parent to target. counter is a pointer to
 * highest_aux_item or equivalent. target_creator is the creator
 * of the target object (author for texts, person for persons and
 * conference for conferences */

void aux_inherit_items(Aux_item_list *target,
		       const Aux_item_list *parent,
                       unsigned long *counter,
		       Conf_no subordinate,
                       Text_no object_no,
		       Text_stat *object);



/* Prepare the items in list for addition to an object. creator is the
 * person who will add the items. The items in list will be modified.
 * You need to call this before sending the list to any of the other
 * functions. */

void prepare_aux_item_list(Aux_item_list *list, Pers_no creator);


/* Get the function pointer for a named trigger */

Aux_item_trigger aux_item_find_trigger(const char *trigger_name);

/* Get the function pointer for a named validation function */

Aux_item_validation_function
aux_item_find_validator(const char *validator_name);


/* Filter the items in ORIGINAL for sending to the person logged in on
 * connection CONN.  On entry, RESULT should be a pointer to an empty
 * aux_item_list.  It will be filled with copies of those items in
 * ORIGINAL that CONN is allowed to see.  All memory allocated in this
 * call is allocated with tmp_alloc. */

void filter_aux_item_list(const Aux_item_list *original,
                          Aux_item_list *result,
                          const Connection *conn);


/* Delete items_to_delete from list_to_delete_from.
 * list_to_delete_from and its elements may be modified. */

void delete_aux_item_list(const Number_list *items_to_delete,
                          Aux_item_list *list_to_delete_from,
                          enum object_type object_type,
                          unsigned long object_no,
                          void *object);



/* Undelete items in items_to_undelete from list_to_undelete_from.
 * list_to_delete_from and its elements may be modified. */

void undelete_aux_item_list(const Number_list *items_to_undelete,
                            Aux_item_list *list_to_undelete_from,
                            enum object_type object_type,
                            unsigned long object_no,
                            void *object);


void
commit_aux_item_list(Aux_item_list *list_to_commit);


/* Check if we may delete items_to_delete from list_to_delete_from
 * Elements in items_to_delete may be modified. Sets kom_errno,
 * err_stat and returns FAILURE if deletion of any single item is
 * not allowed */

Success check_delete_aux_item_list(const Number_list *items_to_delete,
                                   const Aux_item_list *list_to_delete_from,
                                   const Conf_no subordinate);



/* Add item_list to the aux_item_list of text_s. item_creator is the
 * person who is adding the items. Does not check for permission first. */
  
void text_stat_add_aux_item_list(Text_stat *text_s,
                                 Text_no text_no,
                                 Aux_item_list *item_list,
                                 Pers_no item_creator);



/* Check for permission to add items in ``list'' to aux_item_list of
   ``text_s''. ``creating_conn'' is the connection that gave the command that
   attempts to add the items. Sets kom_errno and err_stat and returns
   FAILURE if permission is not granted. You MUST call
   perpare_aux_item_list on the list before calling this!

   When this is called to check items when creating a text, pass NULL
   as ``text_s''.  */

Success text_stat_check_add_aux_item_list(Text_stat *text_s,
                                          Aux_item_list *list,
					  Connection *creating_conn);



/* Add an item to ``conf''.  ``conf_no'' must correspond to ``conf''.
   ``list'' is the list of items to add (NULL is interpreted as an
   empty list).  ``creating_conn'' is the connection whose permissions
   should be used.  ``creating'' should be true if the conference is
   being created.  (The ``conf'' must be non-NULL even if the
   conference is being created.)  */
Success
conf_stat_check_add_aux_item_list(Conference    *conf,
                                  Conf_no        conf_no,
                                  Aux_item_list *list,
                                  Connection    *creating_conn,
                                  Bool creating);
void
conf_stat_add_aux_item_list(Conference    *conf,
                            Conf_no        conf_no,
                            Aux_item_list *list,
                            Pers_no        creator);

void
system_add_aux_item_list(Info    *info,
                         Aux_item_list *item_list,
                         Pers_no        item_creator);

Success system_check_add_aux_item_list(Info *info,
                                       Aux_item_list *list,
                                       Connection *creating_conn);

void initialize_aux_items(char *);
void free_aux_item_definitions(void);

void aux_item_definition_add(Aux_item_definition *);

/* Parser function from aux-item-def.y. */
extern void parse_aux_item_definitions(char *file);
