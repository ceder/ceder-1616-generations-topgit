/*
 * $Id: logII.c,v 0.17 2003/08/23 16:38:16 ceder Exp $
 * Copyright (C) 1991, 1993-1995, 1998-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * log.c
 *
 * File created by ceder 1990-05-25.
 */



#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif
#include "timewrap.h"
#include <unistd.h>

enum 
{
    MESSAGE,			/* No action when message_counter is 0. */
    WARNING,			/* Sync&Restart when message_counter is 0. */
    RESTART,			/* Sync&Restart immediately. */
    CRASH			/* Don't sync, but restart immediately. */
} log_class;

    
extern void
log (enum log_class class,
     const char *file,
     const char *function,
     int	*message_counter,
     const char *format, ...)
{
    va_list AP;
    time_t clock;
    Bool log_it = TRUE;
    Bool restart = FALSE;

    va_start(AP, format);
    time(&clock);

    switch(log_class)
    {
    case MESSAGE:
	if ( message_counter != NULL && message_counter > 0 )
	    message_counter--;
	else
	    log_it = FALSE;
	break;
    case WARNING:
	if ( message_counter != NULL )
	    if ( *message_counter > 0 )
		message_counter--;
	    else
		restart = TRUE;
	break;
    case RESTART:
    case CRASH:
	break;
#ifndef COMPILE_CHECKS
    default:
	fprintf(stderr, "Deep Internal Error: log() called"
		"with log_class %d.\n", log_class);
#endif
    }

    if ( log_it == TRUE )
    {
	fprintf(stderr, "--> %-19s %-29s %s", ctime(&clock));
	vfprintf(stderr, format, AP);
    }

    if ( log_class == RESTART || (log_class == WARNING
				  && message_counter != NULL
				  && *message_counter <= 0))
    {
	fprintf(stderr, "++> Syncing.\n");
	cache_sync_all();
    }

    if ( restart == TRUE || log_class == RESTART || log_class == CRASH )
    {
	fprintf(stderr, "+++> Restarting LysKOM.\n");
/* #include "CloseFileDescriptors" */
	execl("/usr/lyskom/bin/ramkomd", "ramkomd", NULL);
	fprintf(stderr, "+++> execl() failed.\n");
	exit(1);
    }

    va_end(AP);
}


