/*
 * $Id: cache.h,v 0.28 2003/08/23 16:38:18 ceder Exp $
 * Copyright (C) 1991-1996, 1999, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * database cache header file
 */

extern struct matching_info *match_table;
/* match_table, see build_matching_info() */


/******** initiation and shutdown of cache *******************************/

/* initiate cache (called at initiation/startup of server) */
Success init_cache(void);           

/* flush, shutdown cache and free all cache memory (called at shutdown) */
void free_all_cache(void);

/* dump emergency files, etc, if possible */
void cache_emergency_exit(void);

/******** miscellanous cache master routines *****************************/

/* end-of-atomic, may clear read entries */
void cache_limit_size(void);

/*
 * Maybe save some part of the database.  sync_part() should be called
 * often as long as it returns 0.  If it returns anything else, there
 * is no need to call it again until that time has passed, but it is
 * harmless to call it more often than that.
 */
struct timeval sync_part(void);

/* flush all & wait until disk is synced */
void cache_sync_all(void);

/* tell cache that garb_text is in progress, ie never flush cache */
void tell_cache_garb_text(int running);

/* dump cache statistics etc */
void dump_cache_stats(FILE *stat_file);
void dump_cache_mem_usage(FILE *stat_file);

/******** object manipulation routines ***********************************/

/* mark as changed (object is to be re-written to disk) */
void        mark_person_as_changed(Pers_no pers);
void        mark_text_as_changed(Text_no text);
void        mark_conference_as_changed(Conf_no conf);

/* lock (do never remove object from memory) & unlock */
void        cached_lock_conf(Conf_no conf);
void        cached_unlock_conf(Conf_no conf);
void        cached_lock_person(Pers_no pers);
void        cached_unlock_person(Pers_no pers);

/* create object (returns FALSE if person existed) */
Success     cached_create_person(Pers_no pers);
Conf_no     cached_create_conf(String name);
Text_no     cached_create_text(const String message);

/* get object from database */
Person     *cached_get_person_stat(Pers_no person);
Conference *cached_get_conf_stat(Conf_no conf);
Text_stat  *cached_get_text_stat(Text_no text);
Small_conf *cached_get_small_conf_stat(Conf_no conf_no);

/* return text. String is to be freed by caller */
String      cached_get_text(Text_no text);

/* remove object from database */
Success     cached_delete_person(Pers_no pers);
Success     cached_delete_text(Text_no text);
Success     cached_delete_conf(Conf_no conf);

/* return number of next existing object */
Pers_no     traverse_person(Pers_no sead);
Conf_no     traverse_conference(Conf_no sead);
Text_no     traverse_text(Text_no sead);

/* returns highest text_num +1 */
Text_no     query_next_text_num(void);

/* returns highest conf_no +1 */
Conf_no     query_next_conf_no(void);

/******** misc conference routines **************************************/

/* Change conferance name.
   cached_change_name(foo,EMPTY_STRING);
   is used when a conference is deleted. */
void      cached_change_name(Conf_no conf,String new_name);

/* returns TRUE when a conference exists */
Bool      cached_conf_exists(Conf_no conf);

/* returns conference type.
   conf_type is set when mark_conference_as_changed is called */
Conf_type cached_get_conf_type(Conf_no conf);

/* returns supervisor of a conference.
   conf_type is set when mark_conference_as_changed is called */
Conf_no cached_get_conf_supervisor(Conf_no conf_no);

/* Get garb_nice from smallconf  */
Garb_nice cached_get_garb_nice(Conf_no conf);

/* Get keep_commented from smallconf  */
Garb_nice cached_get_keep_commented(Conf_no conf);

/* returns conference name from smallconf */
String    cached_get_name(Conf_no conf);

/* returns highes text number in a certain conference */
Local_text_no cached_get_highest_local_no(Conf_no conf);

/* match from name => conf_list_old */
Success   cached_lookup_name(const String name,Conf_list_old *result);

/* How many conferences exists?  */
Conf_no   cached_no_of_existing_conferences(void);
