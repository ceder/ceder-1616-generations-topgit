/*
 * $Id: prot-a-output.h,v 0.37 2003/08/23 16:38:15 ceder Exp $
 * Copyright (C) 1991-1992, 1994-1999, 2002-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * $Id: prot-a-output.h,v 0.37 2003/08/23 16:38:15 ceder Exp $
 *
 */
extern void
prot_a_output_ul(Connection *fp,
		 unsigned long ul);

extern void
prot_a_output_person(Connection *fp,
		     Person *person);

extern void
prot_a_output_person_old(Connection *fp,
                         Person *person);


extern void
prot_a_output_membership(Connection *fp,
			 const Membership *mship);

extern void
prot_a_output_membership_10(Connection *fp,
			    const Membership *mship);

extern void
prot_a_output_membership_old(Connection *fp,
                             const Membership *mship);

extern void
prot_a_output_membership_list(Connection *fp,
			      Membership_list mlist);

extern void
prot_a_output_membership_list_10(Connection *fp,
				 Membership_list mlist);

extern void
prot_a_output_membership_list_old(Connection *fp,
                                  Membership_list mlist);

extern void
prot_a_output_membership_type(Connection *fp,
                              const Membership_type type);

extern void
prot_a_output_conf_list(Connection *fp,
			Conf_list_old conf_list);

extern void
prot_a_output_conf_no_list(Connection *fp,
			   Conf_no_list conf_no_list);

extern void
prot_a_output_conference(Connection *fp,
			 Conference *conf_c);

extern void
prot_a_output_conference_old(Connection *fp,
                             Conference *conf_c);

extern void
prot_a_output_uconference(Connection *fp,
			  Small_conf *conf_c);

extern void
prot_a_output_mark_list(Connection *fp,
			Mark_list mark_list);

extern void
prot_a_output_aux_item_flags(Connection *fp,
                             Aux_item_flags flags);

extern void
prot_a_output_aux_item(Connection *fp,
                       Aux_item *item);

extern void
prot_a_output_aux_item_list(Connection *fp,
                            Aux_item_list *aux);

extern void
prot_a_output_text_stat_old(Connection *fp,
                            Text_stat *t_stat);

extern void
prot_a_output_text_stat(Connection *fp,
			Text_stat *t_stat);

extern void
prot_a_output_info(Connection *fp,
		   Info *info);

extern void
prot_a_output_info_old(Connection *fp,
                       Info *info);

void
prot_a_output_who_info(Connection *fp,
		       Who_info *info);

extern void
prot_a_output_who_info_list(Connection *fp,
			    Who_info_list info);

extern void
prot_a_output_who_info_ident_list(Connection *fp,
				  Who_info_ident_list info);

extern void
prot_a_output_who_info_list_old(Connection *fp,
				Who_info_list_old info);

void
prot_a_output_session_info(Connection *fp,
			   Session_info *info);

void
prot_a_output_session_info_ident(Connection *fp,
				 Session_info_ident *info);

extern void
prot_a_output_string(Connection *fp,
		     String str);

extern void
prot_a_output_priv_bits(Connection *fp,
			Priv_bits bits);

extern void
prot_a_output_personal_flags(Connection *fp,
			     Personal_flags flags);


extern void
prot_a_output_conf_type(Connection *fp,
			Conf_type type);

extern void
prot_a_output_extended_conf_type(Connection *fp,
				 Conf_type type);

extern void
prot_a_output_member_list(Connection *fp,
			  Member_list m_list);

extern void
prot_a_output_member_list_old(Connection *fp,
                              Member_list m_list);

extern void
prot_a_output_mark(Connection *fp,
		   Mark mark);

extern void
prot_a_output_misc_info(Connection *fp, 
			Misc_info misc);

extern void
prot_a_output_member(Connection *fp,
		     Member member);

extern void
prot_a_output_member_old(Connection *fp,
                         Member member);

void
prot_a_output_time(Connection *fp,
		   time_t clk);

void
prot_a_output_session_no(Connection *fp,
			 Session_no session_no);

void
prot_a_output_text_no(Connection *fp,
		      Text_no text);

void
prot_a_output_conf_no(Connection *fp,
		      Conf_no conf);

void
prot_a_output_conf_z_info_list(Connection *fp,
			       Conf_z_info_list c_list);

void
prot_a_output_version_info (Connection  *fp,
			    Version_info *v_info);

void
prot_a_output_num_list (Connection *fp,
                        Number_list *num_list);

void
prot_a_output_static_session_info (Connection *fp,
				   Static_session_info *info);

void
prot_a_output_dynamic_session_info_list (Connection *fp,
					 Dynamic_session_info_list *list);
void
prot_a_output_l2g_iterator_as_text_list(Connection *fp,
					L2g_iterator *list);

void
prot_a_output_text_mapping(Connection *fp,
			   Text_mapping *result);

void
prot_a_output_text_mapping_reverse(Connection *fp,
				   Text_mapping_reverse *map);

void
prot_a_output_stats_description(Connection *fp,
				Stats_description *result);

void
prot_a_output_stats_list(Connection *fp,
			 const Stats_list *result);

void
prot_a_output_static_server_info(Connection *fp,
				 const Static_server_info *result);

void
prot_a_output_scheduling_info(Connection *fp,
			      const Scheduling_info *result);

#ifdef DEBUG_CALLS
void
prot_a_output_memory_info(Connection *fp,
                          Memory_info *result);
#endif
