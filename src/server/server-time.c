/*
 * The current time is kept in a global variable.
 * Copyright (C) 1999, 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stddef.h>
#include <errno.h>
#include <string.h>
#include "timewrap.h"

#include "misc-types.h"
#include "timeval-util.h"
#include "server-time.h"
#include "log.h"
#include "lyskomd.h"

struct timeval current_time = { 0, 0 };

void
set_initial_time(void)
{
    if (gettimeofday(&current_time, NULL) < 0)
	restart_kom("gettimeofday failed: %s\n", strerror(errno));
}

void
set_time(void)
{
    struct timeval last_time;
    static int limiter = 0;

    last_time = current_time;
    if (gettimeofday(&current_time, NULL) < 0)
    {
	if (limiter < 50)
	{
	    kom_log("WARNING: gettimeofday failed: %s\n", strerror(errno));
	    if (++limiter == 50)
		kom_log("WARNING: will not log the above message again.\n");
	}
    }

    if (timeval_less(current_time, last_time))
    {
	kom_log("WARNING: Time moved backward at least %g seconds.\n",
		timeval_diff_d(last_time, current_time));
	/* FIXME (bug 62): Should we take more decisive action here? */
    }
}
