/*
 * Copyright (C) 2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

/*
 * stats.h -- Handle statistical information, such as load averages.
 */

enum stat_type
{
    STAT_RUN_QUEUE,
    STAT_DNS_QUEUE,
    STAT_IDENT_QUEUE,
    STAT_CLIENTS,
    STAT_REQUESTS,
    STAT_TEXTS,
    STAT_CONFS,			/* Including letterboxes! */
    STAT_PERSONS,
    STAT_SEND_QUEUE,
    STAT_RECV_QUEUE,

    NUM_STAT
};

extern void init_stats(void);
extern void update_stat(enum stat_type st, long delta);
extern long read_stat_value(enum stat_type st);
