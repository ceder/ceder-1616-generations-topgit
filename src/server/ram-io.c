/* Database file handling.
 * Copyright (C) 1991, 1993-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <errno.h>
#include <stdio.h>
#include <assert.h>
#include "timewrap.h"

#include "server/smalloc.h"
#include "eintr.h"
#include "kom-types.h"
#include "ram-io.h"
#include "ram-parse.h"
#include "ram-output.h"
#include "lyskomd.h"
#include "log.h"

/* The native output format is defined here. */
static int output_format = 3;

static long nr_dbfile_objects = 0;
static long nr_dbfile_files = 0;

static struct dbfile *
dbfile_new(void)
{
    struct dbfile *res = smalloc(sizeof(struct dbfile));
    res->fp = NULL;

    res->format = output_format;
    ++nr_dbfile_objects;
    return res;
}

int
dbfile_delete(struct dbfile *obj)
{
    int rv = 0;
    int errno_save = errno;
    
    if (obj->fp != NULL)
    {
	rv = i_fclose(obj->fp);
	errno_save = errno;
	--nr_dbfile_files;
    }

    sfree(obj);
    errno = errno_save;
    --nr_dbfile_objects;
    return rv;
}

void
set_output_format(int fmt)
{
    output_format = fmt;
    switch (fmt)
    {
    case 0:
    case 1:
    case 2:
    case 3:
        break;
    default:
        restart_kom("unknown output format selected: %d\n", fmt);
        break;
    }   
}

static int
get_version(struct dbfile *fp)
{
    int version;
    
    fparse_set_pos(fp, 5);
    if (dbfile_getc(fp) == '\n')
    {
	return 0;
    }
    version = fparse_long(fp);

    return version;
}

struct dbfile *
dbfile_open_read(const char *filename)
{
    struct dbfile *res = dbfile_new();
    res->fp = i_fopen(filename, "rb");
    if (res->fp == NULL)
    {
	int saved_errno = errno;
	dbfile_delete(res);
	errno = saved_errno;
	return NULL;
    }
    ++nr_dbfile_files;
    res->format = get_version(res);
    switch(res->format)
    {
    case 0:
    case 1:
    case 2:
    case 3:
	break;
    default:
        restart_kom("unknown input format selected: %d\n", res->format);
        break;
    }

    return res;
}

struct dbfile *
dbfile_open_write(const char *filename,
		  const char *magic)
{
    struct dbfile *res = dbfile_new();
    if (res == NULL)
	return NULL;

    if ((res->fp = i_fopen(filename, "wb")) == NULL)
    {
	int saved_errno = errno;
	dbfile_delete(res);
	errno = saved_errno;
	return NULL;
    }

    foutput_header(res, magic, 1);

    ++nr_dbfile_files;
    return res;
}

Success
dbfile_change_magic(struct dbfile *fp,
		    const char *magic)
{
    rewind(fp->fp);
    if (dbfile_ferror(fp) != 0)
    {
	kom_log("dbfile_change_magic: rewind failed.\n");
	return FAILURE;
    }

    foutput_header(fp, magic, 0);

    if (dbfile_ferror(fp) != 0)
    {
	kom_log("dbfile_change_magic: writing new header %s failed.\n",
		magic);
	return FAILURE;
    }

    if (fflush(fp->fp) != 0)
    {
	kom_log("dbfile_change_magic(): fflush failed.\n");
	return FAILURE;
    }

    if (dbfile_ferror(fp) != 0)
    {
	kom_log("dbfile_change_magic(): ferror after fflush failed.\n");
	return FAILURE;
    }

    return OK;
}


void
dump_dbfile_stats(FILE *fp)
{
    fprintf(fp, "---%s:\n\tExisting dbfile objects:    %ld\n",
	    __FILE__, nr_dbfile_objects);
    fprintf(fp, "\tExisting dbfile files:    %ld\n", nr_dbfile_files);
}

long
dbfile_ftell(struct dbfile *fp)
{
    return ftell(fp->fp);
}

int
dbfile_getc(struct dbfile *fp)
{
    return getc(fp->fp);
}

int
dbfile_putc(int c,
	    struct dbfile *fp)
{
    return putc(c, fp->fp);
}

int
dbfile_fputs(const char *s,
	     struct dbfile *fp)
{
    return fputs(s, fp->fp);
}

int
dbfile_ungetc(struct dbfile *fp,
	      int c)
{
    return ungetc(c, fp->fp);
}

int
dbfile_feof(struct dbfile *fp)
{
    return feof(fp->fp);
}

int
dbfile_ferror(struct dbfile *fp)
{
    return ferror(fp->fp);
}
