;;; Renumber Protocol A test cases.

;; Copyright (C) 1998, 2002-2003  Lysator Academic Computer Association.
;;
;; This file is part of the LysKOM server.
;; 
;; LysKOM is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by 
;; the Free Software Foundation; either version 1, or (at your option) 
;; any later version.
;; 
;; LysKOM is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with LysKOM; see the file COPYING.  If not, write to
;; Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
;; or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
;; MA 02139, USA.
;;
;; Please report bugs at http://bugzilla.lysator.liu.se/.

;;;###autoload
(defun renumber-lyskom-send-simple-expect ()
  (interactive)
  (goto-char (point-min))
  (let ((ref-nr 999)
	(case-fold-search nil))
    (while (re-search-forward "^send \"\\([0-9]+\\) " nil t)
      (setq ref-nr (+ 1 ref-nr))
      (replace-match (format "%d" ref-nr) t t nil 1)
      (let ((pos (point))
	    (limit (re-search-forward "^\\(send\\)\\|\\(talk_to\\)" nil t)))
	(goto-char pos)
	(while
	    (re-search-forward
	     "^\\(simple\\|extracting\\|good_bad\\)_expect \"[%=]\\([0-9]+\\)"
	     limit t)
	  (replace-match (format "%d" ref-nr) t t nil 2))))))

;;;###autoload
(defun renumber-lyskom-send-simple-expect-indented ()
  (interactive)
  (goto-char (point-min))
  (let ((ref-nr 1999)
	(case-fold-search nil))
    (while (re-search-forward "^  send \"\\([0-9]+\\) " nil t)
      (setq ref-nr (+ 1 ref-nr))
      (replace-match (format "%d" ref-nr) t t nil 1)
      (let ((pos (point))
	    (limit (re-search-forward "^  \\(send\\)\\|\\(talk_to\\)" nil t)))
	(goto-char pos)
	(while
	    (re-search-forward
	     "^  \\(simple\\|extracting\\|good_bad\\)_expect \"[%=]\\([0-9]+\\)"
	     limit t)
	  (replace-match (format "%d" ref-nr) t t nil 2))))))
