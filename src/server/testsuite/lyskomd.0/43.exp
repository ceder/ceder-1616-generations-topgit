# Test suite for lyskomd.
# Copyright (C) 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test async-text-aux-changed.
#
# Persons:
#     5: admin
#     6: author
#     7: bystander

lyskomd_start

# Log in as admin.
client_start 0
send "A[holl "foo"]\n"
simple_expect "LysKOM"
send "1000 80 1 { 22 }\n"
simple_expect "=1000"
send "1001 0 5 [holl "gazonk"]\n"
simple_expect "=1001"

# Log in as "author"
client_start 1
send "A[holl "foo"]\n"
simple_expect "LysKOM"
send "1002 80 1 { 22 }\n"
simple_expect "=1002"
send "1003 89 [holl "author"] [holl "author"] 00000000 0 { }\n"
simple_expect "=1003 6"
send "1004 0 6 [holl "author"]\n"
simple_expect "=1004"

# Log in as "bystander"
client_start 2
send "A[holl "foo"]\n"
simple_expect "LysKOM"
send "1005 80 1 { 22 }\n"
simple_expect "=1005"
send "1006 89 [holl "bystander"] [holl "bystander"] 00000000 0 { }\n"
simple_expect "=1006 7"
send "1007 0 7 [holl "bystander"]\n"
simple_expect "=1007"

# admin and author joins conference 1.
talk_to client 0
send "1008 100 1 5 200 1 00000000\n"
simple_expect "=1008"
talk_to client 1
send "1009 100 1 6 200 1 00000000\n"
simple_expect "=1009"

# Create a text.
send "1010 86 [holl "text 1"] 1 { 0 1 } 0 { }\n"
simple_expect "=1010 1"

# Add an aux-item to the text.  We use fast-reply (type 2).
send "1011 92 1 0 { } 1 { 2 00000000 1 [holl "I agree."] }\n"
simple_expect ":5 22 1 0 \\* 1 { 1 2 6 $any_time 00000000 1 [holl "I agree."] }"
simple_expect "=1011"

client_expect 0 ":5 22 1 0 \\* 1 { 1 2 6 $any_time 00000000 1 [holl "I agree."] }"

# Delete the aux-item.
send "1012 92 1 1 { 1 } 0 { }\n"
simple_expect ":5 22 1 1 { 1 2 6 $any_time 10000000 1 [holl "I agree."] } 0 \\*"
simple_expect "=1012"

client_expect 0 ":5 22 1 1 { 1 2 6 $any_time 10000000 1 [holl "I agree."] } 0 \\*"

# Add two fast-replies.
send "1013 92 1 0 { } 2 { 2 00000000 1 [holl "I don't disagree."] 2 00000000 1 [holl "I see your point."] }\n"
simple_expect ":5 22 1 0 \\* 2 { 2 2 6 $any_time 00000000 1 [holl "I don't disagree."] 3 2 6 $any_time 00000000 1 [holl "I see your point."] }"
simple_expect "=1013"

client_expect 0 ":5 22 1 0 \\* 2 { 2 2 6 $any_time 00000000 1 [holl "I don't disagree."] 3 2 6 $any_time 00000000 1 [holl "I see your point."] }"

# Delete one of the aux-items.
send "1014 92 1 1 { 2 } 0 { }\n"
simple_expect ":5 22 1 1 { 2 2 6 $any_time 10000000 1 [holl "I don't disagree."] } 0 \\*"
simple_expect "=1014"

client_expect 0 ":5 22 1 1 { 2 2 6 $any_time 10000000 1 [holl "I don't disagree."] } 0 \\*"

# Add a fast-reply, a secret item, and an anonymous item,
# and remove the old item.
send "1015 92 1 1 { 3 } 3 { 2 00000000 1 [holl "You've gotta be kidding."] 10201 00100000 1 [holl "private."] 10202 00010000 1 [holl "anon."] }\n"

simple_expect ":5 22 1 1 { 3 2 6 $any_time 10000000 1 [holl "I see your point."] } 3 { 4 2 6 $any_time 00000000 1 [holl "You've gotta be kidding."] 5 10201 6 $any_time 00100000 1 [holl "private."] 6 10202 6 $any_time 00010000 1 [holl "anon."] }"

simple_expect "=1015"

# As can be seen below, it isn't very clever to create a hide-creator
# and a normal aux-item at the same time.  They will be sent in the
# same message.  That they are added at the same time would anyhow be
# a dead giveaway.
client_expect 0 ":5 22 1 1 { 3 2 6 $any_time 10000000 1 [holl "I see your point."] } 2 { 4 2 6 $any_time 00000000 1 [holl "You've gotta be kidding."] 6 10202 0 $any_time 00010000 1 [holl "anon."] }"

# Delete the private item.
send "1016 92 1 1 { 5 } 0 { }\n"
simple_expect ":5 22 1 1 { 5 10201 6 $any_time 10100000 1 [holl "private."] } 0 \\*"
simple_expect "=1016"

# Delete the anon item.
send "1017 92 1 1 { 6 } 0 { }\n"
simple_expect ":5 22 1 1 { 6 10202 6 $any_time 10010000 1 [holl "anon."] } 0 \\*"
simple_expect "=1017"

client_expect 0 ":5 22 1 1 { 6 10202 0 $any_time 10010000 1 [holl "anon."] } 0 \\*"

# Through all of this, person 7 should not have received a single
# async message.
talk_to client 2
send "1018 35\n"
simple_expect "=1018 $any_time"

# Shut down.
talk_to client 0
send "1019 42 255\n"
simple_expect "=1019"
send "1020 44 0\n"
simple_expect "=1020"
client_death 0
client_death 1
client_death 2

lyskomd_death

