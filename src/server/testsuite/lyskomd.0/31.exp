# Test suite for lyskomd.
# Copyright (C) 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 



# Test large text- and conference numbers.  Test what happens when we
# hit the "Max texts" and "Max conferences" limits.

# 1: Hit the limits with 86=create-text and 88=create-conf

lyskomd_start "" "Max texts: 2
Max conferences: 6"

client_start 0
talk_to client 0
send "A\n"
send "0H\n"
simple_expect "LysKOM"
send "1000 62 5 6Hgazonk 0\n"
simple_expect ":2 9 5 1"
simple_expect "=1000"

send "1001 86 [holl "text 1"] 1 { 0 1 } 0 { }\n"
simple_expect "=1001 1"
send "1002 86 [holl "text 2"] 1 { 0 1 } 0 { }\n"
simple_expect "=1002 2"
send "1003 86 [holl "text 3"] 1 { 0 1 } 0 { }\n"
simple_expect "%1003 19 3"
send "1004 86 [holl "text 4"] 1 { 0 1 } 0 { }\n"
simple_expect "%1004 19 3"
send "1005 86 [holl "text 5"] 1 { 0 1 } 0 { }\n"
simple_expect "%1005 19 3"
send "1006 28 [holl "text 6"] 1 { 0 1 }\n"
simple_expect "%1006 19 3"

send "1007 88 [holl "conf 6"] 0000 0 { }\n"
simple_expect "=1007 6"
send "1008 88 [holl "conf 7"] 0000 0 { }\n"
lyskomd_expect "ERROR: Couldn't create conference\\. Too many conferences\\."
simple_expect "%1008 19 7"
send "1009 88 [holl "conf 8"] 0000 0 { }\n"
lyskomd_expect "ERROR: Couldn't create conference\\. Too many conferences\\."
simple_expect "%1009 19 7"
send "1010 5 [holl "person 9"] [holl "secret"]\n"
lyskomd_expect "ERROR: Couldn't create person\\. Too many conferences\\."
simple_expect "%1010 19 7"
send "1011 10 [holl "conf 10"] 0000\n"
lyskomd_expect "ERROR: Couldn't create conference\\. Too many conferences\\."
simple_expect "%1011 19 7"
send "1012 89 [holl "person 11"] [holl "secret"] 00000000 0 { }\n"
lyskomd_expect "ERROR: Couldn't create person\\. Too many conferences\\."
simple_expect "%1012 19 7"

system "kill -HUP $lyskomd_pid"
client_death 0
lyskomd_death "" sighup

# 2: Hit the limits with 28=create-text-old and 10=create-conf-old

lyskomd_start "" "Max texts: 2
Max conferences: 6"

client_start 0
talk_to client 0
send "A\n"
send "0H\n"
simple_expect "LysKOM"
send "1013 62 5 6Hgazonk 0\n"
simple_expect ":2 9 5 1"
simple_expect "=1013"

send "1014 28 [holl "text 1"] 1 { 0 1 }\n"
simple_expect "=1014 1"
send "1015 28 [holl "text 2"] 1 { 0 1 }\n"
simple_expect "=1015 2"
send "1016 28 [holl "text 3"] 1 { 0 1 }\n"
simple_expect "%1016 19 3"
send "1017 28 [holl "text 4"] 1 { 0 1 }\n"
simple_expect "%1017 19 3"
send "1018 86 [holl "text 5"] 1 { 0 1 } 0 { }\n"
simple_expect "%1018 19 3"

send "1019 10 [holl "conf 6"] 0000\n"
simple_expect "=1019 6"
send "1020 10 [holl "conf 7"] 0000\n"
lyskomd_expect "ERROR: Couldn't create conference\\. Too many conferences\\."
simple_expect "%1020 19 7"
send "1021 88 [holl "conf 7"] 0000 0 { }\n"
lyskomd_expect "ERROR: Couldn't create conference\\. Too many conferences\\."
simple_expect "%1021 19 7"
send "1022 88 [holl "conf 8"] 0000 0 { }\n"
lyskomd_expect "ERROR: Couldn't create conference\\. Too many conferences\\."
simple_expect "%1022 19 7"
send "1023 5 [holl "person 9"] [holl "secret"]\n"
lyskomd_expect "ERROR: Couldn't create person\\. Too many conferences\\."
simple_expect "%1023 19 7"
send "1024 10 [holl "conf 10"] 0000\n"
lyskomd_expect "ERROR: Couldn't create conference\\. Too many conferences\\."
simple_expect "%1024 19 7"
send "1025 89 [holl "person 11"] [holl "secret"] 00000000 0 { }\n"
lyskomd_expect "ERROR: Couldn't create person\\. Too many conferences\\."
simple_expect "%1025 19 7"

system "kill -TERM $lyskomd_pid"
client_death 0
lyskomd_death "" signal

# 3: Hit the limit with 5=create-person-old

lyskomd_start "" "Max texts: 2
Max conferences: 6"

client_start 0
talk_to client 0
send "A\n"
send "0H\n"
simple_expect "LysKOM"
send "1026 62 5 6Hgazonk 0\n"
simple_expect ":2 9 5 1"
simple_expect "=1026"

send "1027 5 [holl "person 6"] [holl "secret"]\n"
simple_expect "=1027 6"
send "1028 5 [holl "person 7"] [holl "secret"]\n"
lyskomd_expect "ERROR: Couldn't create person\\. Too many conferences\\."
simple_expect "%1028 19 7"
send "1029 5 [holl "person 8"] [holl "secret"]\n"
lyskomd_expect "ERROR: Couldn't create person\\. Too many conferences\\."
simple_expect "%1029 19 7"
send "1030 10 [holl "conf 7"] 0000\n"
lyskomd_expect "ERROR: Couldn't create conference\\. Too many conferences\\."
simple_expect "%1030 19 7"
send "1031 88 [holl "conf 7"] 0000 0 { }\n"
lyskomd_expect "ERROR: Couldn't create conference\\. Too many conferences\\."
simple_expect "%1031 19 7"
send "1032 88 [holl "conf 8"] 0000 0 { }\n"
lyskomd_expect "ERROR: Couldn't create conference\\. Too many conferences\\."
simple_expect "%1032 19 7"
send "1033 5 [holl "person 9"] [holl "secret"]\n"
lyskomd_expect "ERROR: Couldn't create person\\. Too many conferences\\."
simple_expect "%1033 19 7"
send "1034 10 [holl "conf 10"] 0000\n"
lyskomd_expect "ERROR: Couldn't create conference\\. Too many conferences\\."
simple_expect "%1034 19 7"
send "1035 89 [holl "person 11"] [holl "secret"] 00000000 0 { }\n"
lyskomd_expect "ERROR: Couldn't create person\\. Too many conferences\\."
simple_expect "%1035 19 7"

system "kill -INT $lyskomd_pid"
client_death 0
lyskomd_death "" sigint

# 4: Hit the limit with 89=create-person

lyskomd_start "" "Max texts: 2
Max conferences: 6"

client_start 0
talk_to client 0
send "A\n"
send "0H\n"
simple_expect "LysKOM"
send "1036 62 5 6Hgazonk 0\n"
simple_expect ":2 9 5 1"
simple_expect "=1036"

send "1037 89 [holl "person 6"] [holl "secret"] 00000000 0 { }\n"
simple_expect "=1037 6"
send "1038 89 [holl "person 7"] [holl "secret"] 00000000 0 { }\n"
lyskomd_expect "ERROR: Couldn't create person\\. Too many conferences\\."
simple_expect "%1038 19 7"
send "1039 89 [holl "person 8"] [holl "secret"] 00000000 0 { }\n"
lyskomd_expect "ERROR: Couldn't create person\\. Too many conferences\\."
simple_expect "%1039 19 7"
send "1040 5 [holl "person 7"] [holl "secret"]\n"
lyskomd_expect "ERROR: Couldn't create person\\. Too many conferences\\."
simple_expect "%1040 19 7"
send "1041 5 [holl "person 8"] [holl "secret"]\n"
lyskomd_expect "ERROR: Couldn't create person\\. Too many conferences\\."
simple_expect "%1041 19 7"
send "1042 10 [holl "conf 7"] 0000\n"
lyskomd_expect "ERROR: Couldn't create conference\\. Too many conferences\\."
simple_expect "%1042 19 7"
send "1043 88 [holl "conf 7"] 0000 0 { }\n"
lyskomd_expect "ERROR: Couldn't create conference\\. Too many conferences\\."
simple_expect "%1043 19 7"
send "1044 88 [holl "conf 8"] 0000 0 { }\n"
lyskomd_expect "ERROR: Couldn't create conference\\. Too many conferences\\."
simple_expect "%1044 19 7"
send "1045 5 [holl "person 9"] [holl "secret"]\n"
lyskomd_expect "ERROR: Couldn't create person\\. Too many conferences\\."
simple_expect "%1045 19 7"
send "1046 10 [holl "conf 10"] 0000\n"
lyskomd_expect "ERROR: Couldn't create conference\\. Too many conferences\\."
simple_expect "%1046 19 7"
send "1047 89 [holl "person 11"] [holl "secret"] 00000000 0 { }\n"
lyskomd_expect "ERROR: Couldn't create person\\. Too many conferences\\."
simple_expect "%1047 19 7"

system "kill -TERM $lyskomd_pid"
client_death 0
lyskomd_death "" signal
