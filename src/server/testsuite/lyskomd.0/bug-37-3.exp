# Test suite for lyskomd.
# Copyright (C) 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test bug 37: get_unread_confs does not check that the caller is
# allowed to see the person, and returns the correct result even for
# secret persons, including secret confs!
#
# Variant three: the observer is supervisor of all conferences.
#
# We also check get-membership, get-membership-10, get-membership-old,
# query-read-texts, query-read-texts-10, and query-read-texts-old.
#
# While we are at it, we also test the unread-is-secret personal flag.

#
# Startup and create the players
#

# Start the server.
lyskomd_start

# Create and log in as foo (person 6).
client_start 0
send "A3Hfoo\n"
simple_expect "LysKOM"
send "1000 80 0 { }\n"
simple_expect "=1000"
send "1001 89 [holl "foo"] [holl "p6"] 00000000 0 { }\n"
simple_expect "=1001 6"
send "1002 62 6 [holl "p6"] 0\n"
simple_expect "=1002"

# Create and log in as bar (person 7; secret).
client_start 1
send "A3Hbar\n"
simple_expect "LysKOM"
send "1003 80 0 { }\n"
simple_expect "=1003"
send "1004 89 [holl "bar"] [holl "p7"] 00000000 0 { }\n"
simple_expect "=1004 7"
send "1005 62 7 [holl "p7"] 0\n"
simple_expect "=1005"
send "1006 21 7 10111000\n"
simple_expect "=1006"

# Create and log in as citrus (person 8; has unread-is-secret set).
client_start 2
send "A6Hcitrus\n"
simple_expect "LysKOM"
send "1007 80 0 { }\n"
simple_expect "=1007"
send "1008 89 [holl "citrus"] [holl "p8"] 10000000 0 { }\n"
simple_expect "=1008 8"
send "1009 62 8 [holl "p8"] 0\n"
simple_expect "=1009"

# Create and log in as gazonk (person 9).  This is the observer.
client_start 3
send "A6Hgazonk\n"
simple_expect "LysKOM"
send "1010 80 0 { }\n"
simple_expect "=1010"
send "1011 89 [holl "gazonk"] [holl "p9"] 00000000 0 { }\n"
simple_expect "=1011 9"
send "1012 62 9 [holl "p9"] 0\n"
simple_expect "=1012"

#
# Create conferences.
#

# As gazonk, create secret (conf 10; secret).
talk_to client 3
send "1013 88 [holl "secret"] 10101000 0 { }\n"
simple_expect "=1013 10"

# As gazonk, create rd-prot (conf 11; rd-prot).
send "1014 88 [holl "rd-prot"] 10001000 0 { }\n"
simple_expect "=1014 11"

# As gazonk, create public (conf 12).
send "1015 88 [holl "public"] 00001000 0 { }\n"
simple_expect "=1015 12"

#
# Join conferences
#

# gazonk invites bar to secret, rd-prot and public.
send "1016 100 10 7 200 1 00000000\n"
lyskomd_expect "Person 7 added to conference 10 by 9."
simple_expect "=1016"
send "1017 100 11 7 200 2 00000000\n"
lyskomd_expect "Person 7 added to conference 11 by 9."
simple_expect "=1017"
send "1018 100 12 7 200 3 00000000\n"
lyskomd_expect "Person 7 added to conference 12 by 9."
simple_expect "=1018"
# bar is secret, so gazonk should not be able to do the above.
# When bug 604 is fixed, this test should be rewritten so that
# bar is temporarily made public while being invited.
setup_xfail "*-*-*" "Bug 604"
fail "bar could add a secret person to a conference"

# gazonk invites foo to secret, rd-prot and public.
send "1019 100 10 6 200 1 00000000\n"
lyskomd_expect "Person 6 added to conference 10 by 9."
simple_expect "=1019"
send "1020 100 11 6 200 2 00000000\n"
lyskomd_expect "Person 6 added to conference 11 by 9."
simple_expect "=1020"
send "1021 100 12 6 200 3 00000000\n"
lyskomd_expect "Person 6 added to conference 12 by 9."
simple_expect "=1021"

# gazonk invites citrus to secret, rd-prot and public.
send "1022 100 10 8 200 1 00000000\n"
lyskomd_expect "Person 8 added to conference 10 by 9."
simple_expect "=1022"
send "1023 100 11 8 200 2 00000000\n"
lyskomd_expect "Person 8 added to conference 11 by 9."
simple_expect "=1023"
send "1024 100 12 8 200 3 00000000\n"
lyskomd_expect "Person 8 added to conference 12 by 9."
simple_expect "=1024"

#
# foo examines foo.
#

talk_to client 0

# foo does get-unread-confs of foo.  Should return the empty list.
send "1025 52 6\n"
simple_expect "=1025 0 \\*"

# foo does get-membership-10 of foo.  Should return 4 { 6 10 11 12 }.
send "1026 99 6 0 100 1\n"
simple_expect "=1026 4 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 10 200 0 0 \\* 9 $any_time 10000000 2 $any_time 11 200 0 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000 }"

# foo does get-membership-old of foo.  Should return 4 { 6 10 11 12 }.
send "1027 46 6 0 100 1\n"
simple_expect "=1027 4 { $any_time 6 255 0 0 \\* $any_time 10 200 0 0 \\* $any_time 11 200 0 0 \\* $any_time 12 200 0 0 \\* }"

# foo does get-membership of foo.  Should return 4 { 6 10 11 12 }.
send "1028 108 6 0 100 1 0\n"
simple_expect "=1028 4 { 0 $any_time 6 255 0 \\* 6 $any_time 00000000 1 $any_time 10 200 0 \\* 9 $any_time 10000000 2 $any_time 11 200 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 \\* 9 $any_time 10000000 }"

# foo does query-read-texts-10 on foo and 10, 11 and 12.  Should get results.
send "1029 98 6 10\n"
simple_expect "=1029 1 $any_time 10 200 0 0 \\* 9 $any_time 10000000"
send "1030 98 6 11\n"
simple_expect "=1030 2 $any_time 11 200 0 0 \\* 9 $any_time 10000000"
send "1031 98 6 12\n"
simple_expect "=1031 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000"

# foo does query-read-texts-old on foo and 10, 11 and 12.  Should get results.
send "1032 9 6 10\n"
simple_expect "=1032 $any_time 10 200 0 0 \\*"
send "1033 9 6 11\n"
simple_expect "=1033 $any_time 11 200 0 0 \\*"
send "1034 9 6 12\n"
simple_expect "=1034 $any_time 12 200 0 0 \\*"

# foo does query-read-texts on foo and 10, 11 and 12.  Should get results.
send "1035 107 6 10 1 0\n"
simple_expect "=1035 1 $any_time 10 200 0 \\* 9 $any_time 10000000"
send "1036 107 6 11 1 0\n"
simple_expect "=1036 2 $any_time 11 200 0 \\* 9 $any_time 10000000"
send "1037 107 6 12 1 0\n"
simple_expect "=1037 3 $any_time 12 200 0 \\* 9 $any_time 10000000"

#
# gazonk examines foo.
#

talk_to client 3

# gazonk does get-unread-confs of foo.  Should return the empty list.
send "1038 52 6\n"
simple_expect "=1038 0 \\*"

# gazonk does get-membership-10 of foo.  Should return 4 { 6 10 11 12 }.
send "1039 99 6 0 100 1\n"
simple_expect "=1039 4 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 10 200 0 0 \\* 9 $any_time 10000000 2 $any_time 11 200 0 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000 }"
# (Just a quick check: does the renumber work properly even if we don't
# start from the beginning of the list?)  (Hmm.  No renumber in this
# variant.  Oh well.)
send "1040 99 6 2 100 1\n"
simple_expect "=1040 2 { 2 $any_time 11 200 0 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000 }"

# gazonk does get-membership-old of foo.  Should return 4 { 6 10 11 12 }.
send "1041 46 6 0 100 1\n"
simple_expect "=1041 4 { $any_time 6 255 0 0 \\* $any_time 10 200 0 0 \\* $any_time 11 200 0 0 \\* $any_time 12 200 0 0 \\* }"

# gazonk does get-membership of foo.  Should return 4 { 6 10 11 12 }.
send "1042 108 6 0 100 1 0\n"
simple_expect "=1042 4 { 0 $any_time 6 255 0 \\* 6 $any_time 00000000 1 $any_time 10 200 0 \\* 9 $any_time 10000000 2 $any_time 11 200 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 \\* 9 $any_time 10000000 }"
# (Just a quick check: does the renumber work properly even if we don't
# start from the beginning of the list?)  (Hmm.  No renumber in this
# variant.  Oh well.)
send "1043 108 6 2 100 1 0\n"
simple_expect "=1043 2 { 2 $any_time 11 200 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 \\* 9 $any_time 10000000 }"

# gazonk does query-read-texts-10 on foo and 10, 11 and 12.
send "1044 98 6 10\n"
simple_expect "=1044 1 $any_time 10 200 0 0 \\* 9 $any_time 10000000"
send "1045 98 6 11\n"
simple_expect "=1045 2 $any_time 11 200 0 0 \\* 9 $any_time 10000000"
send "1046 98 6 12\n"
simple_expect "=1046 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000"

# gazonk does query-read-texts-old on foo and 10, 11 and 12.
send "1047 9 6 10\n"
simple_expect "=1047 $any_time 10 200 0 0 \\*"
send "1048 9 6 11\n"
simple_expect "=1048 $any_time 11 200 0 0 \\*"
send "1049 9 6 12\n"
simple_expect "=1049 $any_time 12 200 0 0 \\*"

# gazonk does query-read-texts on foo and 10, 11 and 12.
send "1050 107 6 10 1 0\n"
simple_expect "=1050 1 $any_time 10 200 0 \\* 9 $any_time 10000000"
send "1051 107 6 11 1 0\n"
simple_expect "=1051 2 $any_time 11 200 0 \\* 9 $any_time 10000000"
send "1052 107 6 12 1 0\n"
simple_expect "=1052 3 $any_time 12 200 0 \\* 9 $any_time 10000000"

#
# gazonk examines bar.
#

# gazonk does get-unread-confs of bar.  Should return undefined-person.
send "1053 52 7\n"
good_bad_expect "%1053 10 7" "=0 \\*" "Bug 37"

# gazonk does get-membership-10 of bar.  Should return undefined-person.
send "1054 99 7 0 100 1\n"
simple_expect "%1054 10 7"

# gazonk does get-membership-old of bar.  Should return undefined-person.
send "1055 46 7 0 100 1\n"
simple_expect "%1055 10 7"

# gazonk does get-membership of bar.  Should return undefined-person.
send "1056 108 7 0 100 1 0\n"
simple_expect "%1056 10 7"

# gazonk does query-read-texts-10 on bar and 10, 11 and 12.  undefined-person.
# FIXME (bug 703): This should not be censored.  But see Bug 70...
setup_xfail "*-*-*" "Bug 703"
fail "query-read-texts-10 returns info about secret persons to admin of conf"
send "1057 98 7 10\n"
simple_expect "%1057 10 7"
send "1058 98 7 11\n"
simple_expect "%1058 10 7"
send "1059 98 7 12\n"
simple_expect "%1059 10 7"

# gazonk does query-read-texts-old on bar and 10, 11 and 12.  undefined-person.
# FIXME (bug 703): This should not be censored.  But see Bug 70...
setup_xfail "*-*-*" "Bug 703"
fail "query-read-texts-old returns info about secret persons to admin of conf"
send "1060 9 7 10\n"
simple_expect "%1060 10 7"
send "1061 9 7 11\n"
simple_expect "%1061 10 7"
send "1062 9 7 12\n"
simple_expect "%1062 10 7"

# gazonk does query-read-texts on bar and 10, 11 and 12.  undefined-person.
# FIXME (bug 703): This should not be censored.  But see Bug 70...
setup_xfail "*-*-*" "Bug 703"
fail "query-read-texts returns info about secret persons to admin of conf"
send "1063 107 7 10 1 0\n"
simple_expect "%1063 10 7"
send "1064 107 7 11 1 0\n"
simple_expect "%1064 10 7"
send "1065 107 7 12 1 0\n"
simple_expect "%1065 10 7"

#
# gazonk examines citrus.
#

# gazonk does get-unread-confs of citrus.  Should return the empty list.
send "1066 52 8\n"
simple_expect "=1066 0 \\*"

# gazonk does get-membership-10 of citrus.  Should return 4 { 6 10 11 12 }.
send "1067 99 8 0 100 1\n"
simple_expect "=1067 4 { 0 $any_time 8 255 0 0 \\* 8 $any_time 00000000 1 $any_time 10 200 0 0 \\* 9 $any_time 10000000 2 $any_time 11 200 0 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000 }"

# gazonk does get-membership-old of citrus.  Should return 4 { 6 10 11 12 }.
send "1068 46 8 0 100 1\n"
simple_expect "=1068 4 { $any_time 8 255 0 0 \\* $any_time 10 200 0 0 \\* $any_time 11 200 0 0 \\* $any_time 12 200 0 0 \\* }"

# gazonk does get-membership of citrus.  Should return 4 { 6 10 11 12 }.
send "1069 108 8 0 100 1 0\n"
simple_expect "=1069 4 { 0 $any_time 8 255 0 \\* 8 $any_time 00000000 1 $any_time 10 200 0 \\* 9 $any_time 10000000 2 $any_time 11 200 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 \\* 9 $any_time 10000000 }"

# gazonk does query-read-texts-10 on citrus and 10, 11 and 12.
send "1070 98 8 10\n"
simple_expect "=1070 1 $any_time 10 200 0 0 \\* 9 $any_time 10000000"
send "1071 98 8 11\n"
simple_expect "=1071 2 $any_time 11 200 0 0 \\* 9 $any_time 10000000"
send "1072 98 8 12\n"
simple_expect "=1072 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000"

# gazonk does query-read-texts-old on citrus and 10, 11 and 12.
send "1073 9 8 10\n"
simple_expect "=1073 $any_time 10 200 0 0 \\*"
send "1074 9 8 11\n"
simple_expect "=1074 $any_time 11 200 0 0 \\*"
send "1075 9 8 12\n"
simple_expect "=1075 $any_time 12 200 0 0 \\*"

# gazonk does query-read-texts on citrus and 10, 11 and 12.
send "1076 107 8 10 1 0\n"
simple_expect "=1076 1 $any_time 10 200 0 \\* 9 $any_time 10000000"
send "1077 107 8 11 1 0\n"
simple_expect "=1077 2 $any_time 11 200 0 \\* 9 $any_time 10000000"
send "1078 107 8 12 1 0\n"
simple_expect "=1078 3 $any_time 12 200 0 \\* 9 $any_time 10000000"

#
# foo writes a text with secret, rd-prot and public as recipients.
#

talk_to client 0
send "1079 86 [holl "foo"] 3 { 0 10 0 11 0 12 } 0 { }\n"
simple_expect "=1079 1"

#
# foo examines foo
#

# foo does get-unread-confs of foo.  Should return 3 { 10 11 12 }.
send "1080 52 6\n"
simple_expect "=1080 3 { 10 11 12 }"

# foo does get-membership-10 of foo.  Should return 4 { 6 10 11 12 }.
send "1081 99 6 0 100 1\n"
simple_expect "=1081 4 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 10 200 0 0 \\* 9 $any_time 10000000 2 $any_time 11 200 0 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000 }"

# foo does get-membership-old of foo.  Should return 4 { 6 10 11 12 }.
send "1082 46 6 0 100 1\n"
simple_expect "=1082 4 { $any_time 6 255 0 0 \\* $any_time 10 200 0 0 \\* $any_time 11 200 0 0 \\* $any_time 12 200 0 0 \\* }"

# foo does get-membership of foo.  Should return 4 { 6 10 11 12 }.
send "1083 108 6 0 100 1 0\n"
simple_expect "=1083 4 { 0 $any_time 6 255 0 \\* 6 $any_time 00000000 1 $any_time 10 200 0 \\* 9 $any_time 10000000 2 $any_time 11 200 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 \\* 9 $any_time 10000000 }"

# foo does query-read-texts-10 on foo and 10, 11 and 12.  Should get results.
send "1084 98 6 10\n"
simple_expect "=1084 1 $any_time 10 200 0 0 \\* 9 $any_time 10000000"
send "1085 98 6 11\n"
simple_expect "=1085 2 $any_time 11 200 0 0 \\* 9 $any_time 10000000"
send "1086 98 6 12\n"
simple_expect "=1086 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000"

# foo does query-read-texts-old on foo and 10, 11 and 12.  Should get results.
send "1087 9 6 10\n"
simple_expect "=1087 $any_time 10 200 0 0 \\*"
send "1088 9 6 11\n"
simple_expect "=1088 $any_time 11 200 0 0 \\*"
send "1089 9 6 12\n"
simple_expect "=1089 $any_time 12 200 0 0 \\*"

# foo does query-read-texts on foo and 10, 11 and 12.  Should get results.
send "1090 107 6 10 1 0\n"
simple_expect "=1090 1 $any_time 10 200 0 \\* 9 $any_time 10000000"
send "1091 107 6 11 1 0\n"
simple_expect "=1091 2 $any_time 11 200 0 \\* 9 $any_time 10000000"
send "1092 107 6 12 1 0\n"
simple_expect "=1092 3 $any_time 12 200 0 \\* 9 $any_time 10000000"

#
# gazonk examines foo.
#

talk_to client 3

# gazonk does get-unread-confs of foo.  Should return 3 { 10 11 12 }.
send "1093 52 6\n"
# "Bug 596"
good_bad_expect "=1093 3 { 10 11 12 }" "=1 { 12 }"

# gazonk does get-membership-10 of foo.  Should return 4 { 6 10 11 12 }.
send "1094 99 6 0 100 1\n"
simple_expect "=1094 4 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 10 200 0 0 \\* 9 $any_time 10000000 2 $any_time 11 200 0 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000 }"
# This is a totally pointless test of renumber--no renumbering here!
send "1095 99 6 3 100 1\n"
simple_expect "=1095 1 { 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000 }"

# gazonk does get-membership-old of foo.  Should return 4 { 6 10 11 12 }.
send "1096 46 6 0 100 1\n"
simple_expect "=1096 4 { $any_time 6 255 0 0 \\* $any_time 10 200 0 0 \\* $any_time 11 200 0 0 \\* $any_time 12 200 0 0 \\* }"

# gazonk does get-membership of foo.  Should return 4 { 6 10 11 12 }.
send "1097 108 6 0 100 1 0\n"
simple_expect "=1097 4 { 0 $any_time 6 255 0 \\* 6 $any_time 00000000 1 $any_time 10 200 0 \\* 9 $any_time 10000000 2 $any_time 11 200 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 \\* 9 $any_time 10000000 }"
# This is a totally pointless test of renumber--no renumbering here!
send "1098 108 6 3 100 1 0\n"
simple_expect "=1098 1 { 3 $any_time 12 200 0 \\* 9 $any_time 10000000 }"

# gazonk does query-read-texts-10 on foo and 10, 11 and 12.
send "1099 98 6 10\n"
simple_expect "=1099 1 $any_time 10 200 0 0 \\* 9 $any_time 10000000"
send "1100 98 6 11\n"
simple_expect "=1100 2 $any_time 11 200 0 0 \\* 9 $any_time 10000000"
send "1101 98 6 12\n"
simple_expect "=1101 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000"

# gazonk does query-read-texts-old on foo and 10, 11 and 12.
send "1102 9 6 10\n"
simple_expect "=1102 $any_time 10 200 0 0 \\*"
send "1103 9 6 11\n"
simple_expect "=1103 $any_time 11 200 0 0 \\*"
send "1104 9 6 12\n"
simple_expect "=1104 $any_time 12 200 0 0 \\*"

# gazonk does query-read-texts on foo and 10, 11 and 12.
send "1105 107 6 10 1 0\n"
simple_expect "=1105 1 $any_time 10 200 0 \\* 9 $any_time 10000000"
send "1106 107 6 11 1 0\n"
simple_expect "=1106 2 $any_time 11 200 0 \\* 9 $any_time 10000000"
send "1107 107 6 12 1 0\n"
simple_expect "=1107 3 $any_time 12 200 0 \\* 9 $any_time 10000000"



#
# gazonk examines bar
#

# gazonk does get-unread-confs of bar.  Should return undefined-person.
send "1108 52 7\n"
good_bad_expect "%1108 10 7" "=3 { 10 11 12 }" "Bug 37"

# gazonk does get-membership-10 of bar.  Should return undefined-person.
send "1109 99 7 0 100 1\n"
simple_expect "%1109 10 7"

# gazonk does get-membership-old of bar.  Should return undefined-person.
send "1110 46 7 0 100 1\n"
simple_expect "%1110 10 7"

# gazonk does get-membership of bar.  Should return undefined-person.
send "1111 108 7 0 100 1 0\n"
simple_expect "%1111 10 7"

# gazonk does query-read-texts-10 on bar and 10, 11 and 12.  undefined-person.
# FIXME (bug 703): This should not be censored.  But see Bug 70...
setup_xfail "*-*-*" "Bug 703"
fail "query-read-texts returns info about secret persons to admin of conf"
send "1112 98 7 10\n"
simple_expect "%1112 10 7"
send "1113 98 7 11\n"
simple_expect "%1113 10 7"
send "1114 98 7 12\n"
simple_expect "%1114 10 7"

# gazonk does query-read-texts-old on bar and 10, 11 and 12.  undefined-person.
# FIXME (bug 703): This should not be censored.  But see Bug 70...
setup_xfail "*-*-*" "Bug 703"
fail "query-read-texts-old returns info about secret persons to admin of conf"
send "1115 9 7 10\n"
simple_expect "%1115 10 7"
send "1116 9 7 11\n"
simple_expect "%1116 10 7"
send "1117 9 7 12\n"
simple_expect "%1117 10 7"

# gazonk does query-read-texts on bar and 10, 11 and 12.  undefined-person.
# FIXME (bug 703): This should not be censored.  But see Bug 70...
setup_xfail "*-*-*" "Bug 703"
fail "query-read-texts returns info about secret persons to admin of conf"
send "1118 107 7 10 1 0\n"
simple_expect "%1118 10 7"
send "1119 107 7 11 1 0\n"
simple_expect "%1119 10 7"
send "1120 107 7 12 1 0\n"
simple_expect "%1120 10 7"


#
# gazonk examines citrus.
#

# gazonk does get-unread-confs of citrus.  Should return the empty list.
send "1121 52 8\n"
good_bad_expect "=1121 0 \\*" "=3 { 10 11 12 }" "Bug 595"

# gazonk does get-membership-10 of citrus.  Should return 4 { 6 10 11 12 }.
send "1122 99 8 0 100 1\n"
simple_expect "=1122 4 { 0 $any_time 8 255 0 0 \\* 8 $any_time 00000000 1 $any_time 10 200 0 0 \\* 9 $any_time 10000000 2 $any_time 11 200 0 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000 }"

# gazonk does get-membership-old of citrus.  Should return 4 { 6 10 11 12 }.
send "1123 46 8 0 100 1\n"
simple_expect "=1123 4 { $any_time 8 255 0 0 \\* $any_time 10 200 0 0 \\* $any_time 11 200 0 0 \\* $any_time 12 200 0 0 \\* }"

# gazonk does get-membership of citrus.  Should return 4 { 6 10 11 12 }.
send "1124 108 8 0 100 1 0\n"
simple_expect "=1124 4 { 0 $any_time 8 255 0 \\* 8 $any_time 00000000 1 $any_time 10 200 0 \\* 9 $any_time 10000000 2 $any_time 11 200 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 \\* 9 $any_time 10000000 }"

# gazonk does query-read-texts-10 on citrus and 10, 11 and 12.
send "1125 98 8 10\n"
simple_expect "=1125 1 $any_time 10 200 0 0 \\* 9 $any_time 10000000"
send "1126 98 8 11\n"
simple_expect "=1126 2 $any_time 11 200 0 0 \\* 9 $any_time 10000000"
send "1127 98 8 12\n"
simple_expect "=1127 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000"

# gazonk does query-read-texts-old on citrus and 10, 11 and 12.
send "1128 9 8 10\n"
simple_expect "=1128 $any_time 10 200 0 0 \\*"
send "1129 9 8 11\n"
simple_expect "=1129 $any_time 11 200 0 0 \\*"
send "1130 9 8 12\n"
simple_expect "=1130 $any_time 12 200 0 0 \\*"

# gazonk does query-read-texts on citrus and 10, 11 and 12.
send "1131 107 8 10 1 0\n"
simple_expect "=1131 1 $any_time 10 200 0 \\* 9 $any_time 10000000"
send "1132 107 8 11 1 0\n"
simple_expect "=1132 2 $any_time 11 200 0 \\* 9 $any_time 10000000"
send "1133 107 8 12 1 0\n"
simple_expect "=1133 3 $any_time 12 200 0 \\* 9 $any_time 10000000"


#
# mark as read
#

# foo marks the text as read
talk_to client 0
send "1134 27 10 1 { 1 }\n"
simple_expect "=1134"
send "1135 27 11 1 { 1 }\n"
simple_expect "=1135"
send "1136 27 12 1 { 1 }\n"
simple_expect "=1136"

# bar marks the text as read
talk_to client 1
send "1137 27 10 1 { 1 }\n"
simple_expect "=1137"
send "1138 27 11 1 { 1 }\n"
simple_expect "=1138"
send "1139 27 12 1 { 1 }\n"
simple_expect "=1139"

# citrus marks the text as read
talk_to client 2
send "1140 27 10 1 { 1 }\n"
simple_expect "=1140"
send "1141 27 11 1 { 1 }\n"
simple_expect "=1141"
send "1142 27 12 1 { 1 }\n"
simple_expect "=1142"

#
# foo examines foo.
#

talk_to client 0

# foo does get-unread-confs of foo.  Should return the empty list.
send "1143 52 6\n"
simple_expect "=1143 0 \\*"

# foo does get-membership-10 of foo.  Should return 4 { 6 10 11 12 }.
send "1144 99 6 0 100 1\n"
simple_expect "=1144 4 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 10 200 1 0 \\* 9 $any_time 10000000 2 $any_time 11 200 1 0 \\* 9 $any_time 10000000 3 $any_time 12 200 1 0 \\* 9 $any_time 10000000 }"

# foo does get-membership-old of foo.  Should return 4 { 6 10 11 12 }.
send "1145 46 6 0 100 1\n"
simple_expect "=1145 4 { $any_time 6 255 0 0 \\* $any_time 10 200 1 0 \\* $any_time 11 200 1 0 \\* $any_time 12 200 1 0 \\* }"

# foo does get-membership of foo.  Should return 4 { 6 10 11 12 }.
send "1146 108 6 0 100 1 0\n"
simple_expect "=1146 4 { 0 $any_time 6 255 0 \\* 6 $any_time 00000000 1 $any_time 10 200 1 { 1 1 } 9 $any_time 10000000 2 $any_time 11 200 1 { 1 1 } 9 $any_time 10000000 3 $any_time 12 200 1 { 1 1 } 9 $any_time 10000000 }"

# foo does query-read-texts-10 on foo and 10, 11 and 12.  Should get results.
send "1147 98 6 10\n"
simple_expect "=1147 1 $any_time 10 200 1 0 \\* 9 $any_time 10000000"
send "1148 98 6 11\n"
simple_expect "=1148 2 $any_time 11 200 1 0 \\* 9 $any_time 10000000"
send "1149 98 6 12\n"
simple_expect "=1149 3 $any_time 12 200 1 0 \\* 9 $any_time 10000000"

# foo does query-read-texts-old on foo and 10, 11 and 12.  Should get results.
send "1150 9 6 10\n"
simple_expect "=1150 $any_time 10 200 1 0 \\*"
send "1151 9 6 11\n"
simple_expect "=1151 $any_time 11 200 1 0 \\*"
send "1152 9 6 12\n"
simple_expect "=1152 $any_time 12 200 1 0 \\*"

# foo does query-read-texts on foo and 10, 11 and 12.  Should get results.
send "1153 107 6 10 1 0\n"
simple_expect "=1153 1 $any_time 10 200 1 { 1 1 } 9 $any_time 10000000"
send "1154 107 6 11 1 0\n"
simple_expect "=1154 2 $any_time 11 200 1 { 1 1 } 9 $any_time 10000000"
send "1155 107 6 12 1 0\n"
simple_expect "=1155 3 $any_time 12 200 1 { 1 1 } 9 $any_time 10000000"

#
# gazonk examines foo.
#

talk_to client 3

# gazonk does get-unread-confs of foo.  Should return the empty list.
send "1156 52 6\n"
simple_expect "=1156 0 \\*"

# gazonk does get-membership-10 of foo.  Should return 4 { 6 10 11 12 }.
send "1157 99 6 0 100 1\n"
simple_expect "=1157 4 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 10 200 1 0 \\* 9 $any_time 10000000 2 $any_time 11 200 1 0 \\* 9 $any_time 10000000 3 $any_time 12 200 1 0 \\* 9 $any_time 10000000 }"

# gazonk does get-membership-old of foo.  Should return 4 { 6 10 11 12 }.
send "1158 46 6 0 100 1\n"
simple_expect "=1158 4 { $any_time 6 255 0 0 \\* $any_time 10 200 1 0 \\* $any_time 11 200 1 0 \\* $any_time 12 200 1 0 \\* }"

# gazonk does get-membership of foo.  Should return 4 { 6 10 11 12 }.
send "1159 108 6 0 100 1 0\n"
simple_expect "=1159 4 { 0 $any_time 6 255 0 \\* 6 $any_time 00000000 1 $any_time 10 200 1 { 1 1 } 9 $any_time 10000000 2 $any_time 11 200 1 { 1 1 } 9 $any_time 10000000 3 $any_time 12 200 1 { 1 1 } 9 $any_time 10000000 }"

# gazonk does query-read-texts-10 on foo and 10, 11 and 12.
send "1160 98 6 10\n"
simple_expect "=1160 1 $any_time 10 200 1 0 \\* 9 $any_time 10000000"
send "1161 98 6 11\n"
simple_expect "=1161 2 $any_time 11 200 1 0 \\* 9 $any_time 10000000"
send "1162 98 6 12\n"
simple_expect "=1162 3 $any_time 12 200 1 0 \\* 9 $any_time 10000000"

# gazonk does query-read-texts-old on foo and 10, 11 and 12.
send "1163 9 6 10\n"
simple_expect "=1163 $any_time 10 200 1 0 \\*"
send "1164 9 6 11\n"
simple_expect "=1164 $any_time 11 200 1 0 \\*"
send "1165 9 6 12\n"
simple_expect "=1165 $any_time 12 200 1 0 \\*"

# gazonk does query-read-texts on foo and 10, 11 and 12.
send "1166 107 6 10 1 0\n"
simple_expect "=1166 1 $any_time 10 200 1 { 1 1 } 9 $any_time 10000000"
send "1167 107 6 11 1 0\n"
simple_expect "=1167 2 $any_time 11 200 1 { 1 1 } 9 $any_time 10000000"
send "1168 107 6 12 1 0\n"
simple_expect "=1168 3 $any_time 12 200 1 { 1 1 } 9 $any_time 10000000"

# gazonk does <FIXME (bug 590)> on foo and 10, 11 and 12.  Should work.

#
# gazonk examines bar.
#

# gazonk does get-unread-confs of bar.  Should return undefined-person.
send "1169 52 7\n"
good_bad_expect "%1169 10 7" "=0 \\*" "Bug 37"

# gazonk does get-membership-10 of bar.  Should return undefined-person.
send "1170 99 7 0 100 1\n"
simple_expect "%1170 10 7"

# gazonk does get-membership-old of bar.  Should return undefined-person.
send "1171 46 7 0 100 1\n"
simple_expect "%1171 10 7"

# gazonk does get-membership of bar.  Should return undefined-person.
send "1172 108 7 0 100 1 0\n"
simple_expect "%1172 10 7"

# gazonk does query-read-texts-10 on bar and 10, 11 and 12.  undefined-person.
# FIXME (bug 703): This should not be censored.  But see Bug 70...
setup_xfail "*-*-*" "Bug 703"
fail "query-read-texts-10 returns info about secret persons to admin of conf"
send "1173 98 7 10\n"
simple_expect "%1173 10 7"
send "1174 98 7 11\n"
simple_expect "%1174 10 7"
send "1175 98 7 12\n"
simple_expect "%1175 10 7"

# gazonk does query-read-texts-old on bar and 10, 11 and 12.  undefined-person.
# FIXME (bug 703): This should not be censored.  But see Bug 70...
setup_xfail "*-*-*" "Bug 703"
fail "query-read-texts-old returns info about secret persons to admin of conf"
send "1176 9 7 10\n"
simple_expect "%1176 10 7"
send "1177 9 7 11\n"
simple_expect "%1177 10 7"
send "1178 9 7 12\n"
simple_expect "%1178 10 7"

# gazonk does query-read-texts on bar and 10, 11 and 12.  undefined-person.
# FIXME (bug 703): This should not be censored.  But see Bug 70...
setup_xfail "*-*-*" "Bug 703"
fail "query-read-texts returns info about secret persons to admin of conf"
send "1179 107 7 10 1 0\n"
simple_expect "%1179 10 7"
send "1180 107 7 11 1 0\n"
simple_expect "%1180 10 7"
send "1181 107 7 12 1 0\n"
simple_expect "%1181 10 7"

# FIXME (bug 704): Now, set unread-is-secret for bar, and let gazonk
# redo the query-read-texts-10, query-read-texts-old, and
# query-read-texts requests.  Check that unread-is-secret is
# respected.  Then change unread-is-secret back.


#
# gazonk examines citrus.
#

# gazonk does get-unread-confs of citrus.  Should return the empty list.
send "1182 52 8\n"
simple_expect "=1182 0 \\*"

# gazonk does get-membership-10 of citrus.  Should return 4 { 6 10 11 12 }.
send "1183 99 8 0 100 1\n"
simple_expect "=1183 4 { 0 $any_time 8 255 0 0 \\* 8 $any_time 00000000 1 $any_time 10 200 0 0 \\* 9 $any_time 10000000 2 $any_time 11 200 0 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000 }"

# gazonk does get-membership-old of citrus.  Should return 4 { 6 10 11 12 }.
send "1184 46 8 0 100 1\n"
simple_expect "=1184 4 { $any_time 8 255 0 0 \\* $any_time 10 200 0 0 \\* $any_time 11 200 0 0 \\* $any_time 12 200 0 0 \\* }"

# gazonk does get-membership of citrus.  Should return 4 { 6 10 11 12 }.
send "1185 108 8 0 100 1 0\n"
simple_expect "=1185 4 { 0 $any_time 8 255 0 \\* 8 $any_time 00000000 1 $any_time 10 200 0 \\* 9 $any_time 10000000 2 $any_time 11 200 0 \\* 9 $any_time 10000000 3 $any_time 12 200 0 \\* 9 $any_time 10000000 }"

# gazonk does query-read-texts-10 on citrus and 10, 11 and 12.
send "1186 98 8 10\n"
simple_expect "=1186 1 $any_time 10 200 0 0 \\* 9 $any_time 10000000"
send "1187 98 8 11\n"
simple_expect "=1187 2 $any_time 11 200 0 0 \\* 9 $any_time 10000000"
send "1188 98 8 12\n"
simple_expect "=1188 3 $any_time 12 200 0 0 \\* 9 $any_time 10000000"

# gazonk does query-read-texts-old on citrus and 10, 11 and 12.
send "1189 9 8 10\n"
simple_expect "=1189 $any_time 10 200 0 0 \\*"
send "1190 9 8 11\n"
simple_expect "=1190 $any_time 11 200 0 0 \\*"
send "1191 9 8 12\n"
simple_expect "=1191 $any_time 12 200 0 0 \\*"

# gazonk does query-read-texts on citrus and 10, 11 and 12.
send "1192 107 8 10 1 0\n"
simple_expect "=1192 1 $any_time 10 200 0 \\* 9 $any_time 10000000"
send "1193 107 8 11 1 0\n"
simple_expect "=1193 2 $any_time 11 200 0 \\* 9 $any_time 10000000"
send "1194 107 8 12 1 0\n"
simple_expect "=1194 3 $any_time 12 200 0 \\* 9 $any_time 10000000"

#
# gazonk examines the conferences
#

# Test get-members-old
send "1195 48 10 0 100\n"
simple_expect "=1195 3 { 7 6 8 }"
send "1196 48 11 0 100\n"
simple_expect "=1196 3 { 7 6 8 }"
send "1197 48 12 0 100\n"
simple_expect "=1197 3 { 7 6 8 }"

# Test get-members.
send "1198 101 10 0 100\n"
simple_expect "=1198 3 { 7 9 $any_time 10000000 6 9 $any_time 10000000 8 9 $any_time 10000000 }"
send "1199 101 11 0 100\n"
simple_expect "=1199 3 { 7 9 $any_time 10000000 6 9 $any_time 10000000 8 9 $any_time 10000000 }"
send "1200 101 12 0 100\n"
simple_expect "=1200 3 { 7 9 $any_time 10000000 6 9 $any_time 10000000 8 9 $any_time 10000000 }"


# Shut down.
talk_to client 3
send "1201 0 5 [holl "gazonk"]\n"
simple_expect "=1201"
send "1202 42 255\n"
simple_expect "=1202"
send "1203 44 0\n"
simple_expect "=1203"
client_death 3
client_death 2
client_death 1
client_death 0
lyskomd_death
