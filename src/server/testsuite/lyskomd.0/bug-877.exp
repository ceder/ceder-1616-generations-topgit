# Test suite for lyskomd.
# Copyright (C) 1999, 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test for bug 877: �The mark_as_read() function calls
# mark_conference_as_changed, as if it thinks that the Membership is
# stored in the Conference structure.  It is stored in the Person
# structure.  This may lead to the text not being properly marked as
# read. One factor that reduces the seriousness is that when marking a
# text in the current conference as read, a call to
# mark_person_as_changed will also be made, so as long as the client
# does a change_conference() to one of the recipients before marking
# the text as read, it will sooner or later be marked as read for
# real.�

lyskomd_start

client_start 0

talk_to client 0
send "A3Hfoo\n"
simple_expect "LysKOM" "connected"

# Log in as administrator.
send "1000 62 5 [holl "gazonk"] 1\n"
simple_expect "=1000"

# Create a text.
send "1001 86 [holl "Text 1"] 1 { 0 1 } 0 { }\n"
simple_expect "=1001 1"

# Join conference 1.
send "1002 100 1 5 200 1 00000000\n"
simple_expect "=1002"

# Check what is read.
send "1003 107 5 1 1 0\n"
simple_expect "=1003 1 $any_time 1 200 0 \\* 5 $any_time 00000000"

# Save the database.  We want the person to be clean.
send "1004 42 255\n"
simple_expect "=1004"

send "1005 43\n"
simple_expect ":0 7"
simple_expect ":0 7"
simple_expect "=1005"

if {$debug_calls} {

    # Start saving the database.  A dirty copy of the person now exists,
    # so all future references to the person requires that we get a copy
    # of him from the cache.
    send "1003 1004\n"
    simple_expect ":0 7"
    simple_expect "=1003"
} else {
    unsupported "testing for bug 877 requires --with-debug-calls"
}
    
# Mark the text as read.  This modifies the person, and should
# mark the person as dirty.
send "1006 27 1 1 { 1 }\n"
simple_expect "=1006"

if {$debug_calls} {

    # Actually save the previous snapshot of the database.
    # However, this will also save the supposedly clean but modified
    # person!
    send "1007 1005\n"
    simple_expect ":0 7"
    simple_expect "=1007"

}

# Check what is read, to ensure we have the expected proper stuff in core.
send "1007 107 5 1 1 0\n"
simple_expect "=1007 1 $any_time 1 200 1 { 1 1 } 5 $any_time 00000000"

# Crash lyskomd.
kill_lyskomd
client_death 0

dbck_run

# Start lyskomd, from the previously saved database.
lyskomd_start "" "" "" "" "" "" 0 1 6 2

client_start 0

talk_to client 0
send "A3Hfoo\n"
simple_expect "LysKOM" "connected"

# Log in as administrator.
send "1008 62 5 [holl "gazonk"] 1\n"
simple_expect "=1008"

# Check what is read.  The mark-as-read should be lost -- we are
# reading from an older snapshot.
send "1009 107 5 1 1 0\n"
simple_expect "=1009 1 $any_time 1 200 0 \\* 5 $any_time 00000000"

# Shut down.
send "1010 42 255\n"
simple_expect "=1010"

send "1011 44 0\n"
simple_expect "=1011"

client_death 0

lyskomd_death
