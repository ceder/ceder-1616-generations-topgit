# Test suite for lyskomd.
# Copyright (C) 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test sending of async-deleted-text, async-new-text,
# async-new-text-old, async-add-recipient and async-sub-recipient to
# recipients of texts that are linked to the text.

# We want to be able to set broken aux-items, so we start by
# creating our own aux-item.conf which lack all validate lines.

set auxfile "lyskomd.0/aux-items-35.conf"
system "grep -v validate $aux_item_default_conf_file > $auxfile"

lyskomd_start "$auxfile"

client_start 0
talk_to client 0
send "A0H\n"
simple_expect "LysKOM"
send "1000 62 5 6Hgazonk 1\n"
simple_expect "=1000"

# Create the user "Listener".  Log in as it.
client_start 1
talk_to client 1
send "A0H\n"
simple_expect "LysKOM"
send "1001 89 [holl "Listener"] [holl "none"] 00000000 0 { }\n"
simple_expect "=1001 6"
send "1002 62 6 [holl "none"] 1\n"
simple_expect "=1002"
send "1003 80 5 { 14 15 16 17 0 }\n"
simple_expect "=1003"

# Join conferences:
#
# Conf_no   passive?
#    1         0 (no)
#    2         1 (yes)

send "1004 100 1 6 200 10 00000000\n"
simple_expect "=1004"
send "1005 100 2 6 200 10 01000000\n"
simple_expect "=1005"

# Create base texts:
#
# Text_no   Recipient  passive?
#   1           1        0 (no)
#   2           2        1 (yes)
talk_to client 0
send "1006 86 [holl "text 1"] 1 { 0 1 } 0 { }\n"
simple_expect "=1006 1"
talk_to client 1
simple_expect ":16 0 1 $any_time 5 0 6 0 2 { 0 1 6 1 }"
simple_expect ":18 15 1 $any_time 5 0 6 0 2 { 0 1 6 1 } 0 \\*"

talk_to client 0
send "1007 86 [holl "text 2"] 1 { 0 2 } 0 { }\n"
simple_expect "=1007 2"

# Text 3: not linked at all.
send "1008 86 [holl "text 3"] 1 { 0 3 } 0 { }\n"
simple_expect "=1008 3"

# Text 4: linked via a comm-to.
send "1009 86 [holl "text 4"] 2 { 0 3 2 1 } 0 { }\n"
simple_expect "=1009 4"
talk_to client 1
simple_expect ":16 0 4 $any_time 5 0 6 0 3 { 0 3 6 2 2 1 }"
simple_expect ":18 15 4 $any_time 5 0 6 0 3 { 0 3 6 2 2 1 } 0 \\*"
# Add conference 4 as recipient of text 4.
talk_to client 0
send "1010 30 4 4 0\n"
simple_expect "=1010"
talk_to client 1
simple_expect ":3 16 4 4 0"
# Remove conference 4 as recipient of text 4.
talk_to client 0
send "1011 31 4 4\n"
simple_expect "=1011"
talk_to client 1
simple_expect ":3 17 4 4 0"
# Delete text 4.
talk_to client 0
send "1012 29 4\n"
simple_expect "=1012"
talk_to client 1
simple_expect ":18 14 4 $any_time 5 0 6 0 3 { 0 3 6 2 2 1 } 0 \\*"

# Text 5: linked via a comm-to, to passive recipient.
# This produces no async messages.
talk_to client 0
send "1013 86 [holl "text 5"] 2 { 0 3 2 2 } 0 { }\n"
simple_expect "=1013 5"
# Add conference 4 as recipient of text 5.
send "1014 30 5 4 0\n"
simple_expect "=1014"
# Remove conference 4 as recipient of text 5.
send "1015 31 5 4\n"
simple_expect "=1015"
# Delete text 5.
send "1016 29 5\n"
simple_expect "=1016"

# Text 6: linked via a footn-to.
send "1017 86 [holl "text 6"] 2 { 0 3 4 1 } 0 { }\n"
simple_expect "=1017 6"
talk_to client 1
simple_expect ":16 0 6 $any_time 5 0 6 0 3 { 0 3 6 4 4 1 }"
simple_expect ":18 15 6 $any_time 5 0 6 0 3 { 0 3 6 4 4 1 } 0 \\*"
# Add conference 4 as recipient of text 6.
talk_to client 0
send "1018 30 6 4 0\n"
simple_expect "=1018"
talk_to client 1
simple_expect ":3 16 6 4 0"
# Remove conference 4 as recipient of text 6.
talk_to client 0
send "1019 31 6 4\n"
simple_expect "=1019"
talk_to client 1
simple_expect ":3 17 6 4 0"
# Delete text 6.
talk_to client 0
send "1020 29 6\n"
simple_expect "=1020"
talk_to client 1
simple_expect ":18 14 6 $any_time 5 0 6 0 3 { 0 3 6 4 4 1 } 0 \\*"

# Text 7: linked via footn-in.  For obvious reasons, this cannot
# produce asycn-new-text or async-new-text-old.
talk_to client 0
send "1021 86 [holl "text 7"] 1 { 0 3 } 0 { }\n"
simple_expect "=1021 7"
send "1022 37 1 7\n"
simple_expect "=1022"
talk_to client 1
# FIXME (bug 971): this should produce an async-new-footnote message,
# but no such message exists.
# Add conference 4 as recipient of text 7.
talk_to client 0
send "1023 30 7 4 1\n"
simple_expect "=1023"
talk_to client 1
simple_expect ":3 16 7 4 1"
# Remove conference 4 as recipient of text 7.
talk_to client 0
send "1024 31 7 4\n"
simple_expect "=1024"
talk_to client 1
simple_expect ":3 17 7 4 1"
# Delete text 7.
talk_to client 0
send "1025 29 7\n"
simple_expect "=1025"
talk_to client 1
simple_expect ":18 14 7 $any_time 5 0 6 0 3 { 0 3 6 5 5 1 } 0 \\*"

# Text 8: linked via comm-in.  For obvious reasons, this cannot
# produce asycn-new-text or async-new-text-old.
talk_to client 0
send "1026 86 [holl "text 8"] 1 { 0 3 } 0 { }\n"
simple_expect "=1026 8"
send "1027 32 1 8\n"
simple_expect "=1027"
talk_to client 1
# FIXME (bug 971): this should produce an async-new-comment message,
# but no such message exists.
# Add conference 4 as recipient of text 8.
talk_to client 0
send "1028 30 8 4 1\n"
simple_expect "=1028"
talk_to client 1
simple_expect ":3 16 8 4 1"
# Remove conference 4 as recipient of text 8.
talk_to client 0
send "1029 31 8 4\n"
simple_expect "=1029"
talk_to client 1
simple_expect ":3 17 8 4 1"
# Delete text 8.
talk_to client 0
send "1030 29 8\n"
simple_expect "=1030"
talk_to client 1
simple_expect ":18 14 8 $any_time 5 0 6 0 3 { 0 3 6 6 3 1 } 0 \\*"

if {0} {
    set cross_mirror 1
} else {
    set cross_mirror 0
    setup_xfail "*-*-*" "Bug 23"
    fail "There is no mirroring aux-item for cross-references"
}

# Text 9: linked via a cross-reference.
talk_to client 0
send "1031 86 [holl "text 9"] 1 { 0 3 } 1 { 3 00000000 1 [holl "T1"] }\n"
simple_expect "=1031 9"
talk_to client 1
if {$cross_mirror} {
    simple_expect ":16 0 9 $any_time 5 0 6 0 2 { 0 3 6 7 }"
    simple_expect ":18 15 9 $any_time 5 0 6 0 2 { 0 3 6 7 } 1 { 1 3 5 $any_time 00000000 1 [holl "T1"] }"
}
# Add conference 4 as recipient of text 9.
talk_to client 0
send "1032 30 9 4 0\n"
simple_expect "=1032"
talk_to client 1
if {$cross_mirror} {
    simple_expect ":3 16 9 4 0"
}
# Remove conference 4 as recipient of text 9.
talk_to client 0
send "1033 31 9 4\n"
simple_expect "=1033"
talk_to client 1
if {$cross_mirror} {
    simple_expect ":3 17 9 4 0"
}
# Delete text 9.
talk_to client 0
send "1034 29 9\n"
simple_expect "=1034"
talk_to client 1
if {$cross_mirror} {
    simple_expect ":18 14 9 $any_time 5 0 6 0 2 { 0 3 6 7 } 1 { 1 3 5 $any_time 00000000 1 [holl "T1"] }"
}

# Text 10: linked via a cross-reference with trailing info.
talk_to client 0
send "1035 86 [holl "text 10"] 1 { 0 3 } 1 { 3 00000000 1 [holl "T1 ho ho"] }\n"
simple_expect "=1035 10"
talk_to client 1
if {$cross_mirror} {
    simple_expect ":16 0 10 $any_time 5 0 7 0 2 { 0 3 6 8 }"
    simple_expect ":18 15 10 $any_time 5 0 7 0 2 { 0 3 6 8 } 1 { 1 3 5 $any_time 00000000 1 [holl "T1 ho ho"] }"
}
# Add conference 4 as recipient of text 10.
talk_to client 0
send "1036 30 10 4 0\n"
simple_expect "=1036"
talk_to client 1
if {$cross_mirror} {
    simple_expect ":3 16 10 4 0"
}
# Remove conference 4 as recipient of text 10.
talk_to client 0
send "1037 31 10 4\n"
simple_expect "=1037"
talk_to client 1
if {$cross_mirror} {
    simple_expect ":3 17 10 4 0"
}
# Delete text 10.
talk_to client 0
send "1038 29 10\n"
simple_expect "=1038"
talk_to client 1
if {$cross_mirror} {
    simple_expect ":18 14 10 $any_time 5 0 7 0 2 { 0 3 6 8 } 1 { 1 3 5 $any_time 00000000 1 [holl "T1 ho ho"] }"
}

# Text 11: linked via a cross-reference to a conf (no async).
# FIXME (bug 23): Once a mirroring aux-item for cross-references
# exists, we probably should expect async messages here.
talk_to client 0
send "1039 86 [holl "text 11"] 1 { 0 3 } 1 { 3 00000000 1 [holl "C1 ho ho"] }\n"
simple_expect "=1039 11"
# Add conference 4 as recipient of text 11.
talk_to client 0
send "1040 30 11 4 0\n"
simple_expect "=1040"
# Remove conference 4 as recipient of text 11.
send "1041 31 11 4\n"
simple_expect "=1041"
# Delete text 11.
send "1042 29 11\n"
simple_expect "=1042"

# Text 12: with a broken cross-reference.
talk_to client 0
send "1043 86 [holl "text 12"] 1 { 0 3 } 1 { 3 00000000 1 [holl "C99"] }\n"
simple_expect "=1043 12"
# Add conference 4 as recipient of text 12.
talk_to client 0
send "1044 30 12 4 0\n"
simple_expect "=1044"
# Remove conference 4 as recipient of text 12.
send "1045 31 12 4\n"
simple_expect "=1045"
# Delete text 12.
send "1046 29 12\n"
simple_expect "=1046"

# Text 13: with a broken cross-reference.
talk_to client 0
send "1047 86 [holl "text 13"] 1 { 0 3 } 1 { 3 00000000 1 [holl "C"] }\n"
simple_expect "=1047 13"
# Add conference 4 as recipient of text 13.
talk_to client 0
send "1048 30 13 4 0\n"
simple_expect "=1048"
# Remove conference 4 as recipient of text 13.
send "1049 31 13 4\n"
simple_expect "=1049"
# Delete text 13.
send "1050 29 13\n"
simple_expect "=1050"

# Text 14: linked via a mx-mime-belongs-to.
talk_to client 0
send "1051 86 [holl "text 14"] 1 { 0 3 } 1 { 10100 00000000 1 [holl "1"] }\n"
simple_expect "=1051 14"
talk_to client 1
simple_expect ":16 0 14 $any_time 5 0 7 0 2 { 0 3 6 12 }"
simple_expect ":18 15 14 $any_time 5 0 7 0 2 { 0 3 6 12 } 1 { 1 10100 5 $any_time 00000000 1 [holl "1"] }"
# Add conference 4 as recipient of text 14.
talk_to client 0
send "1052 30 14 4 0\n"
simple_expect "=1052"
talk_to client 1
simple_expect ":3 16 14 4 0"
# Remove conference 4 as recipient of text 14.
talk_to client 0
send "1053 31 14 4\n"
simple_expect "=1053"
talk_to client 1
simple_expect ":3 17 14 4 0"
# Delete text 14.
talk_to client 0
send "1054 29 14\n"
simple_expect "=1054"
talk_to client 1
simple_expect ":18 14 14 $any_time 5 0 7 0 2 { 0 3 6 12 } 1 { 1 10100 5 $any_time 00000000 1 [holl "1"] }"

# Text 15: linked via a mx-mime-part-in.
talk_to client 0
send "1055 86 [holl "text 15"] 1 { 0 3 } 1 { 10101 00000000 1 [holl "1"] }\n"
simple_expect "=1055 15"
talk_to client 1
simple_expect ":16 0 15 $any_time 5 0 7 0 2 { 0 3 6 13 }"
simple_expect ":18 15 15 $any_time 5 0 7 0 2 { 0 3 6 13 } 1 { 1 10101 5 $any_time 00000000 1 [holl "1"] }"
# Add conference 4 as recipient of text 15.
talk_to client 0
send "1056 30 15 4 0\n"
simple_expect "=1056"
talk_to client 1
simple_expect ":3 16 15 4 0"
# Remove conference 4 as recipient of text 15.
talk_to client 0
send "1057 31 15 4\n"
simple_expect "=1057"
talk_to client 1
simple_expect ":3 17 15 4 0"
# Delete text 15.
talk_to client 0
send "1058 29 15\n"
simple_expect "=1058"
talk_to client 1
simple_expect ":18 14 15 $any_time 5 0 7 0 2 { 0 3 6 13 } 1 { 1 10101 5 $any_time 00000000 1 [holl "1"] }"

# Text 16: linked via a mx-mime-belongs-to with empty data.
talk_to client 0
send "1059 86 [holl "text 16"] 1 { 0 3 } 1 { 10100 00000000 1 [holl ""] }\n"
lyskomd_expect "Bad aux-item 10100 found in text 16: \"\": bad number\\."
simple_expect "=1059 16"
# Add conference 4 as recipient of text 16.
send "1060 30 16 4 0\n"
simple_expect "=1060"
# Remove conference 4 as recipient of text 16.
send "1061 31 16 4\n"
simple_expect "=1061"
# Delete text 16.
send "1062 29 16\n"
simple_expect "=1062"

# Text 17: linked via a mx-mime-part-in with empty data.
talk_to client 0
send "1063 86 [holl "text 17"] 1 { 0 3 } 1 { 10101 00000000 1 [holl ""] }\n"
lyskomd_expect "Bad aux-item 10101 found in text 17: \"\": bad number\\."
simple_expect "=1063 17"
# Add conference 4 as recipient of text 17.
send "1064 30 17 4 0\n"
simple_expect "=1064"
# Remove conference 4 as recipient of text 17.
send "1065 31 17 4\n"
simple_expect "=1065"
# Delete text 17.
send "1066 29 17\n"
simple_expect "=1066"

# Text 18: linked via a mx-mime-belongs-to with extra data.
set tno 18
talk_to client 0
send "1067 86 [holl "text $tno"] 1 { 0 3 } 1 { 10100 00000000 1 [holl "1x"] }\n"
lyskomd_expect "Bad aux-item 10100 found in text $tno: \"1x\": trailing garbage\\."
simple_expect "=1067 $tno"
# Add conference 4 as recipient of text $tno.
send "1068 30 $tno 4 0\n"
simple_expect "=1068"
# Remove conference 4 as recipient of text $tno.
send "1069 31 $tno 4\n"
simple_expect "=1069"
# Delete text $tno.
send "1070 29 $tno\n"
simple_expect "=1070"

# Text 19: linked via a mx-mime-part-in with extra data.
set tno 19
talk_to client 0
send "1071 86 [holl "text $tno"] 1 { 0 3 } 1 { 10101 00000000 1 [holl "1 x"] }\n"
lyskomd_expect "Bad aux-item 10101 found in text $tno: \"1 x\": trailing garbage\\."
simple_expect "=1071 $tno"
# Add conference 4 as recipient of text $tno.
send "1072 30 $tno 4 0\n"
simple_expect "=1072"
# Remove conference 4 as recipient of text $tno.
send "1073 31 $tno 4\n"
simple_expect "=1073"
# Delete text $tno.
send "1074 29 $tno\n"
simple_expect "=1074"

# Text 20: linked via a mx-mime-belongs-to with broken data.
set tno 20
talk_to client 0
send "1075 86 [holl "text $tno"] 1 { 0 3 } 1 { 10100 00000000 1 [holl "x1"] }\n"
lyskomd_expect "Bad aux-item 10100 found in text $tno: \"x1\": trailing garbage\\."
simple_expect "=1075 $tno"
# Add conference 4 as recipient of text $tno.
send "1076 30 $tno 4 0\n"
simple_expect "=1076"
# Remove conference 4 as recipient of text $tno.
send "1077 31 $tno 4\n"
simple_expect "=1077"
# Delete text $tno.
send "1078 29 $tno\n"
simple_expect "=1078"

# Text 21: linked via a mx-mime-part-in with broken data.
set tno 21
talk_to client 0
send "1079 86 [holl "text $tno"] 1 { 0 3 } 1 { 10101 00000000 1 [holl "99999999999999999999999999999999999999999999999999999999999999999999999999999999"] }\n"
simple_expect "=1079 $tno"
# Add conference 4 as recipient of text $tno.
send "1080 30 $tno 4 0\n"
simple_expect "=1080"
# Remove conference 4 as recipient of text $tno.
send "1081 31 $tno 4\n"
simple_expect "=1081"
# Delete text $tno.
send "1082 29 $tno\n"
simple_expect "=1082"

# Test for async-sub-recipient when we are not allowed to see the recipient.
talk_to client 0
# Create conference 7, a secret conference.
send "1083 88 [holl "secret conf"] 10100000 0 { }\n"
simple_expect "=1083 7"
# Text 22: linked via a comm-to.
set tno 22
send "1084 86 [holl "text $tno"] 3 { 0 3 2 1 0 7 } 0 { }\n"
simple_expect "=1084 $tno"
talk_to client 1
simple_expect ":16 0 $tno $any_time 5 0 7 0 3 { 0 3 6 20 2 1 }"
simple_expect ":18 15 $tno $any_time 5 0 7 0 3 { 0 3 6 20 2 1 } 0 \\*"
# Add conference 4 as recipient of text $tno.
talk_to client 0
send "1085 30 $tno 4 0\n"
simple_expect "=1085"
talk_to client 1
simple_expect ":3 16 $tno 4 0"
# Remove conference 4 as recipient of text $tno.
talk_to client 0
send "1086 31 $tno 4\n"
simple_expect "=1086"
talk_to client 1
simple_expect ":3 17 $tno 4 0"
# Remove conference 7 as recipient of text $tno.
talk_to client 0
send "1087 31 $tno 7\n"
simple_expect "=1087"
# Add conference 7 as recipient of text $tno.
send "1088 30 $tno 7 0\n"
simple_expect "=1088"
# Delete text $tno.
send "1089 29 $tno\n"
simple_expect "=1089"
talk_to client 1
simple_expect ":18 14 $tno $any_time 5 0 7 0 3 { 0 3 6 20 2 1 } 0 \\*"


# Test for async-sub-recipient when we subtract the reason we want to
# see it.

# Text 23: linked via a comm-to, sent to a conference we are a member of.
set tno 23
talk_to client 0
send "1090 86 [holl "text $tno"] 2 { 0 1 2 1 } 0 { }\n"
simple_expect "=1090 $tno"
talk_to client 1
simple_expect ":16 0 $tno $any_time 5 0 7 0 3 { 0 1 6 2 2 1 }"
simple_expect ":18 15 $tno $any_time 5 0 7 0 3 { 0 1 6 2 2 1 } 0 \\*"
# Remove conference 1 as recipient of text $tno.
talk_to client 0
send "1091 31 $tno 1\n"
simple_expect "=1091"
talk_to client 1
simple_expect ":3 17 $tno 1 0"
# Delete text $tno.
talk_to client 0
send "1092 29 $tno\n"
simple_expect "=1092"

# Text 24: sent to a conference we are a member of.
set tno 24
talk_to client 0
send "1093 86 [holl "text $tno"] 1 { 0 1 } 0 { }\n"
simple_expect "=1093 $tno"
talk_to client 1
simple_expect ":16 0 $tno $any_time 5 0 7 0 2 { 0 1 6 3 }"
simple_expect ":18 15 $tno $any_time 5 0 7 0 2 { 0 1 6 3 } 0 \\*"
# Remove conference 1 as recipient of text $tno.
talk_to client 0
send "1094 31 $tno 1\n"
simple_expect "=1094"
talk_to client 1
simple_expect ":3 17 $tno 1 0"
# Delete text $tno.
talk_to client 0
send "1095 29 $tno\n"
simple_expect "=1095"

# Shut everything down.
talk_to client 0
send "1096 42 255\n"
simple_expect "=1096"
send "1097 44 0\n"
simple_expect "=1097"
client_death 0
client_death 1
lyskomd_death
