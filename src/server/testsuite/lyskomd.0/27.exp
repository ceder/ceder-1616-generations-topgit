# Test suite for lyskomd.
# Copyright (C) 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# We have a secret conference (8), a member of that conference (7),
# and a supervisor of the member (6).  The supervisor is neither member
# of the secret conference, nor supervisor of it.  How much can the
# supervisor find out about the secret conference?


lyskomd_start

# Client 0, person 5, logged in as admin.

client_start 0
talk_to client 0
send "A3Hfoo\n"
simple_expect "LysKOM" "connected"
send "1000 80 0 { }\n"
simple_expect "=1000"
send "1001 62 5 [holl "gazonk"] 1\n"
simple_expect "=1001"

# Client 1, person 6, logged as "supervisor".  
client_start 1
talk_to client 1
send "A3Hfoo\n"
simple_expect "LysKOM" "client 1 connected"
send "1002 80 0 { }\n"
simple_expect "=1002"
send "1003 89 [holl "supervisor"] [holl "supersecret"] 00000000 0 { }\n"
simple_expect "=1003 6"
send "1004 62 6 [holl "supersecret"] 0\n"
simple_expect "=1004"

# Create the member.

send "1005 89 [holl "member"] [holl "subsecret"] 00000000 0 { }\n"
simple_expect "=1005 7"

# Person 5 creates the secret conference, adds the member to it, and
# writes a text in it.

talk_to client 0
send "1006 88 [holl "secret"] 10101000 0 { }\n"
simple_expect "=1006 8"
send "1007 100 8 7 100 100 10000000\n"
lyskomd_expect "Person 7 added to conference 8 by 5."
simple_expect "=1007"
send "1008 87 [holl "demo"] 1 { 0 8 } 0 { }\n"
simple_expect "=1008 1"

# Client 2, person 7, loggs in and accepts the invitation.
client_start 2
talk_to client 2
send "A3Hfoo\n"
simple_expect "LysKOM" "client 2 connected"
send "1009 80 0 { }\n"
simple_expect "=1009"
send "1010 62 7 [holl "subsecret"] 0\n"
simple_expect "=1010"
send "1011 98 7 8\n"
simple_expect "=1011 1 $any_time 8 100 0 0 \\* 5 $any_time 10000000"
send "1012 100 8 7 100 100 00000000\n"
simple_expect "=1012"
send "1013 98 7 8\n"
simple_expect "=1013 1 $any_time 8 100 0 0 \\* 5 $any_time 00000000"

#
# Base case: everything should be visible for person 7.  Ensure that
# it is, just to make certain that everything actually exists.
#

# query-read-texts-old
send "1014 9 7 8\n"
simple_expect "=1014 $any_time 8 100 0 0 \\*"

# lookup-name
send "1015 12 [holl "secret"]\n"
simple_expect "=1015 1 { 8 } { 1010 }"

# get-conf-stat-older
send "1016 13 8 1\n"
simple_expect "=1016 [holl "secret"] 1010 $any_time $any_time 5 0 5 0 5 0 77 1 1 1"
send "1017 13 8 0\n"
simple_expect "=1017 [holl ""] 1010 $any_time $any_time 5 0 5 0 5 0 77 1 1 1"

# get-membership-old
send "1018 46 7 0 10 1\n"
simple_expect "=1018 2 { $any_time 7 255 0 0 \\* $any_time 8 100 0 0 \\* }"

# get-member-old
send "1019 48 8 0 10\n"
simple_expect "=1019 1 { 7 }"

# get-person-stat
send "1020 49 7\n"
simple_expect "=1020 [idholl "foo.unknown."] 0000010000000000 00000000 $any_time 0 0 1 0 0 0 0 0 0 1 0 0 2"

# get-conf-stat-old
send "1021 50 8\n"
simple_expect "=1021 [holl "secret"] 1010 $any_time $any_time 5 0 5 0 5 0 77 1 1 1"

# get-unread-confs
send "1022 52 7\n"
simple_expect "=1022 1 { 8 }"

# re-lookup-conf
send "1023 66 [holl "sec"]\n"
simple_expect "=1023 1 { 8 }"

# lookup-conf
send "1024 68 [holl "sec"]\n"
simple_expect "=1024 1 { 8 }"

# re-z-lookup
send "1025 74 [holl "sec"] 0 1\n"
simple_expect "=1025 1 { [holl "secret"] 1010 8 }"

# lookup-z-name
send "1026 76 [holl "sec"] 0 1\n"
simple_expect "=1026 1 { [holl "secret"] 1010 8 }"

# get-uconf-stat
send "1027 78 8\n"
simple_expect "=1027 [holl "secret"] 10101000 1 77"

# get-conf-stat
send "1028 91 8\n"
simple_expect "=1028 [holl "secret"] 10101000 $any_time $any_time 5 0 5 0 5 0 77 77 1 1 1 0 0 \\*"

# query-read-texts
send "1029 98 7 8\n"
simple_expect "=1029 1 $any_time 8 100 0 0 \\* 5 $any_time 00000000"

# get-membership
send "1030 99 7 0 10 1\n"
simple_expect "=1030 2 { 0 $any_time 7 255 0 0 \\* 6 $any_time 00000000 1 $any_time 8 100 0 0 \\* 5 $any_time 00000000 }"

# get-members
send "1031 101 8 0 10\n"
simple_expect "=1031 1 { 7 5 $any_time 00000000 }"

#
# Test case: Ensure that the supervisor only sees what it should.
#

talk_to client 1

# query-read-texts-old: Note: secret conf VISIBLE.
send "1032 9 7 8\n"
good_bad_expect "=1032 $any_time 8 100 0 0 \\*" "%9 8"

# lookup-name
send "1033 12 [holl "secret"]\n"
simple_expect "=1033 0 \\* \\*"

# get-conf-stat-older
send "1034 13 8 1\n"
simple_expect "%1034 9 8"
send "1035 13 8 0\n"
simple_expect "%1035 9 8"

# get-membership-old. Note: secret conf VISIBLE.
send "1036 46 7 0 10 1\n"
simple_expect "=1036 2 { $any_time 7 255 0 0 \\* $any_time 8 100 0 0 \\* }"

# get-member-old. Note: secret conf NOT visible.
send "1037 48 8 0 10\n"
simple_expect "%1037 9 8"

# get-person-stat
send "1038 49 7\n"
simple_expect "=1038 [idholl "foo.unknown."] 0000010000000000 00000000 $any_time 0 0 1 0 0 0 0 0 0 1 0 0 2"

# get-conf-stat-old
send "1039 50 8\n"
simple_expect "%1039 9 8"

# get-unread-confs. Note: secret conf visible.
send "1040 52 7\n"
simple_expect "=1040 1 { 8 }"

# re-lookup-conf
send "1041 66 [holl "sec"]\n"
simple_expect "=1041 0 \\*"

# lookup-conf
send "1042 68 [holl "sec"]\n"
simple_expect "=1042 0 \\*"

# re-z-lookup
send "1043 74 [holl "sec"] 0 1\n"
simple_expect "=1043 0 \\*"

# lookup-z-name
send "1044 76 [holl "sec"] 0 1\n"
simple_expect "=1044 0 \\*"

# get-uconf-stat
send "1045 78 8\n"
simple_expect "%1045 9 8"

# get-conf-stat
send "1046 91 8\n"
simple_expect "%1046 9 8"

# query-read-texts. Note: secret conf VISIBLE.
send "1047 98 7 8\n"
good_bad_expect "=1047 1 $any_time 8 100 0 0 \\* 5 $any_time 00000000" "%9 8"

# get-membership. Note: secret conf VISIBLE.
send "1048 99 7 0 10 1\n"
simple_expect "=1048 2 { 0 $any_time 7 255 0 0 \\* 6 $any_time 00000000 1 $any_time 8 100 0 0 \\* 5 $any_time 00000000 }"

# get-members. Note: secret conf NOT visible.
send "1049 101 8 0 10\n"
simple_expect "%1049 9 8"

# Shut down.
talk_to client 0
send "1050 42 255\n"
simple_expect "=1050"

send "1051 44 0\n"
simple_expect "=1051"
client_death 0
client_death 1
client_death 2

lyskomd_death
