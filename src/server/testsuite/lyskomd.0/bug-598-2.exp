# Test suite for lyskomd.
# Copyright (C) 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test bug 598: get-membership and get-membership-old sometimes drops
# the array size.
#
# In this version: the victim has unread-is-secret set.

#
# Startup and create the players
#

# Start the server.
lyskomd_start

# Create and log in as foo (person 6).
client_start 0
send "A3Hfoo\n"
simple_expect "LysKOM"
send "1000 80 0 { }\n"
simple_expect "=1000"
send "1001 89 [holl "foo"] [holl "p6"] 10000000 0 { }\n"
simple_expect "=1001 6"
send "1002 62 6 [holl "p6"] 0\n"
simple_expect "=1002"

# Create and log in as bar (person 7).
client_start 1
send "A3Hbar\n"
simple_expect "LysKOM"
send "1003 80 0 { }\n"
simple_expect "=1003"
send "1004 89 [holl "bar"] [holl "p7"] 00000000 0 { }\n"
simple_expect "=1004 7"
send "1005 62 7 [holl "p7"] 0\n"
simple_expect "=1005"

# Create and log in as super (person 8).
client_start 2
send "A3Hsup\n"
simple_expect "LysKOM"
send "1006 80 0 { }\n"
simple_expect "=1006"
send "1007 89 [holl "super"] [holl "p8"] 00000000 0 { }\n"
simple_expect "=1007 8"
send "1008 62 8 [holl "p8"] 0\n"
simple_expect "=1008"

#
# Join conferences
#

# foo joins 1.
talk_to client 0
send "1009 100 1 6 200 1 00000000\n"
simple_expect "=1009"

# bar joins 1.
talk_to client 1
send "1010 100 1 7 200 1 00000000\n"
simple_expect "=1010"

#
# foo writes three texts with 1 as recipient.
#

talk_to client 0
send "1011 86 [holl "text 1"] 1 { 0 1 } 0 { }\n"
simple_expect "=1011 1"
send "1012 86 [holl "text 2"] 1 { 0 1 } 0 { }\n"
simple_expect "=1012 2"
send "1013 86 [holl "text 3"] 1 { 0 1 } 0 { }\n"
simple_expect "=1013 3"

# foo marks texts 2 and 3 as read
send "1014 27 1 2 { 2 3 }\n"
simple_expect "=1014"

# Get membership with unread info.

talk_to client 0

send "1015 99 6 0 100 1\n"
simple_expect "=1015 2 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 1 200 0 2 { 2 3 } 6 $any_time 00000000 }"

send "1016 46 6 0 100 1\n"
simple_expect "=1016 2 { $any_time 6 255 0 0 \\* $any_time 1 200 0 2 { 2 3 } }"

talk_to client 1

send "1017 99 6 0 100 1\n"
simple_expect "=1017 2 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 1 200 0 0 \\* 6 $any_time 00000000 }"

send "1018 46 6 0 100 1\n"
simple_expect "=1018 2 { $any_time 6 255 0 0 \\* $any_time 1 200 0 0 \\* }"

talk_to client 2

send "1019 99 6 0 100 1\n"
simple_expect "=1019 2 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 1 200 0 0 \\* 6 $any_time 00000000 }"

send "1020 46 6 0 100 1\n"
simple_expect "=1020 2 { $any_time 6 255 0 0 \\* $any_time 1 200 0 0 \\* }"

# Get membership without unread info.

talk_to client 0

send "1021 99 6 0 100 0\n"
simple_expect "=1021 2 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 1 200 0 2 \\* 6 $any_time 00000000 }"

send "1022 46 6 0 100 0\n"
simple_expect "=1022 2 { $any_time 6 255 0 0 \\* $any_time 1 200 0 2 \\* }"

talk_to client 1

send "1023 99 6 0 100 0\n"
simple_expect "=1023 2 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 1 200 0 0 \\* 6 $any_time 00000000 }"

send "1024 46 6 0 100 0\n"
simple_expect "=1024 2 { $any_time 6 255 0 0 \\* $any_time 1 200 0 0 \\* }"

talk_to client 2

send "1025 99 6 0 100 0\n"
simple_expect "=1025 2 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 1 200 0 0 \\* 6 $any_time 00000000 }"

send "1026 46 6 0 100 0\n"
simple_expect "=1026 2 { $any_time 6 255 0 0 \\* $any_time 1 200 0 0 \\* }"

# Make super admin for foo.
talk_to client 0
send "1027 18 6 8\n"
simple_expect "=1027"

# Get membership with unread info.

talk_to client 0

send "1028 99 6 0 100 1\n"
simple_expect "=1028 2 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 1 200 0 2 { 2 3 } 6 $any_time 00000000 }"

send "1029 46 6 0 100 1\n"
simple_expect "=1029 2 { $any_time 6 255 0 0 \\* $any_time 1 200 0 2 { 2 3 } }"

talk_to client 1

send "1030 99 6 0 100 1\n"
simple_expect "=1030 2 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 1 200 0 0 \\* 6 $any_time 00000000 }"

send "1031 46 6 0 100 1\n"
simple_expect "=1031 2 { $any_time 6 255 0 0 \\* $any_time 1 200 0 0 \\* }"

talk_to client 2

send "1032 99 6 0 100 1\n"
simple_expect "=1032 2 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 1 200 0 2 { 2 3 } 6 $any_time 00000000 }"

send "1033 46 6 0 100 1\n"
simple_expect "=1033 2 { $any_time 6 255 0 0 \\* $any_time 1 200 0 2 { 2 3 } }"

# Get membership without unread info.

talk_to client 0

send "1034 99 6 0 100 0\n"
simple_expect "=1034 2 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 1 200 0 2 \\* 6 $any_time 00000000 }"

send "1035 46 6 0 100 0\n"
simple_expect "=1035 2 { $any_time 6 255 0 0 \\* $any_time 1 200 0 2 \\* }"

talk_to client 1

send "1036 99 6 0 100 0\n"
simple_expect "=1036 2 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 1 200 0 0 \\* 6 $any_time 00000000 }"

send "1037 46 6 0 100 0\n"
simple_expect "=1037 2 { $any_time 6 255 0 0 \\* $any_time 1 200 0 0 \\* }"

talk_to client 2

send "1038 99 6 0 100 0\n"
simple_expect "=1038 2 { 0 $any_time 6 255 0 0 \\* 6 $any_time 00000000 1 $any_time 1 200 0 2 \\* 6 $any_time 00000000 }"

send "1039 46 6 0 100 0\n"
simple_expect "=1039 2 { $any_time 6 255 0 0 \\* $any_time 1 200 0 2 \\* }"

# Shut down.
talk_to client 0
send "1040 0 5 [holl "gazonk"]\n"
simple_expect "=1040"
send "1041 42 255\n"
simple_expect "=1041"
send "1042 44 0\n"
simple_expect "=1042"
client_death 1
client_death 0
lyskomd_death
