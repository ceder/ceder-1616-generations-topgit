# Test suite for lyskomd.
# Copyright (C) 2001-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 



# Test 109=mark-as-unread and 110=set-read-ranges.

lyskomd_start "" "Max read_ranges per call: 10
Sync interval:	3000"

client_start 0
talk_to client 0
send "A\n"
send "0H\n"
simple_expect "LysKOM"
send "1000 62 5 6Hgazonk 0\n"
simple_expect ":2 9 5 1"
simple_expect "=1000"

# Create texts 1-23.
for {set tno 1} {$tno < 24} {incr tno} {
    send "100 86 [holl "text $tno"] 1 { 0 5 } 0 { }\n"
    simple_expect ":16 0 $tno $any_time 5 0 [string length "text $tno"] 0 2 { 0 5 6 $tno }"
    simple_expect "=100 $tno"
}


# Mark a single text as read, and then revert it.
for {set tno 1} {$tno < 24} {incr tno} {

    # Mark text $tno as read.
    send "101 27 5 1 { $tno }\n"
    simple_expect "=101"
    send "102 107 5 5 1 0\n"
    simple_expect "=102 0 $any_time 5 255 1 { $tno $tno } 5 $any_time 00000000"

    # Revert it.
    send "103 109 5 $tno\n"
    simple_expect "=103"
    send "104 107 5 5 1 0\n"
    simple_expect "=104 0 $any_time 5 255 0 \\* 5 $any_time 00000000"
}

# Mark all texts as read.
send "1001 110 5 1 { 1 23 }\n"
simple_expect "=1001"
send "1002 107 5 5 1 0\n"
simple_expect "=1002 0 $any_time 5 255 1 { 1 23 } 5 $any_time 00000000"

# Mark a single text as unread, and then revert it.
for {set tno 1} {$tno < 24} {incr tno} {

    # Mark text $tno as unread.
    send "105 109 5 $tno\n"
    simple_expect "=105"
    send "106 107 5 5 1 0\n"
    if {$tno == 1} {
	simple_expect "=106 0 $any_time 5 255 1 { 2 23 } 5 $any_time 00000000"
    } elseif {$tno == 23} {
	simple_expect "=106 0 $any_time 5 255 1 { 1 22 } 5 $any_time 00000000"
    } else {
	simple_expect "=106 0 $any_time 5 255 2 { 1 [expr {$tno - 1}] [expr {$tno + 1}] 23 } 5 $any_time 00000000"
    }

    # Revert it.
    send "107 27 5 1 { $tno }\n"
    simple_expect "=107"
    send "108 107 5 5 1 0\n"
    simple_expect "=108 0 $any_time 5 255 1 { 1 23 } 5 $any_time 00000000"
}

# Mark all texts as unread, starting at text 1.
for {set tno 1} {$tno < 24} {incr tno} {

    # Mark text $tno as unread.
    send "109 109 5 $tno\n"
    simple_expect "=109"
    send "110 107 5 5 1 0\n"
    if {$tno == 23} {
	simple_expect "=110 0 $any_time 5 255 0 \\* 5 $any_time 00000000"
    } else {
	simple_expect "=110 0 $any_time 5 255 1 { [expr {$tno + 1}] 23 } 5 $any_time 00000000"
    }
}

# Mark all texts as read.
send "1003 110 5 1 { 1 23 }\n"
simple_expect "=1003"
send "1004 107 5 5 1 0\n"
simple_expect "=1004 0 $any_time 5 255 1 { 1 23 } 5 $any_time 00000000"

# Mark all texts as unread, starting at text 23.
for {set tno 23} {$tno > 0} {incr tno -1} {

    # Mark text $tno as unread.
    send "111 109 5 $tno\n"
    simple_expect "=111"
    send "112 107 5 5 1 0\n"
    if {$tno == 1} {
	simple_expect "=112 0 $any_time 5 255 0 \\* 5 $any_time 00000000"
    } else {
	simple_expect "=112 0 $any_time 5 255 1 { 1 [expr {$tno - 1}] } 5 $any_time 00000000"
    }
}

# Mark all texts as read.
send "1005 110 5 1 { 1 23 }\n"
simple_expect "=1005"
send "1006 107 5 5 1 0\n"
simple_expect "=1006 0 $any_time 5 255 1 { 1 23 } 5 $any_time 00000000"

# Mark texts unread in an order designed to test several special cases.
# We also re-mark texts as unread, to ensure that that works.

set unmarked {}
proc unmark {tno res} {
    global unmarked
    global any_time

    send "113 109 5 $tno\n"
    simple_expect "=113"
    send "114 107 5 5 1 0\n"
    simple_expect "=114 0 $any_time 5 255 $res 5 $any_time 00000000"
    set unmarked [lappend unmarked $tno]
    foreach t $unmarked {
	send "115 109 5 $tno\n"
	simple_expect "=115"
    }
    send "116 107 5 5 1 0\n"
    simple_expect "=116 0 $any_time 5 255 $res 5 $any_time 00000000"
}

unmark 10 "2 { 1 9 11 23 }"
unmark  9 "2 { 1 8 11 23 }"
unmark  8 "2 { 1 7 11 23 }"
unmark 11 "2 { 1 7 12 23 }"
unmark 14 "3 { 1 7 12 13 15 23 }"
unmark 15 "3 { 1 7 12 13 16 23 }"
unmark 13 "3 { 1 7 12 12 16 23 }"
unmark 12 "2 { 1 7 16 23 }"
unmark  3 "3 { 1 2 4 7 16 23 }"
unmark  2 "3 { 1 1 4 7 16 23 }"
unmark  1 "2 { 4 7 16 23 }"
unmark 16 "2 { 4 7 17 23 }"
unmark 18 "3 { 4 7 17 17 19 23 }"
unmark 20 "4 { 4 7 17 17 19 19 21 23 }"
unmark 21 "4 { 4 7 17 17 19 19 22 23 }"
unmark 22 "4 { 4 7 17 17 19 19 23 23 }"
unmark 23 "3 { 4 7 17 17 19 19 }"
unmark 19 "2 { 4 7 17 17 }"
unmark  6 "3 { 4 5 7 7 17 17 }"
unmark  7 "2 { 4 5 17 17 }"
unmark  5 "2 { 4 4 17 17 }"
unmark  4 "1 { 17 17 }"
unmark 17 "0 \\*"

# Mark all texts as read.
send "1007 110 5 1 { 1 23 }\n"
simple_expect "=1007"
send "1008 107 5 5 1 0\n"
simple_expect "=1008 0 $any_time 5 255 1 { 1 23 } 5 $any_time 00000000"

set unread {}

unmark 20 "2 { 1 19 21 23 }"
unmark 23 "2 { 1 19 21 22 }"
unmark 21 "2 { 1 19 22 22 }"
unmark 22 "1 { 1 19 }"
unmark  1 "1 { 2 19 }"
unmark  5 "2 { 2 4 6 19 }"
unmark  7 "3 { 2 4 6 6 8 19 }"
unmark  9 "4 { 2 4 6 6 8 8 10 19 }"
unmark 11 "5 { 2 4 6 6 8 8 10 10 12 19 }"
unmark 13 "6 { 2 4 6 6 8 8 10 10 12 12 14 19 }"
unmark  3 "7 { 2 2 4 4 6 6 8 8 10 10 12 12 14 19 }"
unmark  4 "6 { 2 2 6 6 8 8 10 10 12 12 14 19 }"
unmark  2 "5 { 6 6 8 8 10 10 12 12 14 19 }"
unmark 19 "5 { 6 6 8 8 10 10 12 12 14 18 }"
unmark 14 "5 { 6 6 8 8 10 10 12 12 15 18 }"
unmark 18 "5 { 6 6 8 8 10 10 12 12 15 17 }"
unmark 15 "5 { 6 6 8 8 10 10 12 12 16 17 }"
unmark 17 "5 { 6 6 8 8 10 10 12 12 16 16 }"
unmark 16 "4 { 6 6 8 8 10 10 12 12 }"
unmark 10 "3 { 6 6 8 8 12 12 }"
unmark  6 "2 { 8 8 12 12 }"
unmark 12 "1 { 8 8 }"
unmark  8 "0 \\*"

# Test set-read-ranges a few times

proc set_read {lst} {
    global any_time

    send "117 110 5 $lst\n"
    simple_expect "=117"
    send "118 107 5 5 1 0\n"
    simple_expect "=118 0 $any_time 5 255 $lst 5 $any_time 00000000"
}

send "1009 110 5 0 { }\n"
simple_expect "=1009"
send "1010 107 5 5 1 0\n"
simple_expect "=1010 0 $any_time 5 255 0 \\* 5 $any_time 00000000"

set_read "1 { 5 12 }"

send "1011 110 5 0 { }\n"
simple_expect "=1011"
send "1012 107 5 5 1 0\n"
simple_expect "=1012 0 $any_time 5 255 0 \\* 5 $any_time 00000000"

set_read "1 { 1 23 }"
set_read "1 { 1 22 }"
set_read "1 { 1 20 }"
set_read "1 { 2 23 }"
set_read "1 { 2 22 }"
set_read "1 { 2 20 }"
set_read "1 { 4 23 }"
set_read "1 { 4 22 }"
set_read "1 { 4 20 }"

send "1013 110 5 1 { 1 24 }\n"
simple_expect "%1013 16 24"

send "1014 110 5 1 { 24 24 }\n"
simple_expect "%1014 16 24"

send "1015 110 5 1 { 24 28 }\n"
simple_expect "%1015 16 28"

send "1016 110 5 1 { 25 28 }\n"
simple_expect "%1016 16 28"

send "1017 110 5 1 { 24 25 }\n"
simple_expect "%1017 16 25"

send "1018 110 5 1 { 21 28 }\n"
simple_expect "%1018 16 28"

send "1019 107 5 5 1 0\n"
simple_expect "=1019 0 $any_time 5 255 1 { 4 20 } 5 $any_time 00000000"

send "1020 109 5 24\n"
simple_expect "%1020 16 24"

send "1021 109 5 25\n"
simple_expect "%1021 16 25"

send "1022 109 5 26\n"
simple_expect "%1022 16 26"

send "1023 107 5 5 1 0\n"
simple_expect "=1023 0 $any_time 5 255 1 { 4 20 } 5 $any_time 00000000"

set_read "2 { 4 7 18 20 }"
set_read "2 { 1 7 18 20 }"
set_read "2 { 1 7 18 23 }"
set_read "2 { 4 7 18 23 }"
set_read "10 { 1 1 3 3 5 5 7 7 9 9 11 11 13 13 15 15 17 17 19 19 }"

send "1024 110 5 2 { 4 7 8 12 }\n"
simple_expect "=1024"
send "1025 107 5 5 1 0\n"
simple_expect "=1025 0 $any_time 5 255 1 { 4 12 } 5 $any_time 00000000"

send "1026 110 5 2 { 1 7 8 23 }\n"
simple_expect "=1026"
send "1027 107 5 5 1 0\n"
simple_expect "=1027 0 $any_time 5 255 1 { 1 23 } 5 $any_time 00000000"

send "1028 110 5 2 { 1 8 7 23 }\n"
simple_expect "%1028 56 1"

send "1029 110 5 3 { 1 8 7 12 15 13 }\n"
simple_expect "%1029 56 1"

send "1030 110 5 3 { 1 7 8 12 15 13 }\n"
simple_expect "%1030 55 2"

send "1031 110 5 3 { 1 7 8 12 15 13 }\n"
simple_expect "%1031 55 2"

send "1032 110 5 11 { 1 1 3 3 5 5 7 7 9 9 11 11 13 13 15 15 17 17 19 19 21 21 }\n"
simple_expect "%1032 46 10"

send "1033 110 5 12 { 1 1 3 3 5 5 7 7 9 9 11 11 13 13 15 15 17 17 19 19 21 21 23 23 }\n"
simple_expect "%1033 46 10"

send "1034 110 4 1 { 1 1 }\n"
simple_expect "%1034 13 4"

send "1035 110 5 1 { 0 1 }\n"
simple_expect "%1035 17 0"

send "1036 110 6 1 { 1 1 }\n"
simple_expect "%1036 9 6"

send "1037 110 6 1 { 0 1 }\n"
simple_expect "%1037 17 0"

send "1038 110 0 1 { 1 1 }\n"
simple_expect "%1038 8 0"

send "1039 109 4 1\n"
simple_expect "%1039 13 4"

send "1040 109 5 0\n"
simple_expect "%1040 17 0"

send "1041 109 6 0\n"
simple_expect "%1041 9 6"

send "1042 109 6 1\n"
simple_expect "%1042 9 6"

send "1043 109 0 0\n"
simple_expect "%1043 8 0"

send "1044 109 0 1\n"
simple_expect "%1044 8 0"

send "1045 107 5 5 1 0\n"
simple_expect "=1045 0 $any_time 5 255 1 { 1 23 } 5 $any_time 00000000"

# Delete a few texts.  Remaining: 4-6, 8-11, 15-16, 19-21.

foreach tno {1 2 3 7 12 13 14 17 18 22 23} {
    send "119 29 $tno\n"
    simple_expect "=119"
}

send "1046 107 5 5 1 0\n"
simple_expect "=1046 0 $any_time 5 255 1 { 1 23 } 5 $any_time 00000000"

set unmarked {}
unmark  9 "2 { 1 8 10 23 }"
unmark 10 "2 { 1 8 11 23 }"
unmark  1 "2 { 1 8 11 23 }"
unmark 11 "2 { 1 8 12 23 }"
unmark 12 "2 { 1 8 12 23 }"
unmark 13 "2 { 1 8 12 23 }"
unmark 14 "2 { 1 8 12 23 }"
unmark 15 "3 { 1 8 12 14 16 23 }"
unmark 16 "3 { 1 8 12 14 17 23 }"
unmark 17 "3 { 1 8 12 14 17 23 }"
unmark 18 "3 { 1 8 12 14 17 23 }"
unmark 19 "4 { 1 8 12 14 17 18 20 23 }"
unmark  4 "5 { 1 3 5 8 12 14 17 18 20 23 }"
unmark  2 "5 { 1 3 5 8 12 14 17 18 20 23 }"
unmark  3 "5 { 1 3 5 8 12 14 17 18 20 23 }"
unmark  5 "5 { 1 3 6 8 12 14 17 18 20 23 }"
unmark  6 "5 { 1 3 7 8 12 14 17 18 20 23 }"
unmark  7 "5 { 1 3 7 8 12 14 17 18 20 23 }"
unmark  8 "5 { 1 3 7 7 12 14 17 18 20 23 }"
unmark 22 "5 { 1 3 7 7 12 14 17 18 20 23 }"
unmark 21 "6 { 1 3 7 7 12 14 17 18 20 20 22 23 }"
unmark 20 "5 { 1 3 7 7 12 14 17 18 22 23 }"
unmark 23 "5 { 1 3 7 7 12 14 17 18 22 23 }"

proc set_read_2 {lst exp} {
    global any_time

    send "120 110 5 $lst\n"
    simple_expect "=120"
    send "121 107 5 5 1 0\n"
    simple_expect "=121 0 $any_time 5 255 $exp 5 $any_time 00000000"
}

set_read_2 "0 { }" "0 \\*"
set_read_2 "1 { 1 23 }" "1 { 1 23 }"
set_read_2 "1 { 1 22 }" "1 { 1 23 }"
set_read_2 "1 { 1 21 }" "1 { 1 23 }"
set_read_2 "1 { 1 20 }" "1 { 1 20 }"
set_read_2 "1 { 1 19 }" "1 { 1 19 }"
set_read_2 "1 { 1 18 }" "1 { 1 18 }"
set_read_2 "1 { 1 17 }" "1 { 1 18 }"
set_read_2 "1 { 1 16 }" "1 { 1 18 }"
set_read_2 "1 { 1 15 }" "1 { 1 15 }"
set_read_2 "1 { 1 14 }" "1 { 1 14 }"
set_read_2 "1 { 1 13 }" "1 { 1 14 }"
set_read_2 "1 { 1 12 }" "1 { 1 14 }"
set_read_2 "1 { 1 11 }" "1 { 1 14 }"
set_read_2 "1 { 1 10 }" "1 { 1 10 }"
set_read_2 "1 { 1 9 }" "1 { 1 9 }"
set_read_2 "1 { 1 8 }" "1 { 1 8 }"
set_read_2 "1 { 1 7 }" "1 { 1 7 }"
set_read_2 "1 { 1 6 }" "1 { 1 7 }"
set_read_2 "1 { 1 5 }" "1 { 1 5 }"
set_read_2 "1 { 1 4 }" "1 { 1 4 }"
set_read_2 "1 { 1 3 }" "1 { 1 3 }"
set_read_2 "1 { 1 2 }" "1 { 1 3 }"
set_read_2 "1 { 1 1 }" "1 { 1 3 }"
set_read_2 "1 { 2 23 }" "1 { 1 23 }"
set_read_2 "1 { 3 23 }" "1 { 1 23 }"
set_read_2 "1 { 4 23 }" "1 { 1 23 }"
set_read_2 "1 { 5 23 }" "1 { 5 23 }"
set_read_2 "1 { 6 23 }" "1 { 6 23 }"
set_read_2 "1 { 7 23 }" "1 { 7 23 }"
set_read_2 "1 { 8 23 }" "1 { 7 23 }"
set_read_2 "1 { 9 23 }" "1 { 9 23 }"
set_read_2 "1 { 10 23 }" "1 { 10 23 }"
set_read_2 "1 { 11 23 }" "1 { 11 23 }"
set_read_2 "1 { 12 23 }" "1 { 12 23 }"
set_read_2 "1 { 13 23 }" "1 { 12 23 }"
set_read_2 "1 { 14 23 }" "1 { 12 23 }"
set_read_2 "1 { 15 23 }" "1 { 12 23 }"
set_read_2 "1 { 16 23 }" "1 { 16 23 }"
set_read_2 "1 { 17 23 }" "1 { 17 23 }"
set_read_2 "1 { 18 23 }" "1 { 17 23 }"
set_read_2 "1 { 19 23 }" "1 { 17 23 }"
set_read_2 "1 { 20 23 }" "1 { 20 23 }"
set_read_2 "1 { 21 23 }" "1 { 21 23 }"
set_read_2 "1 { 22 23 }" "1 { 22 23 }"
set_read_2 "1 { 23 23 }" "1 { 22 23 }"
set_read_2 "1 { 8 11 }" "1 { 7 14 }"
set_read_2 "1 { 4 6 }" "1 { 1 7 }"
set_read_2 "1 { 19 21 }" "1 { 17 23 }"
set_read_2 "1 { 15 15 }" "1 { 12 15 }"
set_read_2 "1 { 15 16 }" "1 { 12 18 }"
set_read_2 "1 { 15 17 }" "1 { 12 18 }"
set_read_2 "1 { 15 18 }" "1 { 12 18 }"
set_read_2 "1 { 15 19 }" "1 { 12 19 }"
set_read_2 "1 { 20 21 }" "1 { 20 23 }"
set_read_2 "1 { 12 12 }" "1 { 12 14 }"

# Mark all remaining texts as read.
set_read_2 "4 { 4 6 8 11 15 16 19 21 }" "1 { 1 23 }"

# Increase each range by one in each end.
set_read_2 "4 { 3 6 8 11 15 16 19 21 }" "1 { 1 23 }"
set_read_2 "4 { 4 7 8 11 15 16 19 21 }" "1 { 1 23 }"
set_read_2 "4 { 4 6 7 11 15 16 19 21 }" "1 { 1 23 }"
set_read_2 "4 { 4 6 8 12 15 16 19 21 }" "1 { 1 23 }"
set_read_2 "4 { 4 6 8 11 14 16 19 21 }" "1 { 1 23 }"
set_read_2 "4 { 4 6 8 11 15 17 19 21 }" "1 { 1 23 }"
set_read_2 "4 { 4 6 8 11 15 16 18 21 }" "1 { 1 23 }"
set_read_2 "4 { 4 6 8 11 15 16 19 22 }" "1 { 1 23 }"
set_read_2 "4 { 4 6 8 11 15 16 19 23 }" "1 { 1 23 }"

# Decrease each range by one in each end.
set_read_2 "4 { 5 6 8 11 15 16 19 21 }" "1 { 5 23 }"
set_read_2 "4 { 4 5 8 11 15 16 19 21 }" "2 { 1 5 7 23 }"
set_read_2 "4 { 4 6 9 11 15 16 19 21 }" "2 { 1 7 9 23 }"
set_read_2 "4 { 4 6 8 10 15 16 19 21 }" "2 { 1 10 12 23 }"
set_read_2 "4 { 4 6 8 11 16 16 19 21 }" "2 { 1 14 16 23 }"
set_read_2 "4 { 4 6 8 11 15 15 19 21 }" "2 { 1 15 17 23 }"
set_read_2 "4 { 4 6 8 11 15 16 20 21 }" "2 { 1 18 20 23 }"
set_read_2 "4 { 4 6 8 11 15 16 19 20 }" "1 { 1 20 }"

# A few other test cases.
set_read_2 "2 { 6 6 8 8 }" "1 { 6 8 }"
set_read_2 "3 { 6 6 8 8 9 11 }" "1 { 6 14 }"
set_read_2 "0 { }" "0 \\*"
set_read_2 "1 { 1 1 }" "1 { 1 3 }"
set_read_2 "1 { 1 2 }" "1 { 1 3 }"
set_read_2 "1 { 1 3 }" "1 { 1 3 }"
set_read_2 "1 { 2 2 }" "1 { 1 3 }"
set_read_2 "1 { 2 3 }" "1 { 1 3 }"
set_read_2 "1 { 3 3 }" "1 { 1 3 }"
set_read_2 "1 { 1 4 }" "1 { 1 4 }"
set_read_2 "1 { 2 4 }" "1 { 1 4 }"
set_read_2 "1 { 3 4 }" "1 { 1 4 }"
set_read_2 "1 { 4 4 }" "1 { 1 4 }"
set_read_2 "1 { 3 5 }" "1 { 1 5 }"
set_read_2 "1 { 3 5 }" "1 { 1 5 }"
set_read_2 "2 { 3 5 20 21 }" "2 { 1 5 20 23 }"
set_read_2 "3 { 8 11 15 16 19 21 }" "1 { 7 23 }"
set_read_2 "3 { 8 11 15 15 19 21 }" "2 { 7 15 17 23 }"
set_read_2 "2 { 10 11 15 15 }" "1 { 10 15 }"
set_read_2 "2 { 10 12 15 15 }" "1 { 10 15 }"

system "kill -TERM $lyskomd_pid"
client_death 0
lyskomd_death "" signal
