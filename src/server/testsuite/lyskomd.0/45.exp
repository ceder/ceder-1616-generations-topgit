# Test suite for lyskomd.
# Copyright (C) 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test inheritance of aux items.
# Test who can add a canonical-name item on the server.
#
# Persons:
#     5: admin
#     6: Kalle
#     7: Pelle
#     8: Stina
#     9: Anders
#
# Conferences:
#    10: Fritt Forum (public)     (admin: Kalle)(members: Kalle, Anders)
#    11: Overstrike (private)     (admin: Pelle)(members: Pelle, Stina, Anders)
#    12: Hemliga Klubben (secret) (admin: Stina)(members: Kalle, Stina)

lyskomd_start

# Log in as admin, and enable.
client_start 5
send "A[holl "five"]\n"
simple_expect "LysKOM" "connected"
send "1000 80 2 { 15 22 }\n"
simple_expect "=1000"
send "1001 0 5 [holl "gazonk"]\n"
simple_expect "=1001"
send "1002 42 255\n"
simple_expect "=1002"

# Create person 6 and log in.
client_start 6
send "A[holl "Kalle"]\n"
simple_expect "LysKOM" "connected"
send "1003 80 2 { 15 22 }\n"
simple_expect "=1003"
send "1004 89 [holl "Kalle"] [holl "super"] 00000000 0 { }\n"
simple_expect "=1004 6"
send "1005 0 6 [holl "super"]\n"
simple_expect "=1005"

# Create person 7 and log in.
client_start 7
send "A[holl "Pelle"]\n"
simple_expect "LysKOM" "connected"
send "1006 80 2 { 15 22 }\n"
simple_expect "=1006"
send "1007 89 [holl "Pelle"] [holl "member"] 00000000 0 { }\n"
simple_expect "=1007 7"
send "1008 0 7 [holl "member"]\n"
simple_expect "=1008"

# Create person 8 and log in.
client_start 8
send "A[holl "Stina"]\n"
simple_expect "LysKOM" "connected"
send "1009 80 2 { 15 22 }\n"
simple_expect "=1009"
send "1010 89 [holl "Stina"] [holl "bystander"] 00000000 0 { }\n"
simple_expect "=1010 8"
send "1011 0 8 [holl "bystander"]\n"
simple_expect "=1011"

# Create person 9 and log in.
client_start 9
send "A[holl "Anders"]\n"
simple_expect "LysKOM" "connected"
send "1012 80 2 { 15 22 }\n"
simple_expect "=1012"
send "1013 89 [holl "Anders"] [holl "provocateur"] 00000000 0 { }\n"
simple_expect "=1013 9"
send "1014 0 9 [holl "provocateur"]\n"
simple_expect "=1014"

# Create conferences 10, 11 and 12.
talk_to client 6
send "1015 88 [holl "Fritt Forum"] 00000000 0 { }\n"
simple_expect "=1015 10"

talk_to client 7
send "1016 88 [holl "Overstrike"] 10000000 0 { }\n"
simple_expect "=1016 11"

talk_to client 8
send "1017 88 [holl "Hemliga klubben"] 10100000 0 { }\n"
simple_expect "=1017 12"

# Everybody joins conference 1
talk_to client 6
send "1018 100 1 6 200 1 00000000\n"
simple_expect "=1018"

talk_to client 7
send "1019 100 1 7 200 1 00000000\n"
simple_expect "=1019"

talk_to client 8
send "1020 100 1 8 200 1 00000000\n"
simple_expect "=1020"

talk_to client 9
send "1021 100 1 9 200 1 00000000\n"
simple_expect "=1021"

# Kalle (6) and Anders (9) joins Fritt Forum (10)
talk_to client 6
send "1022 100 10 6 200 1 00000000\n"
simple_expect "=1022"

talk_to client 9
send "1023 100 10 9 200 1 00000000\n"
simple_expect "=1023"

# Pelle (7), Stina (8) and Anders (9) joins Overstrike (11)
talk_to client 7
send "1024 100 11 7 200 1 00000000\n"
simple_expect "=1024"

send "1025 100 11 8 200 1 00000000\n"
lyskomd_expect "Person 8 added to conference 11 by 7\\."
simple_expect "=1025"

send "1026 100 11 9 200 1 00000000\n"
lyskomd_expect "Person 9 added to conference 11 by 7\\."
simple_expect "=1026"

talk_to client 8
send "1027 100 11 8 200 1 00000000\n"
simple_expect "=1027"

talk_to client 9
send "1028 100 11 9 200 1 00000000\n"
simple_expect "=1028"

# Kalle (6) and Stina (8) joins Hemliga klubben (12)
talk_to client 8
send "1029 100 12 8 200 1 00000000\n"
simple_expect "=1029"
send "1030 100 12 6 200 1 00000000\n"
lyskomd_expect "Person 6 added to conference 12 by 8\\."
simple_expect "=1030"

talk_to client 6
send "1031 100 12 6 200 1 00000000\n"
simple_expect "=1031"

# Pelle (7) creates text 1 in Fritt Forum (10).
# It contains a public inherited aux-item with tag 10203 and infinite
# inheritance.
talk_to client 7
send "1032 86 [holl "text 1"] 1 { 0 10 } 1 { 10203 01000000 0 [holl "forever more"] }\n"
client_extracting_expect 6 ":18 15 1 ($any_time) 7 0 6 0 2 { 0 10 6 1 } 1 { 1 10203 7 $any_time 01000000 0 [holl "forever more"] }" tt1 1
client_expect 9 ":18 15 1 $tt1 7 0 6 0 2 { 0 10 6 1 } 1 { 1 10203 7 $tt1 01000000 0 [holl "forever more"] }"
simple_expect "=1032 1"

# Wait for a new second.
sleep 1

# Kalle (6) writes a comment (2) to text 1.  Pelle is added as a cc-recpt.
talk_to client 6
send "1033 86 [holl "text 2"] 3 { 0 10 2 1 1 7 } 0 { }\n"
extracting_expect ":18 15 2 ($any_time) 6 0 6 0 5 { 0 10 6 2 2 1 1 7 6 1 } 1 { 1 10203 7 $any_time 01000000 0 [holl "forever more"] }" tt2 1
client_expect 9 ":18 15 2 $tt2 6 0 6 0 5 { 0 10 6 2 2 1 1 7 6 1 } 1 { 1 10203 7 $tt2 01000000 0 [holl "forever more"] }"
client_expect 7 ":18 15 2 $tt2 6 0 6 0 5 { 0 10 6 2 2 1 1 7 6 1 } 1 { 1 10203 7 $tt2 01000000 0 [holl "forever more"] }"
simple_expect "=1033 2"

set test "aux-item created-at not inherited"
if {$tt1 == $tt2} {
    fail "$test (tt1 == tt2 == $tt1)"
} else {
    pass "$test"
}
unset test

# Anders (9) adds a secret inheriting aux-item to text 2.
talk_to client 9
send "1034 92 2 0 { } 1 { 10204 0110000 4 [holl "the invisible aux"] }\n"
simple_expect ":5 22 2 0 \\* 1 { 2 10204 9 $any_time 01100000 4 [holl "the invisible aux"] }"
simple_expect "=1034"

# Kalle writes a comment to text 2 in "Hemliga klubben".
talk_to client 6
send "1035 86 [holl "text 3"] 2 { 2 2 0 12 } 0 { }\n"
simple_expect ":18 15 3 $any_time 6 0 6 0 3 { 2 2 0 12 6 1 } 1 { 1 10203 7 $any_time 01000000 0 [holl "forever more"] }"
client_expect 8 ":18 15 3 $any_time 6 0 6 0 3 { 2 2 0 12 6 1 } 1 { 1 10203 7 $any_time 01000000 0 [holl "forever more"] }"
simple_expect "=1035 3"

# There is a secret aux-item on text 3, but Anders cannot read the text.
talk_to client 9
send "1036 90 3\n"
simple_expect "%1036 14 3"

# The admin can read it, though.
talk_to client 5
send "1037 90 3\n"
simple_expect "=1037 $any_time 6 0 6 0 3 { 2 2 0 12 6 1 } 2 { 1 10203 7 $any_time 01000000 0 [holl "forever more"] 2 10204 9 $any_time 01100000 3 [holl "the invisible aux"] }"

# Stina comments Kalles text.
talk_to client 8
send "1038 86 [holl "text 4"] 2 { 0 12 2 3 } 0 { }\n"
simple_expect ":18 15 4 $any_time 8 0 6 0 3 { 0 12 6 2 2 3 } 1 { 1 10203 7 $any_time 01000000 0 [holl "forever more"] }"
client_expect 6 ":18 15 4 $any_time 8 0 6 0 3 { 0 12 6 2 2 3 } 1 { 1 10203 7 $any_time 01000000 0 [holl "forever more"] }"
simple_expect "=1038 4"

# Now Kalle makes a big mistake: he comments the text in a public conference.
# The secret aux-item will once again be visible.
talk_to client 6
send "1039 86 [holl "text 5"] 2 { 0 1 2 4 } 0 { }\n"
simple_expect ":18 15 5 $any_time 6 0 6 0 3 { 0 1 6 1 2 4 } 1 { 1 10203 7 $any_time 01000000 0 [holl "forever more"] }"
client_expect 7 ":18 15 5 $any_time 6 0 6 0 3 { 0 1 6 1 2 4 } 1 { 1 10203 7 $any_time 01000000 0 [holl "forever more"] }"
client_expect 8 ":18 15 5 $any_time 6 0 6 0 3 { 0 1 6 1 2 4 } 1 { 1 10203 7 $any_time 01000000 0 [holl "forever more"] }"
client_expect 9 ":18 15 5 $any_time 6 0 6 0 3 { 0 1 6 1 2 4 } 2 { 1 10203 7 $any_time 01000000 0 [holl "forever more"] 2 10204 9 $any_time 01100000 1 [holl "the invisible aux"] }"
simple_expect "=1039 5"

# Pelle writes a comment.  The secret aux is no longer inherited.
talk_to client 7
send "1040 86 [holl "text 6"] 2 { 0 1 2 5 } 0 { }\n"
simple_expect ":18 15 6 $any_time 7 0 6 0 3 { 0 1 6 2 2 5 } 1 { 1 10203 7 $any_time 01000000 0 [holl "forever more"] }"
client_expect 6 ":18 15 6 $any_time 7 0 6 0 3 { 0 1 6 2 2 5 } 1 { 1 10203 7 $any_time 01000000 0 [holl "forever more"] }"
client_expect 8 ":18 15 6 $any_time 7 0 6 0 3 { 0 1 6 2 2 5 } 1 { 1 10203 7 $any_time 01000000 0 [holl "forever more"] }"
client_expect 9 ":18 15 6 $any_time 7 0 6 0 3 { 0 1 6 2 2 5 } 1 { 1 10203 7 $any_time 01000000 0 [holl "forever more"] }"
simple_expect "=1040 6"

# Kalle attempts to add a canonical-name aux item on the server.
talk_to client 6
send "1041 95 0 { } 1 { 31 00000000 1 [holl "kom.lysator.liu.se:4444"] }\n"
simple_expect "%1041 12 0"

# admin attempts to add a canonical-name aux item on the server,
# without enabling.
talk_to client 5
send "1042 42 0\n"
simple_expect "=1042"
send "1043 95 0 { } 1 { 31 00000000 1 [holl "kom.lysator.liu.se:4444"] }\n"
simple_expect "%1043 12 0"

# admin succeeds once he has enabled.
send "1044 42 255\n"
simple_expect "=1044"
send "1045 95 0 { } 1 { 31 00000000 1 [holl "kom.lysator.liu.se:4444"] }\n"
simple_expect "=1045"

# Shut down.
talk_to client 5
send "1046 44 0\n"
simple_expect "=1046"
client_death 5
client_death 6
client_death 7
client_death 8
client_death 9

lyskomd_death
