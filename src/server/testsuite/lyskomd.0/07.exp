# Test suite for lyskomd.
# Copyright (C) 1999, 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 


# Test what happens when too many clients try to connect.

lyskomd_start "" "Open files: [expr $PROTECTED_FDS + 5]"

# fd 13
client_start 1
talk_to client 1
send "A3Hone\n"
simple_expect "LysKOM" "one up"

# fd 14
client_start 2
talk_to client 2
send "A3Htwo\n"
simple_expect "LysKOM" "two up"

# fd 15
client_start 3
talk_to client 3
send "A5Hthree\n"
simple_expect "LysKOM" "three up"

# fd 16
client_start 4
talk_to client 4
send "A4Hfour\n"
simple_expect "LysKOM" "four up"

# fd 17 -- not available
client_start 5
talk_to client 5
simple_expect "%% No connections left." "five closing down"
client_death 5

talk_to client 1
simple_expect ":0 11"
talk_to client 2
simple_expect ":0 11"
talk_to client 3
simple_expect ":0 11"
talk_to client 4
simple_expect ":0 11"

# fd 17 -- still not available
client_start 6
talk_to client 6
simple_expect "%% No connections left." "six closing down"
client_death 6

# The server sends at most one ":0 11" per minute.  This test *should*
# never take more than a minute, so don't expect any ":0 11" here.
##
##  talk_to client 1
##  simple_expect ":0 11"
##  talk_to client 2
##  simple_expect ":0 11"
##  talk_to client 3
##  simple_expect ":0 11"
##  talk_to client 4
##  simple_expect ":0 11"

# Shut down fd 15
talk_to client 3
send "1 55 3\n"
simple_expect "=1"
# simple_expect ":2 13 0 3"
client_death 3

talk_to client 1
# simple_expect ":2 13 0 3"
talk_to client 2
# simple_expect ":2 13 0 3"
talk_to client 4
# simple_expect ":2 13 0 3"

# fd 15
client_start 7
talk_to client 7
send "A5Hseven\n"
simple_expect "LysKOM" "seven up"

# fd 17 -- not available again
client_start 8
talk_to client 8
simple_expect "%% No connections left." "eight closing down"
client_death 8

# We don't get these messages, since they are only sent at most once
# per 60 seconds.
##  
##  talk_to client 1
##  simple_expect ":0 11"
##  talk_to client 2
##  simple_expect ":0 11"
##  talk_to client 4
##  simple_expect ":0 11"
##  talk_to client 7
##  simple_expect ":0 11"

# Check that everything is up
talk_to client 1
send "31 56\n"
simple_expect "=31 1"

talk_to client 2
send "32 56\n"
simple_expect "=32 2"

talk_to client 4
send "33 56\n"
simple_expect "=33 4"

talk_to client 7
send "34 56\n"
simple_expect "=34 5"


# shut 'em all down
talk_to client 1
send "2 55 0\n"
simple_expect "=2"
# simple_expect ":2 13 0 1"
client_death 1

talk_to client 2
# simple_expect ":2 13 0 1"
talk_to client 4
# simple_expect ":2 13 0 1"
talk_to client 7
# simple_expect ":2 13 0 1"

talk_to client 2
send "20 55 0\n"
simple_expect "=20"
# simple_expect ":2 13 0 2"
client_death 2

talk_to client 4
# simple_expect ":2 13 0 2"
talk_to client 7
# simple_expect ":2 13 0 2"

talk_to client 4
send "24 55 0\n"
simple_expect "=24"
# simple_expect ":2 13 0 4"
client_death 4

talk_to client 7
# simple_expect ":2 13 0 4"

talk_to client 7
send "27 55 0\n"
simple_expect "=27"
# simple_expect ":2 13 0 5"
client_death 7

client_start 8
talk_to client 8
send "B\n"
simple_expect "%%LysKOM unsupported protocol\\."
client_death 8

# Test shutdown via a signal
system "kill -TERM $lyskomd_pid"

lyskomd_death "" signal
