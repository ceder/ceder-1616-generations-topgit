# Test suite for lyskomd.
# Copyright (C) 1998-1999, 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 


# Check that get-text, get-text-stat-old and get-text-stat don't leak
# information to clients that are not logged in.

lyskomd_start

client_start 0
talk_to client 0
send "A\n"
send "[holl "DejaGnu test suite"]\n"
simple_expect "LysKOM" "connected"

# Log in as the administrator and create a few texts.

send "1000 0 5 [holl gazonk]\n"
simple_expect ":2 9 5 1"
simple_expect "=1000"

send "1001 88 [holl "Secret Conf"] 10100000 0 { }\n"
simple_expect "=1001 6"

send "1002 86 [holl "Text in Secret Conf"] 1 { 0 6 } 0 { }\n"
simple_expect "=1002 1"

send "1003 86 [holl "Text in news conference"] 1 { 0 4 } 0 { }\n"
simple_expect "=1003 2"

send "1004 86 [holl "Deleted text"] 1 { 0 4 } 0 { }\n"
simple_expect "=1004 3"
send "1005 29 3\n"
simple_expect "=1005"

send "1006 86 [holl "Solipsist Nation"] 0 { } 0 { }\n"
simple_expect "=1006 4"

send "1007 86 [holl "Message of the day"] 1 { 0 4 } 0 { }\n"
simple_expect "=1007 5"
send "1008 41 5\n"
simple_expect "%1008 12 0"
send "1009 42 255\n"
simple_expect "=1009"
send "1010 41 5\n"
simple_expect "=1010"
send "1011 42 0\n"
simple_expect "=1011"
send "1012 41 5\n"
simple_expect "%1012 12 0"

send "1013 86 [holl "Mixed-mode text"] 2 { 0 4 0 6 } 1 { 15 00000000 0 [holl "DejaGnu"] }\n"
simple_expect "=1013 6"

# Check that the administrator can retrieve the texts.

send "1014 25 0 0 999\n"
simple_expect "%1014 15 0"
send "1015 26 0\n"
simple_expect "%1015 15 0"
send "1016 90 0\n"
simple_expect "%1016 15 0"

send "1017 25 1 0 999\n"
simple_expect "=1017 [holl "Text in Secret Conf"]"
send "1018 26 1\n"
simple_expect "=1018 $any_time 5 0 19 0 2 { 0 6 6 1 }"
send "1019 90 1\n"
simple_expect "=1019 $any_time 5 0 19 0 2 { 0 6 6 1 } 0 \\*"

send "1020 25 2 1 10\n"
simple_expect "=1020 [holl "ext in new"]"
send "1021 26 2\n"
simple_expect "=1021 $any_time 5 0 23 0 2 { 0 4 6 1 }"
send "1022 90 2\n"
simple_expect "=1022 $any_time 5 0 23 0 2 { 0 4 6 1 } 0 \\*"

send "1023 25 3 0 999\n"
simple_expect "%1023 14 3"
send "1024 26 3\n"
simple_expect "%1024 14 3"
send "1025 90 3\n"
simple_expect "%1025 14 3"

send "1026 25 4 5 11\n"
simple_expect "=1026 [holl "sist Na"]"
send "1027 26 4\n"
simple_expect "=1027 $any_time 5 0 16 0 0 \\*"
send "1028 90 4\n"
simple_expect "=1028 $any_time 5 0 16 0 0 \\* 0 \\*"

send "1029 25 5 0 17\n"
simple_expect "=1029 [holl "Message of the day"]"
send "1030 26 5\n"
# Implementation detail: the text is marked since it is the message of the day.
simple_expect "=1030 $any_time 5 0 18 1 2 { 0 4 6 3 }"
send "1031 90 5\n"
# Implementation detail: the text is marked since it is the message of the day.
simple_expect "=1031 $any_time 5 0 18 1 2 { 0 4 6 3 } 0 \\*"

send "1032 25 5 0 16\n"
simple_expect "=1032 [holl "Message of the da"]"

send "1033 25 5 1 16\n"
simple_expect "=1033 [holl "essage of the da"]"

send "1034 25 5 3 15\n"
simple_expect "=1034 [holl "sage of the d"]"
send "1035 25 5 3 1540\n"
simple_expect "=1035 [holl "sage of the day"]"

send "1036 25 6 0 110\n"
simple_expect "=1036 [holl "Mixed-mode text"]"
send "1037 26 6\n"
simple_expect "=1037 $any_time 5 0 15 0 4 { 0 4 6 4 0 6 6 2 }"
send "1038 90 6\n"
simple_expect "=1038 $any_time 5 0 15 0 4 { 0 4 6 4 0 6 6 2 } 1 { 1 15 5 $any_time 00000000 0 [holl "DejaGnu"] }"

# Check what a session that isn't logged in can retrieve.

client_start 1
talk_to client 1
send "A3Hfoo\n"
simple_expect "LysKOM" "connected"

send "1039 25 0 0 999\n"
simple_expect "%1039 15 0"
send "1040 26 0\n"
simple_expect "%1040 15 0"
send "1041 90 0\n"
simple_expect "%1041 15 0"

send "1042 25 1 0 999\n"
simple_expect "%1042 14 1"
send "1043 26 1\n"
simple_expect "%1043 14 1"
send "1044 90 1\n"
simple_expect "%1044 14 1"

send "1045 25 2 1 10\n"
simple_expect "%1045 14 2"
send "1046 26 2\n"
simple_expect "%1046 14 2"
send "1047 90 2\n"
simple_expect "%1047 14 2"

send "1048 25 3 0 999\n"
simple_expect "%1048 14 3"
send "1049 26 3\n"
simple_expect "%1049 14 3"
send "1050 90 3\n"
simple_expect "%1050 14 3"

send "1051 25 4 5 11\n"
simple_expect "%1051 14 4"
send "1052 26 4\n"
simple_expect "%1052 14 4"
send "1053 90 4\n"
simple_expect "%1053 14 4"

send "1054 25 5 0 17\n"
simple_expect "=1054 [holl "Message of the day"]"
send "1055 26 5\n"
# Implementation detail: the text is marked since it is the message of the day.
simple_expect "=1055 $any_time 5 0 18 1 2 { 0 4 6 3 }"
send "1056 90 5\n"
# Implementation detail: the text is marked since it is the message of the day.
simple_expect "=1056 $any_time 5 0 18 1 2 { 0 4 6 3 } 0 \\*"

send "1057 25 6 0 110\n"
simple_expect "%1057 14 6"
send "1058 26 6\n"
simple_expect "%1058 14 6"
send "1059 90 6\n"
simple_expect "%1059 14 6"

# Create an unprivileged person, and try to re-fetch all texts

send "1060 56\n"
simple_expect "=1060 2"
send "1061 83 1 1 0\n"
simple_expect "=1061 2 { 2 0 0 $any_num 10000000 0H 1 5 0 $any_num 00000000 0H }"
send "1062 89 [holl "John Doe"] [holl "letmein"] 00000000 0 { }\n"
simple_expect "=1062 7"
send "1062 62 7 [holl "letmein"] 0\n"
simple_expect ":2 9 7 2"
simple_expect "=1062"

talk_to client 0
simple_expect ":2 9 7 2" "client 0 also got :2 9 7 2 message"

talk_to client 1
send "1063 83 1 1 0\n"
simple_expect "=1063 2 { 2 7 0 $any_num 00000000 0H 1 5 0 $any_num 00000000 0H }"

send "1064 25 0 0 999\n"
simple_expect "%1064 15 0"
send "1065 26 0\n"
simple_expect "%1065 15 0"
send "1066 90 0\n"
simple_expect "%1066 15 0"

send "1067 25 1 0 999\n"
simple_expect "%1067 14 1"
send "1068 26 1\n"
simple_expect "%1068 14 1"
send "1069 90 1\n"
simple_expect "%1069 14 1"

send "1070 25 2 1 10\n"
simple_expect "=1070 [holl "ext in new"]"
send "1071 26 2\n"
simple_expect "=1071 $any_time 5 0 23 0 2 { 0 4 6 1 }"
send "1072 90 2\n"
simple_expect "=1072 $any_time 5 0 23 0 2 { 0 4 6 1 } 0 \\*"

send "1073 25 3 0 999\n"
simple_expect "%1073 14 3"
send "1074 26 3\n"
simple_expect "%1074 14 3"
send "1075 90 3\n"
simple_expect "%1075 14 3"

send "1076 25 4 5 11\n"
simple_expect "%1076 14 4"
send "1077 26 4\n"
simple_expect "%1077 14 4"
send "1078 90 4\n"
simple_expect "%1078 14 4"

send "1079 25 5 0 17\n"
simple_expect "=1079 [holl "Message of the day"]"
send "1080 26 5\n"
# Implementation detail: the text is marked since it is the message of the day.
simple_expect "=1080 $any_time 5 0 18 1 2 { 0 4 6 3 }"
send "1081 90 5\n"
# Implementation detail: the text is marked since it is the message of the day.
simple_expect "=1081 $any_time 5 0 18 1 2 { 0 4 6 3 } 0 \\*"

send "1082 25 6 0 110\n"
simple_expect "=1082 [holl "Mixed-mode text"]"
send "1083 26 6\n"
simple_expect "=1083 $any_time 5 0 15 0 2 { 0 4 6 4 }"
send "1084 90 6\n"
simple_expect "=1084 $any_time 5 0 15 0 2 { 0 4 6 4 } 1 { 1 15 5 $any_time 00000000 0 [holl "DejaGnu"] }"

talk_to client 0

send "1085 42 255\n"
simple_expect "=1085" "42=enable succeeded"
send "1086 44 0\n"
simple_expect "=1086" "44=shutdown-kom succeeded"
client_death 0
client_death 1
lyskomd_death
