# Test suite for lyskomd.
# Copyright (C) 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test checkkomspace.

if {[file exists ../checkkomspace]} {

# Start lyskomd.
lyskomd_start

set cspid [spawn ../checkkomspace -d config/lyskomd-config]

set test "backup missing"
expect {
    -re "checkkomspace: stat: $any*/db/lyskomd-backup: No such file or directory$nl" {
	pass "$test"
    }
    timeout {
	fail "$test (timeout)"
    }
    eof {
	fail "$test (eof)"
    }
}
set test "first line"
expect {
    -re "If this is a new installation this error will go away after$nl" {
	pass "$test"
    }
    timeout {
	fail "$test (timeout)"
    }
    eof {
	fail "$test (eof)"
    }
}
set test "second line"
expect {
    -re "roughly 1440 minutes\\.$nl" {
	pass "$test"
    }
    timeout {
	fail "$test (timeout)"
    }
    eof {
	fail "$test (eof)"
    }
}

set test "checkkomspace died"
expect {
    eof { pass "$test"; wait }
}
unset test

# Save the database to get rid of the above error.

client_start 0
send "A[holl "foo"]\n"
simple_expect "LysKOM" "connected"
send "1000 80 0 { }\n"
simple_expect "=1000"
send "1001 0 5 [holl "gazonk"]\n"
simple_expect "=1001"
send "1002 42 255\n"
simple_expect "=1002"
send "1003 43\n"
simple_expect "=1003"
send "1004 43\n"
simple_expect "=1004"

# Rerun checkkomspace.

set cspid [spawn ../checkkomspace -d config/lyskomd-config]

set test "diskspace report"
expect {
    -re "Diskspace OK: $any_float MB \\($any_float%\\) and $any_float inodes \\($any_float%\\) free$nl" {
	pass "$test (diskspace OK)$nl"
    }
    -re "Diskspace error: $any_float MB \\($any_float%\\) and $any_float inodes \\($any_float%\\) free$nl" {
	pass "$test (diskspace error reported)"
    }
    -re "Diskspace warning: $any_float MB \\($any_float%\\) and $any_float inodes \\($any_float%\\) free$nl" {
	pass "$test (diskspace warning reported)"
    }
    timeout {
	fail "$test (timeout)"
    }
    eof {
	fail "$test (eof)"
    }
}

set test "checkkomspace sent final line"
expect {
    -re "^Press enter to terminate checkkomspace$nl" {
	pass "$test"
    }
    -re "^($any*)$nl" {
	fail "$test (unexpected line: $expect_out(1,string))"
	exp_continue
    }
}
send "\n"
set test "checkkomspace died"
expect {
    eof { pass "$test"; wait }
}
unset test

talk_to client 0
send "1005 44 0\n"
simple_expect "=1005"
client_death 0
lyskomd_death

} else {
unsupported "checkkomspace is not built"
}
