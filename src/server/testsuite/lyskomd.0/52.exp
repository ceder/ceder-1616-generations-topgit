# Test suite for lyskomd.
# Copyright (C) 2004  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test async-text-read and async-invalidate-text-read.
#
# Persons:
#     5: admin (session 5)
#     6: Kalle (session 6)
#
# Conferences:
#     7: Fritt Forum (public)     (supervisor: Kalle)(members: Kalle, admin)

lyskomd_start

# Log in as admin.
client_start 5
send "A[holl "five"]\n"
simple_expect "LysKOM" "connected"
send "1000 80 2 { 23 24 }\n"
simple_expect "=1000"
send "1001 0 5 [holl "gazonk"]\n"
simple_expect "=1001"

# Create person 6 and log in.
client_start 6
send "A[holl "Kalle"]\n"
simple_expect "LysKOM" "connected"
send "1002 80 2 { 23 24 }\n"
simple_expect "=1002"
send "1003 89 [holl "Kalle"] [holl "super"] 00000000 0 { }\n"
simple_expect "=1003 6"
send "1004 0 6 [holl "super"]\n"
simple_expect "=1004"

# Create conference 7.
talk_to client 6
send "1005 88 [holl "Fritt Forum"] 00000000 0 { }\n"
simple_expect "=1005 7"

# Everybody joins conference 7
talk_to client 6
send "1006 100 7 6 200 1 00000000\n"
simple_expect "=1006"

talk_to client 5
send "1007 100 7 5 200 1 00000000\n"
simple_expect "=1007"

# Kalle (6) creates text 1 in Fritt Forum (7).
talk_to client 6
send "1008 86 [holl "text 1"] 1 { 0 7 } 0 { }\n"
simple_expect "=1008 1"

# admin (5) creates text 2 in Fritt Forum (7).
talk_to client 5
send "1009 86 [holl "text 2"] 1 { 0 7 } 0 { }\n"
simple_expect "=1009 2"

# Kalle (6) creates text 3 in Fritt Forum (7).
talk_to client 6
send "1010 86 [holl "text 3"] 1 { 0 7 } 0 { }\n"
simple_expect "=1010 3"

# admin (5) creates text 4 in Fritt Forum (7).
talk_to client 5
send "1011 86 [holl "text 4"] 1 { 0 7 } 0 { }\n"
simple_expect "=1011 4"

# Kalle (6) marks text 1 and 2 as read.
talk_to client 6
send "1012 27 7 1 { 1 }\n"
simple_expect "=1012"
send "1013 27 7 1 { 2 }\n"
simple_expect "=1013"

# Log in as person 6 again, in a new session.
client_start 61
send "A[holl "Kalle"]\n"
simple_expect "LysKOM" "connected"
send "1014 80 2 { 23 24 }\n"
simple_expect "=1014"
send "1015 0 6 [holl "super"]\n"
simple_expect "=1015"

# Kalle (6) marks text 3 and 4 as read.
talk_to client 6
send "1016 27 7 1 { 3 }\n"
client_expect 61 ":2 23 1 { 7 3 }"
simple_expect "=1016"

talk_to client 61
send "1017 27 7 1 { 4 }\n"
client_expect 6 ":2 23 1 { 7 4 }"
simple_expect "=1017"

# admin (5) marks texts as read.
talk_to client 5
send "1018 27 7 1 { 3 }\n"
simple_expect "=1018"

# Kalle (6) modifies what is unread using 40=set-unread.
talk_to client 6
send "1019 40 7 4\n"
client_expect 61 ":1 24 7"
simple_expect "=1019"

# Kalle (6) modifies what is unread using 77=set-last-read.
talk_to client 6
send "1020 77 7 3\n"
client_expect 61 ":1 24 7"
simple_expect "=1020"

# Kalle (6) modifies what is unread using 109=mark-as-unread.
talk_to client 6
send "1021 109 7 1\n"
client_expect 61 ":1 24 7"
simple_expect "=1021"

# Kalle (6) modifies what is unread using 110=set-read-ranges.
talk_to client 6
send "1022 110 7 1 { 1 3 }\n"
client_expect 61 ":1 24 7"
simple_expect "=1022"

# Shut down.
talk_to client 5
send "1023 42 255\n"
simple_expect "=1023"

send "1024 44 0\n"
simple_expect "=1024"
client_death 5
client_death 6
client_death 61

lyskomd_death
