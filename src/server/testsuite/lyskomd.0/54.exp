# Test suite for lyskomd.
# Copyright (C) 2006  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test the "Listen" configuration directive.

lyskomd_start "" "Listen: 127.0.0.1:$clientport_2
Listen: $clientport_3" "" "" "" {} 1 0 6 1 0 {} {} [list \
    "Listening for clients on 127.0.0.1:$clientport_2." \
    "Listening for clients on 0.0.0.0:$clientport_3."
]

client_start 0

client_start 1 $clientport_2
client_start 2 $clientport_3

talk_to client 1
send "A3Hfoo\n"
simple_expect "LysKOM"
send "1000 0 5 6Hgazonk\n"
simple_expect ":2 9 5 2"
simple_expect "=1000"
send "1001 42 255\n"
simple_expect "=1001"

talk_to client 0
send "A0H\n"
simple_expect "LysKOM"

talk_to client 2
send "A1Hx\n"
simple_expect "LysKOM"

talk_to client 1
send "1002 44 0\n"
simple_expect "=1002"

client_death 0
client_death 1
client_death 2
lyskomd_death


# This test assumes that "localhost" resolves to 127.0.0.1.  I hope it is
# uncommon with systems where that assumption doesn't hold.

lyskomd_start "" "Listen: localhost:$clientport_2
Listen: $clientport_3" "" "" "" {} 1 0 6 1 0 {} {} [list \
    "Listening for clients on 127.0.0.1:$clientport_2." \
    "Listening for clients on 0.0.0.0:$clientport_3."
]

client_start 0

client_start 1 $clientport_2
client_start 2 $clientport_3

talk_to client 1
send "A3Hfoo\n"
simple_expect "LysKOM"
send "1003 0 5 6Hgazonk\n"
simple_expect ":2 9 5 2"
simple_expect "=1003"
send "1004 42 255\n"
simple_expect "=1004"

talk_to client 0
send "A0H\n"
simple_expect "LysKOM"

talk_to client 2
send "A1Hx\n"
simple_expect "LysKOM"

talk_to client 1
send "1005 44 0\n"
simple_expect "=1005"

client_death 0
client_death 1
client_death 2
lyskomd_death
