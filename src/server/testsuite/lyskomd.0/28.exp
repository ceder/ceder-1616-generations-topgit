# Test suite for lyskomd.
# Copyright (C) 2001-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 



# Check the aux-item send-comments-to (33).
# This file checks the variant of send-comments-to that includes the
# recipient type.  See 23.exp for tests that omits the recipient
# type.


# FIXME (bug 314): When a mirror aux-item is implemented, this test
# should be extended with get-conf-stat calls that checks that the
# mirroring aux-item is set and removed appropriately.

# There are three major players:
#
#   5. (client 0) The administrator.
#   6. (client 1)
#   7. (client 2)
#
# There are a few major conferences:
#
#   8. Created by 6.  Public.  Members: 6.  
#   9. Created by 6.  Rd-prot.  Members: 6.
#  10. Created by 6.  Secret.  Members: 6.
#
# Some texts:
#
#   1. Created by 6.  Recipients: 8.
#   2. Created by 6.  Recipients: 9.
#   3. Created by 6.  Recipients: 10.

proc enable {} {
    send "990 42 255\n"
    simple_expect "=990"
}

proc disable {} {
    send "991 42 0\n"
    simple_expect "=991"
}


read_versions

lyskomd_start

client_start 0
talk_to client 0
send "A3Hfoo\n"
simple_expect "LysKOM" "connected"
send "1000 94\n"
simple_expect "=1000 $server_compat_version 1 2 3 4 0 0 \\*"
send "1001 62 5 [holl "gazonk"] 1\n"
simple_expect "=1001"

# Populate the database.

client_start 1
talk_to client 1
send "A3Hbar\n"
simple_expect "LysKOM"
send "1002 89 [holl "Person 6"] [holl "pswd6"] 00 0 { }\n"
simple_expect "=1002 6"
send "1003 62 6 [holl "pswd6"] 1\n"
simple_expect "=1003"

client_start 2
talk_to client 2
send "A  10Hfoo@bar.se\n"
simple_expect "LysKOM"
send "1004 89 [holl "Person 7"] [holl "pswd7"] 00 0 { }\n"
simple_expect "=1004 7"
send "1005 62 7 [holl "pswd7"] 1\n"
simple_expect "=1005"

talk_to client 1
send "1006 88 [holl "conf 8"] 0000 0 { }\n"
simple_expect "=1006 8"
send "1007 88 [holl "conf 9"] 1000 0 { }\n"
simple_expect "=1007 9"
send "1008 88 [holl "conf 10"] 1010 0 { }\n"
simple_expect "=1008 10"

# (Test to create a public-secret conf with four-bit conference type.)
send "1009 88 [holl "conf 11"] 0010 0 { }\n"
simple_expect "%1009 22 0"
# (Test to create a public-secret conf using create-conf-old.)
send "1010 10 [holl "conf 11"] 0010\n"
simple_expect "%1010 22 0"

send "1011 86 [holl "text 1"] 1 { 0 8 } 0 { }\n"
simple_expect "=1011 1"
send "1012 86 [holl "text 2"] 1 { 0 9 } 0 { }\n"
simple_expect "=1012 2"
send "1013 86 [holl "text 3"] 1 { 0 10 } 0 { }\n"
simple_expect "=1013 3"

#
# Test send-comments-to
#

set next_conf 11

# Attempt to set "0" while creating a conference.
# Should fail, since we can only set send-comments-to on letterboxes.
talk_to client 1
send "1014 88 [holl "conf $next_conf I"] 00000000 1 { 33 00000000 1 [holl "0 0"] }\n"
simple_expect "%1014 63 0" "Correct error code for send-comments-to on conference"
send "1015 88 [holl "conf $next_conf I"] 00000000 1 { 33 00000000 1 [holl "0 1"] }\n"
simple_expect "%1015 63 0" "Correct error code for send-comments-to on conference"
send "1016 88 [holl "conf $next_conf I"] 00000000 1 { 33 00000000 1 [holl "0 15"] }\n"
simple_expect "%1016 63 0" "Correct error code for send-comments-to on conference"
send "1017 88 [holl "conf $next_conf I"] 00000000 1 { 33 00000000 1 [holl "0 2"] }\n"
simple_expect "%1017 63 0" "Correct error code for send-comments-to on conference"

# Attempt to set "1" while creating a conference.
# Should fail, since we can only set send-comments-to on letterboxes.
send "1018 88 [holl "conf $next_conf II"] 00000000 1 { 33 00000000 1 [holl "1 0"] }\n"
simple_expect "%1018 63 0" "Correct error code for send-comments-to on conference"
send "1019 88 [holl "conf $next_conf II"] 00000000 1 { 33 00000000 1 [holl "1 1"] }\n"
simple_expect "%1019 63 0" "Correct error code for send-comments-to on conference"
send "1020 88 [holl "conf $next_conf II"] 00000000 1 { 33 00000000 1 [holl "1 15"] }\n"
simple_expect "%1020 63 0" "Correct error code for send-comments-to on conference"
send "1021 88 [holl "conf $next_conf II"] 00000000 1 { 33 00000000 1 [holl "1 13"] }\n"
simple_expect "%1021 63 0" "Correct error code for send-comments-to on conference"

# Attempt to set "0" while creating a text.
# Should fail, since we can only set send-comments-to on letterboxes.
send "1022 86 [holl "text N"] 1 { 0 1 } 1 { 33 00000000 1 [holl "0 0"] }\n"
simple_expect "%1022 63 0" "Correct error code for send-comments-to on texts"
send "1023 86 [holl "text N"] 1 { 0 1 } 1 { 33 00000000 1 [holl "0 1"] }\n"
simple_expect "%1023 63 0" "Correct error code for send-comments-to on texts"
send "1024 86 [holl "text N"] 1 { 0 1 } 1 { 33 00000000 1 [holl "0 15"] }\n"
simple_expect "%1024 63 0" "Correct error code for send-comments-to on texts"
send "1025 86 [holl "text N"] 1 { 0 1 } 1 { 33 00000000 1 [holl "0 7"] }\n"
simple_expect "%1025 63 0" "Correct error code for send-comments-to on texts"

# Attempt to set "1" while creating a text.
# Should fail, since we can only set send-comments-to on letterboxes.
send "1026 86 [holl "text N"] 1 { 0 1 } 1 { 33 00000000 1 [holl "1 0"] }\n"
simple_expect "%1026 63 0" "Correct error code for send-comments-to on texts"
send "1027 86 [holl "text N"] 1 { 0 1 } 1 { 33 00000000 1 [holl "1 1"] }\n"
simple_expect "%1027 63 0" "Correct error code for send-comments-to on texts"
send "1028 86 [holl "text N"] 1 { 0 1 } 1 { 33 00000000 1 [holl "1 15"] }\n"
simple_expect "%1028 63 0" "Correct error code for send-comments-to on texts"
send "1029 86 [holl "text N"] 1 { 0 1 } 1 { 33 00000000 1 [holl "1 16"] }\n"
simple_expect "%1029 63 0" "Correct error code for send-comments-to on texts"

# Attempt to set "0" on system while enabled.
# Should fail, since we can only set send-comments-to on letterboxes.
talk_to client 0
enable
send "1030 95 0 { } 1 { 33 00000000 1 [holl "0 0"] }\n"
simple_expect "%1030 63 0" "Correct error code for send-comments-to on system"
send "1031 95 0 { } 1 { 33 00000000 1 [holl "0 1"] }\n"
simple_expect "%1031 63 0" "Correct error code for send-comments-to on system"
send "1032 95 0 { } 1 { 33 00000000 1 [holl "0 15"] }\n"
simple_expect "%1032 63 0" "Correct error code for send-comments-to on system"
send "1033 95 0 { } 1 { 33 00000000 1 [holl "0 3"] }\n"
simple_expect "%1033 63 0" "Correct error code for send-comments-to on system"

# Attempt to set "1" on system while enabled.
# Should fail, since we can only set send-comments-to on letterboxes.
send "1034 95 0 { } 1 { 33 00000000 1 [holl "1 0"] }\n"
simple_expect "%1034 63 0" "Correct error code for send-comments-to on system"
send "1035 95 0 { } 1 { 33 00000000 1 [holl "1 1"] }\n"
simple_expect "%1035 63 0" "Correct error code for send-comments-to on system"
send "1036 95 0 { } 1 { 33 00000000 1 [holl "1 15"] }\n"
simple_expect "%1036 63 0" "Correct error code for send-comments-to on system"
send "1037 95 0 { } 1 { 33 00000000 1 [holl "1 4"] }\n"
simple_expect "%1037 63 0" "Correct error code for send-comments-to on system"

# Attempt to set "0" on an existing conference.
# Should fail, since we can only set send-comments-to on letterboxes.
talk_to client 1
send "1038 93 10 0 { } 1 { 33 00000000 1 [holl "0 0"] }\n"
simple_expect "%1038 63 0" "Correct error code for send-comments-to on conference"
send "1039 93 10 0 { } 1 { 33 00000000 1 [holl "0 1"] }\n"
simple_expect "%1039 63 0" "Correct error code for send-comments-to on conference"
send "1040 93 10 0 { } 1 { 33 00000000 1 [holl "0 15"] }\n"
simple_expect "%1040 63 0" "Correct error code for send-comments-to on conference"
send "1041 93 10 0 { } 1 { 33 00000000 1 [holl "0 5"] }\n"
simple_expect "%1041 63 0" "Correct error code for send-comments-to on conference"

# Attempt to set "1" on an existing conference.
# Should fail, since we can only set send-comments-to on letterboxes.
send "1042 93 10 0 { } 1 { 33 00000000 1 [holl "1 0"] }\n"
simple_expect "%1042 63 0" "Correct error code for send-comments-to on conference"
send "1043 93 10 0 { } 1 { 33 00000000 1 [holl "1 1"] }\n"
simple_expect "%1043 63 0" "Correct error code for send-comments-to on conference"
send "1044 93 10 0 { } 1 { 33 00000000 1 [holl "1 15"] }\n"
simple_expect "%1044 63 0" "Correct error code for send-comments-to on conference"
send "1045 93 10 0 { } 1 { 33 00000000 1 [holl "1 6"] }\n"
simple_expect "%1045 63 0" "Correct error code for send-comments-to on conference"

# Attempt to set "0" on an existing text.
# Should fail, since we can only set send-comments-to on letterboxes.
send "1046 92 3 0 { } 1 { 33 00000000 1 [holl "0 0"] }\n"
simple_expect "%1046 63 0" "Correct error code for send-comments-to on text"
send "1047 92 3 0 { } 1 { 33 00000000 1 [holl "0 1"] }\n"
simple_expect "%1047 63 0" "Correct error code for send-comments-to on text"
send "1048 92 3 0 { } 1 { 33 00000000 1 [holl "0 15"] }\n"
simple_expect "%1048 63 0" "Correct error code for send-comments-to on text"
send "1049 92 3 0 { } 1 { 33 00000000 1 [holl "0 8"] }\n"
simple_expect "%1049 63 0" "Correct error code for send-comments-to on text"

# Attempt to set "1" on an existing text.
# Should fail, since we can only set send-comments-to on letterboxes.
send "1050 92 3 0 { } 1 { 33 00000000 1 [holl "1 0"] }\n"
simple_expect "%1050 63 0" "Correct error code for send-comments-to on text"
send "1051 92 3 0 { } 1 { 33 00000000 1 [holl "1 1"] }\n"
simple_expect "%1051 63 0" "Correct error code for send-comments-to on text"
send "1052 92 3 0 { } 1 { 33 00000000 1 [holl "1 15"] }\n"
simple_expect "%1052 63 0" "Correct error code for send-comments-to on text"
send "1053 92 3 0 { } 1 { 33 00000000 1 [holl "1 9"] }\n"
simple_expect "%1053 63 0" "Correct error code for send-comments-to on text"

# Attempt to set "0" while creating a person.
# Should work.
client_start 3
talk_to client 3
send "A4Hheja\n"
simple_expect "LysKOM"
send "1054 89 [holl "person $next_conf III"] [holl "pwd"] 00000000 1 { 33 00000000 1 [holl "0 0"] }\n"
extracting_expect "=1054 (\[0-9\]*)" conf_no 1
setup_xfail "*-*-*" "Bug 146"
if {$conf_no == $next_conf} {
    pass
    set next_conf [expr {1 + $next_conf}]
} else {
    fail "got wrong person number"
    set next_conf [expr {1 + $conf_no}]
}

# Attempt to set "1" while creating a person.
# Should work.
send "1055 89 [holl "person $next_conf IV"] [holl "pwd"] 00000000 1 { 33 00000000 1 [holl "1 15"] }\n"
simple_expect "=1055 $next_conf"
set conf_IV $next_conf
set next_conf [expr {1 + $next_conf}]

# Attempt to set "99" while creating a person.
# Should fail, because no such conference exists, but because there is
# no mirror aux item (bug 314) this will work.
send "1056 89 [holl "person $next_conf V"] [holl "pwd"] 00000000 1 { 33 00000000 1 [holl "99 1"] }\n"
# We need a version of simple_expect with a list of expected errors...
setup_xfail "*-*-*" "Bug 314"
if {0} {
    pass "Creation of person with send-comments-to to nonexisting conference forbidden"
    simple_expect "%1056 49 0"
} else {
    fail "Creation of person with send-comments-to to nonexisting conference forbidden"
    simple_expect "=1056 $next_conf"
    set next_conf [expr {1 + $next_conf}]
}

# Attempt to set "10", a secret conference, while creating a person.
# Should fail, because no such conference exists, but because there is
# no mirror aux item (bug 314) this will work.
send "1057 89 [holl "person $next_conf VI"] [holl "pwd"] 00000000 1 { 33 00000000 1 [holl "10 1"] }\n"
setup_xfail "*-*-*" "Bug 314"
if {0} {
    pass "Creation of person with send-comments-to to secret conference forbidden"
    simple_expect "=1057 49 0"
} else {
    fail "Creation of person with send-comments-to to secret conference forbidden"
    simple_expect "=1057 $next_conf"
    set next_conf [expr {1 + $next_conf}]
}

# Attempt to set "9", an rd-prot conference, while creating a person.
# Should work.
send "1058 89 [holl "person $next_conf VII"] [holl "pwd"] 00000000 1 { 33 00000000 1 [holl "9 1"] }\n"
simple_expect "=1058 $next_conf"
set created_person $next_conf
set next_conf [expr {1 + $next_conf}]

# Set "9", an rd-prot conference, on person 6.
# Should work.
talk_to client 1
send "1059 93 6 0 { } 1 { 33 00000000 1 [holl "9 15"] }\n"
simple_expect "=1059"

# Set "10", a secret conference that is visible to the administrator,
# on person 6.  Should fail -- there can be only one send-comments-to.
talk_to client 0
enable
send "1060 93 6 0 { } 1 { 33 00000000 1 [holl "10 0"] }\n"
setup_xfail "*-*-*" "Bug 327"
if {0} {
    simple_expect "%1060 49 0"
} else {
    fail "unique dosen't mean globally unique"
    simple_expect "=1060"
    # Remove the send-comments-to.
    send "900 93 6 1 { 2 } 0 { }\n"
    simple_expect "=900"
}

# Remove the send-comments-to.
send "1061 93 6 1 { 1 } 0 { }\n"
simple_expect "=1061"

# Set "10", a secret conference that is visible to the
# administrator and to person 6.  Should work.
send "1062 93 6 0 { } 1 { 33 00000000 1 [holl "10 2"] }\n"
simple_expect "%1062 64 0"
send "1063 93 6 0 { } 1 { 33 00000000 1 [holl "10 0"] }\n"
simple_expect "=1063"

# Fetch the conference status of person 6.  The aux-item should be
# visible.
send "1064 91 6\n"
extracting_expect "=1064 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 1 { (2|3) 33 5 $any_time 00000000 1 [holl "10 0"] }" auxno 1
setup_xfail "*-*-*" "Bug 327"
if {$auxno == 2} {
    pass "Correct aux-no"
} else {
    # We were able to create an aux-item above that should have failed,
    # so the numbers are now wrong.
    fail "Correct aux-no"
}

# Fetch the conference status of person 6.  The aux-item should be
# visible.
talk_to client 1
send "1065 91 6\n"
simple_expect "=1065 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 1 { $auxno 33 5 $any_time 00000000 1 [holl "10 0"] }"

# Fetch the conference status of person 6.  The aux-item should not be
# visible, because conference 10 is secret and person 7 isn't allowed
# to see it.
talk_to client 2
# (Check that the various get-conf-stat* calls really fail.)
send "1066 13 10 0\n"
simple_expect "%1066 9 10"
send "1067 13 10 1\n"
simple_expect "%1067 9 10"
send "1068 50 10\n"
simple_expect "%1068 9 10"
send "1069 91 10\n"
simple_expect "%1069 9 10"

send "1070 91 6\n"
setup_xfail "*-*-*" "Bug 314"
if {0} {
    pass "Person 7 cannot see conference 10"
    simple_expect "=1070 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 0 { }"
} else {
    fail "Person 7 cannot see conference 10"
    simple_expect "=1070 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 1 { $auxno 33 5 $any_time 00000000 1 [holl "10 0"] }"
}

# Remove the aux-item.
talk_to client 0
send "1071 93 6 1 { $auxno } 0 { }\n"
simple_expect "=1071"

# Attempt to set some bad values, including the empty string, negative
# numbers, hexadecimal numbers, whitespace-separated digits, et c.
# They should all fail.
talk_to client 1
send "1072 93 6 0 { } 1 { 33 00000000 1 [holl " 0"] }\n"
simple_expect "%1072 64 0"
send "1073 93 6 0 { } 1 { 33 00000000 1 [holl " 1"] }\n"
simple_expect "%1073 64 0"
send "1074 93 6 0 { } 1 { 33 00000000 1 [holl "-1 1"] }\n"
simple_expect "%1074 64 0"
send "1075 93 6 0 { } 1 { 33 00000000 1 [holl "-6 1"] }\n"
simple_expect "%1075 64 0"
send "1076 93 6 0 { } 1 { 33 00000000 1 [holl "a 2"] }\n"
simple_expect "%1076 64 0"
send "1077 93 6 0 { } 1 { 33 00000000 1 [holl "0a 2"] }\n"
simple_expect "%1077 64 0"
send "1078 93 6 0 { } 1 { 33 00000000 1 [holl "0x0a 1"] }\n"
simple_expect "%1078 64 0"
send "1079 93 6 0 { } 1 { 33 00000000 1 [holl "0 6 "] }\n"
simple_expect "%1079 64 0"
send "1080 93 6 0 { } 1 { 33 00000000 1 [holl "5 2"] }\n"
simple_expect "%1080 64 0"
send "1081 93 6 0 { } 1 { 33 00000000 1 [holl "5 5"] }\n"
simple_expect "%1081 64 0"
send "1082 93 6 0 { } 1 { 33 00000000 1 [holl "5 3"] }\n"
simple_expect "%1082 64 0"
send "1083 93 6 0 { } 1 { 33 00000000 1 [holl " 5 0"] }\n"
simple_expect "%1083 64 0"
send "1084 93 6 0 { } 1 { 33 00000000 1 [holl " 5 3"] }\n"
simple_expect "%1084 64 0"
send "1085 93 6 0 { } 1 { 33 00000000 1 [holl "0 0 "] }\n"
simple_expect "%1085 64 0"
send "1086 93 6 0 { } 1 { 33 00000000 1 [holl " 0 18"] }\n"
simple_expect "%1086 64 0"
send "1087 93 6 0 { } 1 { 33 00000000 1 [holl " 0 0"] }\n"
simple_expect "%1087 64 0"

# Set to "0".
send "1088 94\n"
simple_expect "=1088 $server_compat_version 1 2 3 4 0 0 \\*"
send "1089 93 6 0 { } 1 { 33 00000000 1 [holl "0 15"] }\n"
simple_expect "=1089"
send "1090 94\n"
simple_expect "=1090 $server_compat_version 1 2 3 4 0 0 \\*"

# Check visibility for person 6.
send "1091 91 6\n"
extracting_expect "=1091 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 1 { (\[0-9\]*) 33 6 $any_time 00000000 1 [holl "0 15"] }" zeroauxno 1
if {$zeroauxno == $auxno + 1} {
    pass "Correct aux-no II"
} else {
    fail "Correct aux-no II"
}

# Check visibility for person 7.
talk_to client 2
send "1092 91 6\n"
simple_expect "=1092 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 1 { $zeroauxno 33 6 $any_time 00000000 1 [holl "0 15"] }"

# Remove "0" and set to "6".
talk_to client 1
send "1093 93 6 1 { $zeroauxno } 1 { 33 00000000 1 [holl "6 1"] }\n"
simple_expect "=1093"
send "1094 94\n"
simple_expect "=1094 $server_compat_version 1 2 3 4 0 0 \\*"

# Check visibility for person 6.
send "1095 91 6\n"
extracting_expect "=1095 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 1 { (\[0-9\]*) 33 6 $any_time 00000000 1 [holl "6 1"] }" sixauxno 1
if {$sixauxno == $zeroauxno + 1} {
    pass "Correct aux-no III"
} else {
    fail "Correct aux-no III"
}

# Check visibility for person 7.
talk_to client 2
send "1096 91 6\n"
simple_expect "=1096 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 1 { $sixauxno 33 6 $any_time 00000000 1 [holl "6 1"] }"

# Remove "6".
talk_to client 0
send "1097 93 6 1 { $sixauxno } 0 { }\n"
simple_expect "=1097"

# Set to "10".
talk_to client 1
send "1098 93 6 0 { } 1 { 33 00000000 1 [holl "10 15"] }\n"
simple_expect "=1098"

# Check visibility for person 6.
send "1099 91 6\n"
extracting_expect "=1099 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 1 { (\[0-9\]*) 33 6 $any_time 00000000 1 [holl "10 15"] }" tenauxno 1
if {$tenauxno == $sixauxno + 1} {
    pass "Correct aux-no IV"
} else {
    fail "Correct aux-no IV"
}

# Check visibility for person 7.  Should not be visible.
talk_to client 2
send "1100 91 6\n"
setup_xfail "*-*-*" "Bug 314"
if {0} {
    pass "Person 7 cannot see conference 10"
    simple_expect "=1100 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 0 { }"
} else {
    fail "Person 7 cannot see conference 10"
    simple_expect "=1100 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 1 { $tenauxno 33 6 $any_time 00000000 1 [holl "10 15"] }"
}

# Remove "10".
talk_to client 1
send "1101 93 6 1 { $tenauxno } 0 { }\n"
simple_expect "=1101"

# Change "10" to "9".  Should fail, since "10" is already removed.
# FIXME (bug 328): What error code should be returned?  Currently,
# 49=aux-item-permission is returned, but is that really proper?
send "1102 93 6 1 { $tenauxno } 1 { 33 00000000 1 [holl "9 15"] }\n"
simple_expect "%1102 49 0"

# Set to "9".
send "1103 93 6 0 { } 1 { 33 00000000 1 [holl "9 15"] }\n"
simple_expect "=1103"

# Check visibility for person 6.
send "1104 91 6\n"
extracting_expect "=1104 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 1 { (\[0-9\]*) 33 6 $any_time 00000000 1 [holl "9 15"] }" nineauxno 1
if {$nineauxno == $tenauxno + 1} {
    pass "Correct aux-no V"
} else {
    fail "Correct aux-no V"
}

# Check visibility for person 7.
talk_to client 2
send "1105 91 6\n"
simple_expect "=1105 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 1 { $nineauxno 33 6 $any_time 00000000 1 [holl "9 15"] }"

# Remove "9".
talk_to client 1
send "1106 93 6 1 { $nineauxno } 0 { }\n"
simple_expect "=1106"

# Set to "8".
send "1107 93 6 0 {  } 1 { 33 00000000 2 [holl "8 0"] }\n"
simple_expect "=1107"

# Check visibility for person 6.
send "1108 91 6\n"
extracting_expect "=1108 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 1 { (\[0-9\]*) 33 6 $any_time 00000000 2 [holl "8 0"] }" eightauxno 1
if {$eightauxno == $nineauxno + 1} {
    pass "Correct aux-no VI"
} else {
    fail "Correct aux-no VI"
}

# Check visibility for person 7.
talk_to client 2
send "1109 91 6\n"
simple_expect "=1109 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 1 { $eightauxno 33 6 $any_time 00000000 2 [holl "8 0"] }"

# Remove "8".
talk_to client 1
send "1110 93 6 1 { $eightauxno } 0 { }\n"
simple_expect "=1110"

# Attempt to set to "10" on person 7. Should fail, because this is a
# unique aux-item, and person 7 isn't allowed to see conference 10.
# See the 2001-12-12 00:30 entry on bug 25 for more info.
# This will work, due to bug 314.
talk_to client 0
send "1111 93 7 0 { } 1 { 33 00000000 1 [holl "10 0"] }\n"
setup_xfail "*-*-*" "Bug 314"
if {0} {
    pass "Cannot create global-unique link to object that the owner cannot see"
    simple_expect "%1111 49 0"
} else {
    fail "Cannot create global-unique link to object that the owner cannot see"
    simple_expect "=1111"
    send "901 93 7 1 { 1 } 0 { }\n"
    simple_expect "=901"
}

# Finally, let the administrator fetch everything, to make sure
# that no stray aux-items (or mirror aux-items) remain.
send "1112 93 $conf_no 1 { 1 } 0 { }\n"
simple_expect "=1112"
send "1113 93 $conf_IV 1 { 1 } 0 { }\n"
simple_expect "=1113"
send "1114 93 $created_person 1 { 1 } 0 { }\n"
simple_expect "=1114"
send "1115 94\n"
simple_expect "=1115 $server_compat_version 1 2 3 4 0 0 \\*"
send "1116 91 1\n"
simple_expect "=1116 [holl "Presentation .av nya. m�ten"] 00001000 $any_time $any_time 0 0 0 0 0 0 77 77 0 1 0 0 0 \\*"
send "1117 91 2\n"
simple_expect "=1117 [holl "Presentation .av nya. medlemmar"] 00001000 $any_time $any_time 0 0 0 0 0 0 77 77 0 1 0 0 0 \\*"
send "1118 91 3\n"
simple_expect "=1118 [holl "Lappar .p�. d�rren"] 00001000 $any_time $any_time 0 0 0 0 0 0 77 77 0 1 0 0 0 \\*"
send "1119 91 4\n"
simple_expect "=1119 [holl "Nyheter om LysKOM"] 00001000 $any_time $any_time 0 0 0 0 0 0 77 77 0 1 0 0 0 \\*"
send "1120 91 5\n"
simple_expect "=1120 [holl "Administrat�r .f�r. LysKOM"] 10011000 $any_time $any_time 5 0 5 0 0 0 77 77 1 1 0 0 0 \\*"
send "1121 91 6\n"
simple_expect "=1121 [holl "Person 6"] 10011000 $any_time $any_time 6 0 6 0 0 0 77 77 1 1 0 0 0 \\*"
send "1122 91 7\n"
simple_expect "=1122 [holl "Person 7"] 10011000 $any_time $any_time 7 0 7 0 0 0 77 77 1 1 0 0 0 \\*"
send "1123 91 8\n"
simple_expect "=1123 [holl "conf 8"] 00001000 $any_time $any_time 6 0 6 0 6 0 77 77 0 1 1 0 0 \\*"
send "1124 91 9\n"
simple_expect "=1124 [holl "conf 9"] 10001000 $any_time $any_time 6 0 6 0 6 0 77 77 0 1 1 0 0 \\*"
send "1125 91 10\n"
simple_expect "=1125 [holl "conf 10"] 10101000 $any_time $any_time 6 0 6 0 6 0 77 77 0 1 1 0 0 \\*"
send "1126 91 $conf_no\n"
simple_expect "=1126 [holl "person 11 III"] 10011000 $any_time $any_time $conf_no 0 $conf_no 0 0 0 77 77 1 1 0 0 0 \\*"
send "1127 91 $conf_IV\n"
simple_expect "=1127 [holl "person $conf_IV IV"] 10011000 $any_time $any_time $conf_IV 0 $conf_IV 0 0 0 77 77 1 1 0 0 0 \\*"
send "1128 91 $created_person\n"
simple_expect "=1128 [holl "person $created_person VII"] 10011000 $any_time $any_time $created_person 0 $created_person 0 0 0 77 77 1 1 0 0 0 \\*"
send "1129 90 1\n"
simple_expect "=1129 $any_time 6 0 6 0 2 { 0 8 6 1 } 0 \\*"
send "1130 90 2\n"
simple_expect "=1130 $any_time 6 0 6 0 2 { 0 9 6 1 } 0 \\*"
send "1131 90 3\n"
simple_expect "=1131 $any_time 6 0 6 0 2 { 0 10 6 1 } 0 \\*"

#
# Shut down
#

talk_to client 0
enable

send "1132 44 0\n"
simple_expect "=1132"
client_death 0

client_death 1

client_death 2

client_death 3

lyskomd_death
