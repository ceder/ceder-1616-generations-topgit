# Test suite for lyskomd.
# Copyright (C) 1998-1999, 2001-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 


# Test of protocol 10 async messages and session issues
#
# Preconditions:
#   Person 5 exists, can enable to 255 and has password "gazonk"
#   Membership functions work
#

read_versions
source "$srcdir/config/prot-a.exp"


# ----------------------------------------------------------------------
# startup_06 sets up the database and starts client 0
#
# The sample database
#
# Person 6 "P6"; supervisor 6; password pw1
# Person 7 "P7"; supervisor 6; password pw2
# Person 8 "P8"; supervisor 7; password pw3
# Person 13 "P13"; supervisor 8; password pw4
# Person 14 "P14"; supervisor 14; password pw5
# Set supervisor of P8 to P14
#
# Conference 9 "C9"; created by 7; rd_prot 
# Conference 10 "C10"; created by 7; secret
# Conference 11 "C11"; created by 7; forbid_secret
# Conference 12 "C12"; created by 7; no flags
#
# Person 8 is a member of conference 12
# Person 13 is a passive member of conference 12
#
# Client 0 is logged on as person 8
# Client 1 is logged on as person 13
# Client 2 is logged on as person 5 with all bits enabled
# Client 3 is logged on as person 8
#
# No clients are accepting async messages
# ----------------------------------------------------------------------
    
proc startup_06 {} {
    client_start 0
    talk_to client 0
    send "A[holl "DejaGnu test suite 1"]\n"
    simple_expect "LysKOM"

    kom_accept_async "0 { }"

    # Set up some users

    kom_create_person "P6" "pw1" "00000000" "0 { }"
    kom_login 6 "pw1" 0
    kom_create_person "P7" "pw2" "00000000" "0 { }"
    kom_logout
    kom_create_person "P8" "pw3" "00000000" "0 { }"
    
    # Set up conferences

    kom_logout
    kom_login 7 "pw2" 0
    kom_create_conference "C9"  "10000000" "0 { }"
    kom_create_conference "C10" "10100000" "0 { }"
    kom_create_conference "C11" "00000100" "0 { }"
    kom_create_conference "C12" "00000000" "0 { }"
    
    # Set up more users

    kom_logout
    kom_login 8 "pw3" 0
    kom_create_person "P13" "pw4" "00000000" "0 { }"

    kom_logout
    kom_create_person "P14" "pw5" "00000000" "0 { }"
    
    kom_logout
    kom_login 5 "gazonk" 0
    kom_enable 255
    kom_set_supervisor 8 14

    # Set up memberships

    kom_logout
    kom_login 8 "pw3" 0
    kom_add_member 12 8 200 0 "00000000"

    kom_logout
    kom_login 13 "pw4" 0
    kom_add_member 12 13 200 0 "01000000"


    # Set up the clients

    # talk_to client 0
    kom_logout
    kom_login 8 "pw3" 0


    client_start 1
    talk_to client 1
    send "A[holl "DejaGnu test suite 2"]\n"
    simple_expect "LysKOM"
    kom_accept_async "0 { }"
    kom_login 13 "pw4" 0


    client_start 2
    talk_to client 2
    send "A[holl "DejaGnu test suite 3"]\n"
    simple_expect "LysKOM"
    kom_accept_async "0 { }"
    kom_login 5 "gazonk" 0
    kom_enable 255

    client_start 3
    talk_to client 3
    send "A[holl "DejaGnu test suite 2"]\n"
    simple_expect "LysKOM"
    kom_accept_async "0 { }"
    kom_login 8 "pw3" 0
}

proc shutdown_06 {} {
    talk_to client 0
    kom_accept_async "0 { }"
    talk_to client 1
    kom_accept_async "0 { }"
    talk_to client 2
    kom_accept_async "0 { }"
    talk_to client 3
    kom_accept_async "0 { }"

    talk_to client 1
    kom_logout

    talk_to client 2
    kom_logout

    talk_to client 3
    kom_logout

    talk_to client 0
    kom_logout
    kom_login 5 "gazonk" 0
    kom_enable 255
    send "9999 44 0\n"
    simple_expect "=9999"

    client_death 0
    client_death 1
    client_death 2
    client_death 3
    lyskomd_death
}



lyskomd_start
startup_06

# ======================================================================
# Test new-text message

# ----------------------------------------------------------------------
# Set up accept-async

talk_to client 0
kom_accept_async "13 { 5 6 7 8 9 11 12 13 14 15 16 17 18 }\n"

talk_to client 1
kom_accept_async "13 { 5 6 7 8 9 11 12 13 14 15 16 17 18 }\n"

talk_to client 2
kom_accept_async "13 { 5 6 7 8 9 11 12 13 14 15 16 17 18 }\n"

talk_to client 3
kom_accept_async "12 { 5 6 7 8 9 11 12 13 14 16 17 18 }\n"



# ----------------------------------------------------------------------
# Create a text in client 2 
# Check that message is not sent to non-member

talk_to client 2
send "1000 86 [holl "Async test 1"] 1 { 0 12 } 0 { }\n"
simple_expect "=1000 1"

# Check that message is not sent to passive member
talk_to client 1
kom_ping_server

# Check that message is sent to active member
talk_to client 0
simple_expect ":18 15 1 $any_time 5 0 12 0 2 { 0 12 6 1 } 0 \\*"
kom_ping_server

# Check that message is blocked by accept-async
talk_to client 3
kom_ping_server


# ======================================================================
# Test new-text-old message

# ----------------------------------------------------------------------
# Set up accept-async

talk_to client 0
kom_accept_async "13 { 0 5 6 7 8 9 11 12 13 14 16 17 18 }\n"

talk_to client 1
kom_accept_async "13 { 0 5 6 7 8 9 11 12 13 14 16 17 18 }\n"

talk_to client 2
kom_accept_async "13 { 0 5 6 7 8 9 11 12 13 14 16 17 18 }\n"

talk_to client 3
kom_accept_async "12 { 5 6 7 8 9 11 12 13 14 16 17 18 }\n"


# ----------------------------------------------------------------------
# Create a text in client 2 
# Check that message is not sent to non-member

talk_to client 2
send "1010 86 [holl "Async test 1"] 1 { 0 12 } 0 { }\n"
simple_expect "=1010 2"

# Check that message is not sent to passive member
talk_to client 1
kom_ping_server

# Check that message is sent to active member
talk_to client 0
simple_expect ":16 0 2 $any_time 5 0 12 0 2 { 0 12 6 2 }"
kom_ping_server

# Check that message is blocked by accept-async
talk_to client 3
kom_ping_server



# ======================================================================
# Test new-text-old message

# ----------------------------------------------------------------------
# Set up accept-async

talk_to client 0
kom_accept_async "14 { 0 5 6 7 8 9 11 12 13 14 15 16 17 18 }\n"

talk_to client 1
kom_accept_async "14 { 0 5 6 7 8 9 11 12 13 14 15 16 17 18 }\n"

talk_to client 2
kom_accept_async "14 { 0 5 6 7 8 9 11 12 13 14 15 16 17 18 }\n"

talk_to client 3
kom_accept_async "13 { 0 5 6 7 8 9 11 12 13 15 16 17 18 }\n"

# ----------------------------------------------------------------------
# Delete a text in client 2
# Check that the message is not sent to a non-member

talk_to client 2
send "1020 29 1\n"
simple_expect "=1020"

# Check that the message is not sent to a passive member
talk_to client 1
kom_ping_server

# Check that message is sent to active member
talk_to client 0
simple_expect ":18 14 1 $any_time 5 0 12 0 2 { 0 12 6 1 } 0 \\*"

kom_ping_server

# Check that message is blocked by accept-async
talk_to client 3

kom_ping_server



# ======================================================================
# Test new-recipient message

# ----------------------------------------------------------------------
# Set up accept-async

talk_to client 0
kom_accept_async "14 { 0 5 6 7 8 9 11 12 13 14 15 16 17 18 }\n"

talk_to client 1
kom_accept_async "14 { 0 5 6 7 8 9 11 12 13 14 15 16 17 18 }\n"

talk_to client 2
kom_accept_async "14 { 0 5 6 7 8 9 11 12 13 14 15 16 17 18 }\n"

talk_to client 3
kom_accept_async "13 { 0 5 6 7 8 9 11 12 13 14 15 17 18 }\n"


# ----------------------------------------------------------------------
# Add a recipient to text 1 in client 2
# Check that the message is not sent to a non-member

talk_to client 2
send "1030 30 2 9 0\n"
simple_expect "=1030"

# Check that the message is not sent to a passive member
talk_to client 1
kom_ping_server

# Check that message is sent to active member
talk_to client 0
simple_expect ":3 16 2 9 0"
kom_ping_server

# Check that message is blocked by accept-async
talk_to client 3
kom_ping_server

# ----------------------------------------------------------------------
# Change recipient type of conference 9 / text 1 in client 2
# Check that the message is not sent to a non-member

talk_to client 2
send "1031 30 2 9 1\n"
simple_expect "=1031"

# Check that the message is not sent to a passive member
talk_to client 1
kom_ping_server

# Check that message is sent to active member
talk_to client 0
simple_expect ":3 16 2 9 1"
kom_ping_server

# Check that message is blocked by accept-async
talk_to client 3
kom_ping_server

# ----------------------------------------------------------------------
# Add a BCC recipient to text one in client 2

talk_to client 2
send "1032 30 2 5 15\n"
simple_expect ":3 16 2 5 15"
simple_expect "=1032"

# Check that the message is not sent to any non-members of 5
talk_to client 1
kom_ping_server

talk_to client 0
kom_ping_server

talk_to client 3
kom_ping_server



# ======================================================================
# Test sub-recipient message

# ----------------------------------------------------------------------
# Set up accept-async

talk_to client 0
kom_accept_async "14 { 0 5 6 7 8 9 11 12 13 14 15 16 17 18 }\n"

talk_to client 1
kom_accept_async "14 { 0 5 6 7 8 9 11 12 13 14 15 16 17 18 }\n"

talk_to client 2
kom_accept_async "14 { 0 5 6 7 8 9 11 12 13 14 15 16 17 18 }\n"

talk_to client 3
kom_accept_async "13 { 0 5 6 7 8 9 11 12 13 14 15 16 18 }\n"


# ----------------------------------------------------------------------
# Remove a BCC recipient
# Check that no message is sent to non-members of the BCC recipient

talk_to client 2
send "1039 31 2 5\n"
simple_expect ":3 17 2 5 15"
simple_expect "=1039"

talk_to client 0
kom_ping_server
talk_to client 1
kom_ping_server
talk_to client 3
kom_ping_server



# ----------------------------------------------------------------------
# Remove a recipient of text 1 in client 2
# Check that the message is not sent to a non-member

talk_to client 2
send "1040 31 2 9\n"
simple_expect "=1040"

# Check that the message is not sent to a passive member
talk_to client 1
kom_ping_server

# Check that message is sent to active member
talk_to client 0
simple_expect ":3 17 2 9 1"
kom_ping_server

# Check that message is blocked by accept-async
talk_to client 3
kom_ping_server



# ======================================================================
# Test new-membership message

# ----------------------------------------------------------------------
# Set up accept-async

talk_to client 0
kom_accept_async "14 { 0 5 6 7 8 9 11 12 13 14 15 16 17 18 }\n"

talk_to client 1
kom_accept_async "14 { 0 5 6 7 8 9 11 12 13 14 15 16 17 18 }\n"

talk_to client 2
kom_accept_async "14 { 0 5 6 7 8 9 11 12 13 14 15 16 17 18 }\n"

talk_to client 3
kom_accept_async "13 { 0 5 6 7 8 9 11 12 13 14 15 16 17 }\n"


# ----------------------------------------------------------------------
# Admin adds a membership to person 8
# Check that no async message is sent to others

talk_to client 2
send "1050 100 9 8 201 0 00000000\n"
lyskomd_expect "Person 8 added to conference 9 by 5."
simple_expect "=1050"

# Check that async message is not sent to supervisor
talk_to client 1
kom_ping_server

# Check that async message is sent to person
talk_to client 0
simple_expect ":2 18 8 9"
kom_ping_server

# Check that accept-async can block the message
talk_to client 3
kom_ping_server


# ----------------------------------------------------------------------
# Admin changes membership of person 8 in conference 12
# Check that no async message is sent to others

talk_to client 2
send "1051 100 9 8 202 0 00000000\n"
simple_expect "=1051"

# Check that async message is not sent to supervisor
talk_to client 1
kom_ping_server

# Check that async message is not sent to person
talk_to client 0
kom_ping_server

# Check that accept-async can block the message
talk_to client 3
kom_ping_server


# ----------------------------------------------------------------------
# Test that new-name messages are sent to the appropriate recipients

talk_to client 0
kom_accept_async "1 { 5 }"
talk_to client 1
kom_accept_async "1 { 5 }"
talk_to client 2
kom_accept_async "1 { 5 }"
talk_to client 3
kom_accept_async "1 { 5 }"

talk_to client 3
kom_login 7 "pw2" 0

talk_to client 2
send "1060 3 10 [holl "NN"]\n"
simple_expect ":3 5 10 [holl "C10"] [holl "NN"]"
simple_expect "=1060"
lyskomd_expect "User 5 changed the name of conference 10\\."
lyskomd_expect "Old name: 'C10'\\."
lyskomd_expect "New name: 'NN'\\."

talk_to client 1
kom_ping_server

talk_to client 0
kom_ping_server

talk_to client 3
simple_expect ":3 5 10 [holl "C10"] [holl "NN"]"
kom_ping_server

talk_to client 3
send "1061 3 10 [holl "C10"]\n"
simple_expect ":3 5 10 [holl "NN"] [holl "C10"]"
simple_expect "=1061"
lyskomd_expect "User 7 changed the name of conference 10\\."
lyskomd_expect "Old name: 'NN'\\."
lyskomd_expect "New name: 'C10'\\."

talk_to client 0
kom_ping_server

talk_to client 1
kom_ping_server

talk_to client 2
simple_expect ":3 5 10 [holl "NN"] [holl "C10"]"
kom_ping_server



# ======================================================================
# Test session issues

# ------------------------------------------------------------
# Test interpretation of session 0 except for disconnect
# disconnect HAS been tested.

client_start 4
talk_to client 4
kom_connect "Session Tester"
kom_accept_async "0 { }"

send "2000 56\n"
extracting_expect "=2000 ($any_num)" session_no 1

kom_login 8 "pw3" 0

send "2001 54 0\n"
simple_expect "=2001 8 0 $session_no 0H $hollerith $any_num $any_time"

send "2002 64 0\n"
simple_expect "=2002 8 0 $session_no 0H $hollerith $hollerith $hollerith $any_num $any_time"

send "2003 69 [holl "DejaGnu Test Suite"] [holl "2-0"]\n"
simple_expect "=2003"

send "2004 70 0\n"
simple_expect "=2004 [holl "DejaGnu Test Suite"]"

send "2005 71 0\n"
simple_expect "=2005 [holl "2-0"]"

send "2006 55 0\n"
simple_expect "=2006"

client_death 4



# ======================================================================

shutdown_06
