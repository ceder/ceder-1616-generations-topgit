# Test suite for lyskomd.
# Copyright (C) 1999, 2002-2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 


# Supplemental test cases for send-async.c

# Note: We can't get good coverage of this file since there
#       is lots of code that checks for protocol A. This code
#       is never run.


source "$srcdir/config/prot-a.exp"

read_versions

proc send_async_test { async } {
    global any_time
    global any_num
    global hollerith
    global PROTECTED_FDS

    lyskomd_start "" "\
Send async: $async
Open files: [expr $PROTECTED_FDS + 4]"

    client_start 0
    talk_to client 0
    kom_connect "DejaGnu Test Suite"

    client_start 1
    talk_to client 1
    kom_connect "DejaGnu Test Suite"
    kom_accept_async "0 { }"
    kom_create_person "P6" "PW6" 00000000 "0 { }"
    kom_login 6 "PW6" 1

    client_start 2

    talk_to client 0
    kom_accept_async "14 { 0 5 6 7 8 9 11 12 13 14 15 16 17 18 }"

    # Test async 9
    
    send "1000 62 5 [holl "gazonk"] 0\n"
    if { $async == "on" } { 
        simple_expect ":2 9 5 1"
    }
    simple_expect "=1000"
    
    # Test async 0 and 15
    
    send "1001 86 [holl "T1"] 1 { 0 5 } 0 { }\n"
    if { $async == "on" } { 
        simple_expect ":16 0 1 $any_time 5 0 2 0 2 { 0 5 6 1 }"
        simple_expect ":18 15 1 $any_time 5 0 2 0 2 { 0 5 6 1 } 0 \\*"
    }
    simple_expect "=1001 1"
    
    send "1001 86 [holl "T2"] 1 { 0 5 } 0 { }\n"
    if { $async == "on" } { 
        simple_expect ":16 0 2 $any_time 5 0 2 0 2 { 0 5 6 2 }"
        simple_expect ":18 15 2 $any_time 5 0 2 0 2 { 0 5 6 2 } 0 \\*"
    }
    simple_expect "=1001 2"
    
    # Test async 5
    
    send "1002 3 5 [holl "Nameless Thing"]\n"
    if { $async == "on" } { 
        simple_expect ":3 5 5 [holl "Administrat�r .f�r. LysKOM"] [holl "Nameless Thing"]"
    }
    simple_expect "=1002"
    lyskomd_expect "User 5 changed the name of conference 5\\."
    lyskomd_expect "Old name: 'Administrat�r \\(f�r\\) LysKOM'\\."
    lyskomd_expect "New name: 'Nameless Thing'\\."

    
    # Test async 6
    
    send "1003 4 [holl "Testing"]\n"
    if { $async == "on" } { 
        simple_expect ":5 6 5 0 1 [holl "Testing"] $hollerith"
    }
    simple_expect "=1003"
    
    # Test async 7
    kom_enable 255
    send "1004 43\n"
    if { $async == "on" } { 
        simple_expect ":0 7"
        simple_expect ":0 7"
    }
    simple_expect "=1004"
    
    
    # Test async 8
    
    kom_add_member 1 6 100 0 00000000
    lyskomd_expect "Person 6 added to conference 1 by 5."
    talk_to client 1
    kom_accept_async "1 { 8 }"
    talk_to client 0
    kom_enable 255
    send "1100 15 1 6\n"
    simple_expect "=1100"
    talk_to client 1
    if { $async == "on" } { 
        simple_expect ":1 8 1"
    }
    talk_to client 0
    kom_enable 0
    
    
    # Test async 12
    
    send "1200 53 0 [holl "Message"]\n"
    if { $async == "on" } { 
        simple_expect ":3 12 0 5 [holl "Message"]"
        simple_expect "=1200"
    } else {
        simple_expect "%1200 52 0"
    }
    
    
    # Test async 13
    
    send "1201 1\n"
    if { $async == "on" } { 
        simple_expect ":2 13 5 1"
    }
    simple_expect "=1201"
    
    send "1202 62 5 [holl "gazonk"] 0\n"
    if { $async == "on" } { 
        simple_expect ":2 9 5 1"
    }
    simple_expect "=1202"
    
    
    # Test async 14
    
    send "1300 29 1\n"
    if { $async == "on" } { 
        simple_expect ":18 14 1 $any_time 5 0 2 0 2 { 0 5 6 1 } 0 \\*"
    }
    simple_expect "=1300"
    
    
    # Test async 16
    
    send "1400 30 2 4 0\n"
    if { $async == "on" } { 
        simple_expect ":3 16 2 4 0"
    }
    simple_expect "=1400"
    
    
    # Test async 17
    
    send "1500 31 2 4\n"
    if { $async == "on" } { 
        simple_expect ":3 17 2 4 0"
    }
    simple_expect "=1500"
    
    
    # Test async 18
    
    send "1600 100 4 5 100 0 00000000\n"
    if { $async == "on" } { 
        simple_expect ":2 18 5 4"
    }
    simple_expect "=1600"
    
    
    kom_accept_async "0 { }"
    kom_login 5 "gazonk" 0
    kom_enable 255
    kom_accept_async "1 { 11 }"
    
    # Test async 11
    
    client_start 3
    talk_to client 3
    send "A[holl "Reject Me"]\n"
    simple_expect "%% No connections left."
    client_death 3
    
    talk_to client 0
    if { $async == "on" } { 
        simple_expect ":0 11"
    }

    talk_to client 2
    kom_connect "Connect Me"
    kom_accept_async "0 { }"

    talk_to client 0
    
    send "9999 44 0\n"
    simple_expect "=9999"
    
    client_death 2
    client_death 1
    client_death 0
    lyskomd_death
}


send_async_test on
send_async_test off


