# Hunt for memory leaks.
# Copyright (C) 1999, 2003  Lysator Academic Computer Association.
#
# This file is part of the LysKOM server.
# 
# LysKOM is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by 
# the Free Software Foundation; either version 1, or (at your option) 
# any later version.
# 
# LysKOM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License
# along with LysKOM; see the file COPYING.  If not, write to
# Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
# or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
# MA 02139, USA.
#
# Please report bugs at http://bugzilla.lysator.liu.se/. 

# Test for leaks when we have to truncate stuff

read_versions
source "$srcdir/config/leaks.exp"

# ----------------------------------------------------------------------
# Test for leaks in text creation

startup_leaks "" "\
Max client data length: 1
Max aux_items added per call: 1
Max aux_items deleted per call: 1
Max links per text: 1"
send "1000 69 [holl "1234567890"] [holl "1234567890"]\n"
simple_expect "%1000 5 1"
shutdown_leaks
read_usage_base


startup_leaks "" "\
Max client data length: 1
Max aux_items added per call: 1
Max aux_items deleted per call: 1
Max links per text: 1"

for { set i 0 } { $i < 200 } { incr i 1 } {

# String truncation
    send "[expr 1000 + $i] 69 [holl "1234567890"] [holl "1234567890"]\n"
    simple_expect "%[expr 1000 + $i] 5 1"
}

shutdown_leaks
check_usage "String truncation" "leaks09-1"


startup_leaks "" "\
Max client data length: 1
Max aux_items added per call: 1
Max aux_items deleted per call: 1
Max links per text: 1"
send "2000 95 0 { } 10 { 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] }\n"
simple_expect "%2000 46 $any_num"
shutdown_leaks
read_usage_base


startup_leaks "" "\
Max client data length: 1
Max aux_items added per call: 1
Max aux_items deleted per call: 1
Max links per text: 1"

for { set i 0 } { $i < 200 } { incr i 1 } {

# Aux-item list truncation
    send "[expr 2000 + $i] 95 0 { } 10 { 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] 10000 00000000 0 [holl "X"] }\n"
    simple_expect "%[expr 2000 + $i] 46 $any_num"

}

shutdown_leaks
check_usage "Aux item list truncation" "leaks09-2"



startup_leaks "" "\
Max client data length: 1
Max aux_items added per call: 1
Max aux_items deleted per call: 1
Max links per text: 1"
send "3000 95 10 { 1 2 3 4 5 6 7 8 9 10 } 0 { }\n"
simple_expect "%3000 46 $any_num"
shutdown_leaks
read_usage_base


startup_leaks "" "\
Max client data length: 1
Max aux_items added per call: 1
Max aux_items deleted per call: 1
Max links per text: 1"

for { set i 0 } { $i < 200 } { incr i 1 } {

# Num-list truncation
    send "[expr 3000 + $i] 95 10 { 1 2 3 4 5 6 7 8 9 10 } 0 { }\n"
    simple_expect "%[expr 3000 + $i] 46 $any_num"

}

shutdown_leaks
check_usage "Number list truncation" "leaks09-3"


startup_leaks "" "\
Max client data length: 1
Max aux_items added per call: 1
Max aux_items deleted per call: 1
Max links per text: 1"
send "4000 86 [holl "X"] 10 { 0 5 0 5 0 5 0 5 0 5 0 5 0 5 0 5 0 5 0 5 } 0 { }\n"
simple_expect "%4000 46 $any_num"
shutdown_leaks
read_usage_base

startup_leaks "" "\
Max client data length: 1
Max aux_items added per call: 1
Max aux_items deleted per call: 1
Max links per text: 1"

for { set i 0 } { $i < 200 } { incr i 1 } {
# Misc-info-list truncation
    send "[expr 4000 + $i] 86 [holl "X"] 10 { 0 5 0 5 0 5 0 5 0 5 0 5 0 5 0 5 0 5 0 5 } 0 { }\n"
    simple_expect "%[expr 4000 + $i] 46 $any_num"

}

shutdown_leaks
check_usage "Misc-info list truncation" "leaks09-4"





