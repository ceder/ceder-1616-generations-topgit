/*
 * $Id: isc-parse.c,v 0.23 2003/08/23 16:38:16 ceder Exp $
 * Copyright (C) 1991, 1993-1994, 1996, 1998-1999, 2001-2003  Lysator Academic Computer Association.
 *
 * This file is part of the LysKOM server.
 * 
 * LysKOM is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 1, or (at your option) 
 * any later version.
 * 
 * LysKOM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with LysKOM; see the file COPYING.  If not, write to
 * Lysator, c/o ISY, Linkoping University, S-581 83 Linkoping, SWEDEN,
 * or the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, 
 * MA 02139, USA.
 *
 * Please report bugs at http://bugzilla.lysator.liu.se/. 
 */
/*
 * Generic parse routines.
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#ifdef HAVE_STRING_H
#  include <string.h>
#else
#  ifdef HAVE_STRINGS_H
#    include <strings.h>
#  endif
#endif
#ifndef HAVE_STRCHR
#  define strchr index
#endif
#include <setjmp.h>
#include "timewrap.h"
#include <sys/types.h>

#include "s-string.h"
#include "kom-types.h"
#include "com.h"
#include "async.h"
#include "connections.h"
#include "isc-parse.h"
#include "kom-config.h"

int
parse_char(Connection *client)
{
    if ( client->unparsed.len <= client->first_to_parse )
	longjmp(parse_env, KOM_MSG_INCOMPLETE);

    return client->unparsed.string[ client->first_to_parse++ ];
}
	

int
parse_nonwhite_char(Connection *client)
{
    int c;

    while (strchr(WHITESPACE, c = parse_char(client)) != NULL)
	;
    return c;
}
