;; Define the indentation style "KOM", for use with GNU Emacs.  This
;; file was placed in the public domain by Per Cederqvist on
;; 2003-08-19.  Please report bugs or improvements at
;; http://bugzilla.lysator.liu.se/

(c-add-style
 "KOM"
 '((c-basic-offset . 4)
   (c-comment-only-line-offset . 0)
   (c-offsets-alist . ((statement-block-intro . +)
		       (knr-argdecl-intro . +)
		       (substatement-open . 0)
		       (label . 0)
		       (statement-cont . +)))
   (c-hanging-braces-alist . ((brace-list-open)
			      (class-open after)
			      (inline-close after)
			      (block-close . c-snug-do-while)))
   (c-hanging-colons-alist . ((access-label after)
			      (case-label after)))
   (c-cleanup-list . (defun-close-semi
		       scope-operator))))
